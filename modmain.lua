
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppev_strings")

GLOBAL.PPEV_FORGE = require("ppev_forge")

local TUNING = require("ppev_tuning")
------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

GLOBAL.GORGE_FOODS = PlayablePets.GetModConfigData("MobFood_Event")
------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	-------FX--------
	"trails2spit",
	"trails2fire",
	"trails2_util",
	"peghook_projectilep",
	"goo_projectilep",
	"lavaarena_floorgratep",
	"boaron_p",
	"groundliftsp",
	"lavaarena_rhinobuff",
	
	"forge_fence",
	"forge_banner",
	
	"coinsp",
	"gorge_foodp",
	"gorge_ingredientsp",
	"gorge_mermhousep",
	"gingerbread_pig_housep",
	"gorge_lampsp",
	"lavaarena",
	"warg_gooicingp",
	-------Tools---------
	"evmonster_wpn",
	"battlestandardp",
	"gorge_ovenp",
	"chefhatp",
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPEV_MobCharacters = {
	--Forge
	boaronp        = { fancyname = "Pit Pig",           gender = "MALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"pitpig"} },
	snapperp        = { fancyname = "Crocommanders",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true, mimic_prefab = {"crocomander"}},
	tortankp        = { fancyname = "Snortoises",           gender = "MALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"snortoise"}},
	peghookp        = { fancyname = "Scorpeons",           gender = "MALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"scorpeon"}},
	trailsp        = { fancyname = "Boarilla",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"boarilla"}},
	trails2p        = { fancyname = "Trails",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true},
	boarriorp        = { fancyname = "Grand Forge Boarrior",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"boarrior"} },
	rhinodrillp        = { fancyname = "Rhinocebro",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = false, mimic_prefab = {"rhinocebro"} },
	beetletaurp        = { fancyname = "Swineclops",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"swineclops"} },
	golemp        = { fancyname = "Magma Golem",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true},
	--Winterfeast
	deerclops_yulep        = { fancyname = "Feastclops",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	goose_yulep        = { fancyname = "Jolly Moose",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	babygoose_yulep        = { fancyname = "Jolly Mossling",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	bearger_yulep        = { fancyname = "Grumpy Bearger",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	dragonfly_yulep        = { fancyname = "Feastfly",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	
	gingerbread_pigp        = { fancyname = "Gingerbread Pig",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"gingerbreadpig"} },
	gingerbread_vargp        = { fancyname = "Gingerbread Varg",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"gingerbreadwarg"} },
	--YotV
	clayvargp        = { fancyname = "Clay Varg",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"claywarg"} },
	clayhoundp        = { fancyname = "Clay Hound",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"clayhound"} },
	--Gorge
	swamppigp        = { fancyname = "Swamp Pig",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true},
	merm_trader1p        = { fancyname = "Sammy",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true},
	merm_trader2p        = { fancyname = "Pipton",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true},
	goatmomp        = { fancyname = "Mumsy",           gender = "FEMALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true},
	goatkidp        = { fancyname = "Billy",           gender = "ROBOT",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true},
	pebblecrabp        = { fancyname = "Pebble Crab",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true},
	pidgeonp        = { fancyname = "Pigeon",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true},
	--YotP
	pigelite_redp        = { fancyname = "Red Elite Pig",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true, mimic_prefab = {"pigelite2"}},
	pigelite_bluep        = { fancyname = "Blue Elite Pig",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true, mimic_prefab = {"pigelite1"}},
	pigelite_greenp        = { fancyname = "Green Elite Pig",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true, mimic_prefab = {"pigelite4"}},
	pigelite_whitep        = { fancyname = "White Elite Pig",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, forge = true, mimic_prefab = {"pigelite3"}},
	--YotC
	kitcoonp        = { fancyname = "Kitcoon",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"kitcoon_deciduous"}},
	--Cawnival
	crowkidp        = { fancyname = "Crow Kid",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, mimic_prefab = {"carnival_crowkid"}},
	crowhostp        = { fancyname = "Corvus Goodfeather",           gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {}, mimic_prefab = {"carnival_host"}},
	herdling_chickp        = { fancyname = "Scrambling Egg",           gender = "ROBOT",   mobtype = {}, skins = {}},
	--Terraria Crossover
	eyeofterrorp        = { fancyname = "Eye of Terror",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"eyeofterror"}},
	eyeofterror_minip        = { fancyname = "Suspicious Peeper",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"eyeofterror_mini"}},

}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPEV_Character_Order = {
	"boaronp",
	"snapperp",
	"tortankp",
	"peghookp",
	"trailsp",
	"boarriorp",
	"rhinodrillp",
	"beetletaurp",
	"golemp",
	"deerclops_yulep",
	"goose_yulep",
	"babygoose_yulep",
	"bearger_yulep",
	"dragonfly_yulep",
	"clayvargp",
	"clayhoundp",
	"swamppigp",
	"merm_trader1p",
	"merm_trader2p",
	"goatmomp",
	"goatkidp",
	"pebblecrabp",
	"pidgeonp",
	"pigelite_redp",
	"pigelite_bluep",
	"pigelite_greenp",
	"pigelite_whitep",
	"trails2p",
	"gingerbread_pigp",
	"gingerbread_vargp",
	"herdling_chickp",
	"crowkidp",
	"crowhostp",
	"kitcoonp",
	"eyeofterror_minip",
	"eyeofterrorp"
}

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"), 
	
	------------
	Asset("SOUNDPACKAGE", "sound/forged_forge.fev"),
	Asset( "SOUND", "sound/forged_forge_bank00.fsb"),
	------------
	
	Asset("ANIM", "anim/lavaarena_boaron_basic.zip"),
	Asset("ANIM", "anim/lavaarena_boarrior_basic.zip"),
	Asset("ANIM", "anim/lavaarena_peghook_basic.zip"),
	Asset("ANIM", "anim/lavaarena_trails_basic.zip"),
	Asset("ANIM", "anim/lavaarena_snapper_basic.zip"),
	Asset("ANIM", "anim/lavaarena_turtillus_basic.zip"),
	--------------------------HOMES--------------------------------
	Asset( "IMAGE", "images/inventoryimages/event_housesp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/event_housesp.xml" ),
	--------------------------SOUNDS-------------------------------
	--Asset("SOUNDPACKAGE", "sound/dontstarvep.fev"), 
	--Asset( "SOUND", "sound/lava_arena.fsb"), 
	
	--------------------------TOOLS---------------------------------
	--Forge
	Asset( "IMAGE", "images/inventoryimages/battlestand_atk.tex" ),
	Asset( "ATLAS", "images/inventoryimages/battlestand_atk.xml" ),
	Asset( "IMAGE", "images/inventoryimages/battlestand_def.tex" ),
	Asset( "ATLAS", "images/inventoryimages/battlestand_def.xml" ),
	Asset( "IMAGE", "images/inventoryimages/battlestand_heal.tex" ),
	Asset( "ATLAS", "images/inventoryimages/battlestand_heal.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/forge_fence_item.tex" ),
	Asset( "ATLAS", "images/inventoryimages/forge_fence_item.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/forge_banner.tex" ),
	Asset( "ATLAS", "images/inventoryimages/forge_banner.xml" ),
	
	Asset("ANIM", "anim/fossilized.zip"),
	--Gorge
	Asset( "IMAGE", "images/inventoryimages/gorge_fencep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_fencep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_gatep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_gatep.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/gorge_lampp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_lampp.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_lamp2p.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_lamp2p.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/gorge_safep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_safep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_keyp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_keyp.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_key_parkp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_key_parkp.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/gorge_coin1p.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_coin1p.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_coin2p.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_coin2p.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_coin3p.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_coin3p.xml" ),
	Asset( "IMAGE", "images/inventoryimages/gorge_coin4p.tex" ),
	Asset( "ATLAS", "images/inventoryimages/gorge_coin4p.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/swamppig_home.tex" ),
	Asset( "ATLAS", "images/inventoryimages/swamppig_home.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/chefhatp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/chefhatp.xml" ),
	
	
	--Asset( "IMAGE", "images/inventoryimages/monster_wpn.tex" ),
	--Asset( "ATLAS", "images/inventoryimages/monster_wpn.xml" ),
}

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------


local defrecipe = AddRecipe("battlestandard_shieldp", {Ingredient("twigs", 4), Ingredient("cutstone", 1), Ingredient("silk", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "battlestandardp_placer", nil, nil, 0.1, "crocovile", "images/inventoryimages/battlestand_def.xml", "battlestand_def.tex")
local atkrecipe = AddRecipe("battlestandard_damagerp", {Ingredient("twigs", 4), Ingredient("boneshard", 1), Ingredient("silk", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "battlestandardp_placer", nil, nil, 0.1, "crocovile", "images/inventoryimages/battlestand_atk.xml", "battlestand_atk.tex")
local healstandrecipe = AddRecipe("battlestandard_healp", {Ingredient("twigs", 4), Ingredient("petals", 4), Ingredient("nightmarefuel", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "battlestandardp_placer", nil, nil, 0.1, "crocovile", "images/inventoryimages/battlestand_heal.xml", "battlestand_heal.tex")

local fencerecipe = AddRecipe("forge_fence_item", {Ingredient("twigs", 3), Ingredient("rope", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 6, "LA_mob", "images/inventoryimages/forge_fence_item.xml", "forge_fence_item.tex")
local forgebannerrecipe = AddRecipe("forge_banner", {Ingredient("twigs", 4), Ingredient("silk", 3), Ingredient("rope", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "forge_banner_placer", nil, nil, 0.1, "LA_mob", "images/inventoryimages/forge_banner.xml", "forge_banner.tex")

--Gorge
local coin1recipe = AddRecipe("gorge_coin1p", {Ingredient("goldnugget", 3)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 1, "QM_mob", "images/inventoryimages/gorge_coin1p.xml", "gorge_coin1p.tex")
local coin2recipe = AddRecipe("gorge_coin2p", {Ingredient("gorge_coin1p", 12, "images/inventoryimages/gorge_coin1p.xml")}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 1, "QM_mob", "images/inventoryimages/gorge_coin2p.xml", "gorge_coin2p.tex")
local coin3recipe = AddRecipe("gorge_coin3p", {Ingredient("gorge_coin2p", 6, "images/inventoryimages/gorge_coin2p.xml")}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 1, "QM_mob", "images/inventoryimages/gorge_coin3p.xml", "gorge_coin3p.tex")
local coin4recipe = AddRecipe("gorge_coin4p", {Ingredient("gorge_coin3p", 3, "images/inventoryimages/gorge_coin3p.xml")}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 1, "QM_mob", "images/inventoryimages/gorge_coin4p.xml", "gorge_coin4p.tex")

local lamprecipe = AddRecipe("gorge_lampp", {Ingredient("cutstone", 4), Ingredient("lightbulb", 1)}, GLOBAL.RECIPETABS.LIGHT, GLOBAL.TECH.SCIENCE_ONE, "gorge_lampp_placer", 1.5, nil, 1, "QM_mob", "images/inventoryimages/gorge_lampp.xml", "gorge_lampp.tex")
--local saferecipe = AddRecipe("gorge_safep", {Ingredient("gears", 1), Ingredient("steelwool", 2), Ingredient("goldnugget", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "gorge_safep_placer", 1, nil, 1, nil, "images/inventoryimages/gorge_safep.xml", "gorge_safep.tex")
--local key1recipe = AddRecipe("gorge_keyp", {Ingredient("goldnugget", 6)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_TWO, nil, nil, nil, 1, nil, "images/inventoryimages/gorge_keyp.xml", "gorge_keyp.tex")
--local key2recipe = AddRecipe("gorge_key_parkp", {Ingredient("cutstone", 3)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.SCIENCE_TWO, nil, nil, nil, 1, nil, "images/inventoryimages/gorge_key_parkp.xml", "gorge_key_parkp.tex")
--local lamp2recipe = AddRecipe("gorge_lamp2p", {Ingredient("cutstone", 4), Ingredient("lightbulb", 1)}, GLOBAL.RECIPETABS.LIGHT, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, 1, "QM_mob", "images/inventoryimages/gorge_lamp2p.xml", "gorge_lamp2p.tex")

local gorgehomerecipe = AddRecipe("gorge_mermhousep", {Ingredient("cutstone", 6), Ingredient("boards", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "gorge_mermhousep_placer", nil, nil, 0.1, "QM_mob", "images/inventoryimages/swamppig_home.xml", "swamppig_home.tex")


--Winterfeast
local gingerbreadhouserecipe = AddRecipe("gingerbread_pig_housep", {Ingredient("rocks", 1), Ingredient("log", 4), Ingredient("wintersfeastfuel", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.NONE, "gingerbread_pig_housep_placer", nil, nil, 0.1, "gingerbread_pig", "images/inventoryimages/event_housesp.xml", "gingerbread_pig_housep.tex")

gingerbreadhouserecipe.sortkey = -25.0
gorgehomerecipe.sortkey = -13.0


-------------------------------------------
--Gorge Food Recipes--
if GLOBAL.GORGE_FOODS == "Enable" then

local chefhatrecipe = AddRecipe("chefhatp", {Ingredient("silk", 8)}, GLOBAL.RECIPETABS.DRESS, GLOBAL.TECH.SCIENCE_TWO, nil, nil, nil, nil, nil, "images/inventoryimages/chefhatp.xml")
local gorgeovenrecipe = AddRecipe("gorge_ovenp", {Ingredient("cutstone", 6), Ingredient("log", 3), Ingredient("cutgrass", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.NONE, "gorge_ovenp_placer", nil, nil, 0.1, "master_chef", "images/inventoryimages2.xml", "wintersfeastoven.tex")
local flourrecipe = AddRecipe("gorge_flourp", {Ingredient("cutgrass", 3)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.NONE, nil, nil, nil, nil, "master_chef", "images/inventoryimages.xml", "quagmire_flour.tex")
local turniprecipe = AddRecipe("gorge_turnipp", {Ingredient("onion", 1, nil, nil, "quagmire_onion.tex"), Ingredient("nightmarefuel", 1)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.NONE, nil, nil, nil, nil, "master_chef", "images/inventoryimages.xml", "quagmire_turnip.tex")
local syruprecipe = AddRecipe("gorge_syrupp", {Ingredient("butter", 1), Ingredient("nightmarefuel", 1)}, GLOBAL.RECIPETABS.REFINE, GLOBAL.TECH.NONE, nil, nil, nil, nil, "master_chef", "images/inventoryimages.xml", "quagmire_syrup.tex")

chefhatrecipe.sortkey = -20.0
gorgeovenrecipe.sortkey = -20.5

modimport "ctt.lua"

AddNewTechTree("GORGE_COOKINGP", 1)

local TechTree = require("techtree")

GLOBAL.TECH.GORGE_COOKINGP = {GORGE_COOKINGP = 1}
GLOBAL.TUNING.PROTOTYPER_TREES.GORGE_COOKINGP = TechTree.Create({GORGE_COOKINGP = 1})

local function MakeIngredients(data)
	local ingredients = {}
	for k,v in pairs(data) do
		--if #data > 2 then
		table.insert(ingredients, Ingredient(v[1], v[2], v[3] and v[3] or nil, v[4] or nil, v[5] or nil))
	end
		return ingredients
end

local function MakeFoodRecipe(data)	
	return AddRecipe("gorge_"..data.name.."p", MakeIngredients(data.ingredients), GLOBAL.RECIPETABS.WINTERSFEASTCOOKING, GLOBAL.TECH.GORGE_COOKINGP, nil, nil, true, nil, nil, "images/inventoryimages/gorge_foodsp.xml", "gorge_"..data.name.."p.tex")
end

local prefs = {}

local foods = require("ppev_foods")
for k,v in pairs(foods) do
	GLOBAL.STRINGS.NAMES[string.upper("gorge_"..v.name.."p")] = GLOBAL.STRINGS.NAMES[string.upper("quagmire_food_"..v.id)]
	GLOBAL.STRINGS.RECIPE_DESC[string.upper("gorge_"..v.name.."p")] = "Delicious food from a far away land."
    MakeFoodRecipe(v)
end

local gorge_ingredients = require("ppev_ingredients")
for k,v in pairs(gorge_ingredients) do
	GLOBAL.STRINGS.NAMES[string.upper("gorge_"..v.name.."p")] = GLOBAL.STRINGS.NAMES[string.upper(v.nameoverride)]
	GLOBAL.STRINGS.RECIPE_DESC[string.upper("gorge_"..v.name.."p")] = "An ingredient used for cooking."
end

end
-------------------------------------------

defrecipe.sortkey = -8.0
atkrecipe.sortkey = -9.0
healstandrecipe.sortkey = -10.0
fencerecipe.sortkey = -11.0
forgebannerrecipe.sortkey = -12.0

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------


-------------------------------------------------------
--Wardrobe stuff--
local MobPuppets = require("ppev_puppets")
local MobSkins = require("ppev_skins")
PlayablePets.RegisterPuppetsAndSkins(PPEV_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPEV_Character_Order) do
	local mob = GLOBAL.PPEV_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPEV_Character_Order) do
	local mob = GLOBAL.PPEV_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end