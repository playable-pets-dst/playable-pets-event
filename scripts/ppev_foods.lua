
return	{
	
	--[[
		PERISH_ONE_DAY = 1*total_day_time*perish_warp,
        PERISH_TWO_DAY = 2*total_day_time*perish_warp,
        PERISH_SUPERFAST = 3*total_day_time*perish_warp,
        PERISH_FAST = 6*total_day_time*perish_warp,
        PERISH_FASTISH = 8*total_day_time*perish_warp,
        PERISH_MED = 10*total_day_time*perish_warp,
        PERISH_SLOW = 15*total_day_time*perish_warp,
        PERISH_PRESERVED = 20*total_day_time*perish_warp,
        PERISH_SUPERSLOW = 40*total_day_time*perish_warp,
	]]
	BREAD = {
		id = "001",
		name = "bread",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 0,
		hunger = 15,
		sanity = 1,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_MED,
		
		ingredients = {{"gorge_flourp", 3, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CHIPS = {
		id = "002",
		name = "chips",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 3,
		hunger = 20,
		sanity = 15,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"potato", 3, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	VEGGIE_SOUP = {
		id = "003",
		name = "veggie_soup",
		plating = "bowl",
		type = "idle", --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 20,
		sanity = 5,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"carrot", 2}, {"potato", 1, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	JELLY_SANDWICH = {
		id = "004",
		name = "jelly_sandwich",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 25,
		sanity = 25,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"berries", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISH_STEW = {
		id = "005",
		name = "fish_stew",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 35,
		sanity = 10,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"pondfish", 1}, {"carrot", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	TURNIP_CAKE = {
		id = "006",
		name = "turnip_cake",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 30,
		sanity = 20,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_turnipp", 3, nil, nil, "quagmire_turnip.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	POTATO_PANCAKES = {
		id = "007",
		name = "potato_pancakes",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 15,
		hunger = 35,
		sanity = 30,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"potato", 3, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	POTATO_SOUP = {
		id = "008",
		name = "potato_soup",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 25,
		sanity = 30,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"potato", 3, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISHBALL_SKEWERS = {
		id = "009",
		name = "fishball_skewers",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 25,
		sanity = 5,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"pondfish", 1}, {"carrot", 1}, {"twigs", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MEATBALLS = {
		id = "010",
		name = "meatballs",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 40,
		sanity = 3,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"smallmeat", 1}, {"carrot", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MEAT_SKEWERS = {
		id = "011",
		name = "meat_skewers",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 30,
		sanity = 5,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"smallmeat", 2}, {"twigs", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	STONE_SOUP = {
		id = "012",
		name = "stone_soup",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 50,
		sanity = 30,
		
		foodtype = FOODTYPE.ELEMENTAL,
		
		perish = TUNING.PERISH_SUPERSLOW,
		
		ingredients = {{"berries", 1}, {"carrot", 1}, {"rocks", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CROQUETTE = {
		id = "013",
		name = "croquette",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 30,
		sanity = 12,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"potato", 2, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	ROASTED_VEGGIES = {
		id = "014",
		name = "roasted_veggies",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 25,
		hunger = 50,
		sanity = 15,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"carrot", 2}, {"potato", 1, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MEATLOAF = {
		id = "015",
		name = "meatloaf",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 15,
		hunger = 60,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"carrot", 1}, {"potato", 1, nil, nil, "quagmire_potato.tex"}, {"smallmeat", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CARROT_SOUP = {
		id = "016",
		name = "carrot_soup",
		plating = "bowl",
		type = "idle", --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 30,
		sanity = 10,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"carrot", 3}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISH_PIE = {
		id = "017",
		name = "fish_pie",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 15,
		hunger = 40,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"pondfish", 1}, {"carrot", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISH_AND_CHIPS = {
		id = "018",
		name = "fish_and_chips",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 35,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"pondfish", 1}, {"potato", 1, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	
	MEAT_PIE = {
		id = "019",
		name = "meat_pie",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 50,
		sanity = 50,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"meat", 1}, {"carrot", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SLIDER = {
		id = "020",
		name = "slider",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 50,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"meat", 1}, {"carrot", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	JAM = {
		id = "021",
		name = "jam",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 1,
		hunger = 25,
		sanity = 1,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"berries", 3}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	JELLY_ROLL = {
		id = "022",
		name = "jelly_roll",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 30,
		sanity = 50,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"berries", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CARROT_CAKE = {
		id = "023",
		name = "carrot_cake",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 60,
		sanity = 70,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}, {"carrot", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MASHED_POTATOES = {
		id = "024",
		name = "mashed_potatoes",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 50,
		sanity = 20,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		ingredients = {{"potato", 2, nil, nil, "quagmire_potato.tex"}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	GARLIC_BREAD = {
		id = "025",
		name = "garlic_bread",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 2,
		hunger = 20,
		sanity = 10,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	TOMATO_SOUP = {
		id = "026",
		name = "tomato_soup",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 30,
		sanity = 15,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"tomato", 3, nil, nil, "quagmire_tomato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SAUSAGE = {
		id = "027",
		name = "sausage",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 50,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"smallmeat", 3}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CANDIED_FISH = {
		id = "028",
		name = "candied_fish",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 35,
		sanity = 35,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"fishmeat_small", 2}, {"gorge_syrupp", 1, nil, nil, "quagmire_syrup.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	STUFFED_MUSHROOM = {
		id = "029",
		name = "stuffed_mushroom",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 30,
		sanity = 25,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"garlic", 1, nil, nil, "quagmire_garlic.tex"}, {"onion", 1, nil, nil, "quagmire_onion.tex"}, {"blue_cap", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	RATATOUILLE = {
		id = "030",
		name = "ratatouille",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 30,
		sanity = 1,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"carrot", 2}, {"tomato", 1, nil, nil, "quagmire_tomato.tex"}, {"onion", 1, nil, nil, "quagmire_onion.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BRUSCHETTA = {
		id = "031",
		name = "bruschetta",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 1,
		hunger = 25,
		sanity = 30,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MEAT_STEW = {
		id = "032",
		name = "meat_stew",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 1,
		hunger = 90,
		sanity = 10,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 1}, {"carrot", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	HAMBURGER = {
		id = "033",
		name = "hamburger",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 50,
		sanity = 40,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 1}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"foliage", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISHBURGER = {
		id = "034",
		name = "fish_burger",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 40,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"pondfish", 2}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"foliage", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MUSHROOMBURGER = {
		id = "035",
		name = "mushroom_burger",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 40,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"blue_cap", 2}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FISH_STEAK = {
		id = "036",
		name = "fish_steak",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 50,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"fishmeat", 1}, {"carrot", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CURRY = {
		id = "037",
		name = "curry",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 25,
		sanity = 40,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"smallmeat", 1}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SPAGHETTI_AND_MEATBALLS = {
		id = "038",
		name = "spaghetti",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 85,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"smallmeat", 1}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	LASANGA = {
		id = "039",
		name = "lasagna",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 0,
		hunger = 40,
		sanity = 0,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 1}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	POACHED_FISH = {
		id = "040",
		name = "poached_fish",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 40,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"pondfish", 1}, {"foliage", 1}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SHEPERDS_PIE = {
		id = "041",
		name = "shepherds_pie",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 5,
		hunger = 40,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 1}, {"carrot", 1}, {"potato", 2, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CANDY = {
		id = "042",
		name = "candy",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = -5,
		hunger = 25,
		sanity = 30,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_syrupp", 3, nil, nil, "quagmire_syrup.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BREAD_PUDDING = {
		id = "043",
		name = "pudding",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = -10,
		hunger = 25,
		sanity = 100,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_syrupp", 1, nil, nil, "quagmire_syrup.tex"}, {"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	WAFFLES = {
		id = "044",
		name = "waffles",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 30,
		sanity = 50,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_syrupp", 1, nil, nil, "quagmire_syrup.tex"}, {"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BERRY_TART = {
		id = "045",
		name = "berry_tart",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 30,
		sanity = 50,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_syrupp", 1, nil, nil, "quagmire_syrup.tex"}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"berries", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MAC_N_CHEESE = {
		id = "046",
		name = "mac_n_cheese",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 40,
		sanity = 10,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BAGEL_AND_FISH = {
		id = "047",
		name = "bagel_n_fish",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 30,
		sanity = 20,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"pondfish", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	GRILLEDCHEESE = {
		id = "048",
		name = "grilled_cheese",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 35,
		sanity = 30,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CREAM_OF_MUSHROOM = {
		id = "049",
		name = "cream_of_mushroom",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 40,
		hunger = 30,
		sanity = 20,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"blue_cap", 2}, {"goatmilk", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	PIEROGIES = {
		id = "050",
		name = "pierogies",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 40,
		hunger = 30,
		sanity = 5,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"potato", 1, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MANICOTTI = {
		id = "051",
		name = "manicotti",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 40,
		hunger = 40,
		sanity = 10,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}, {"tomato", 1, nil, nil, "quagmire_tomato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CHEESEBURGER = {
		id = "052",
		name = "cheeseburger",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 60,
		sanity = 50,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"meat", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FETTUCCINE = {
		id = "053",
		name = "fettuccine",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 50,
		sanity = 20,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 2}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	ONION_SOUP = {
		id = "054",
		name = "onion_soup",
		plating = "bowl",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 80,
		hunger = 30,
		sanity = 30,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"onion", 2, nil, nil, "quagmire_onion.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BREADED_CUTLET = {
		id = "055",
		name = "breaded_cutlet",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 30,
		sanity = 60,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 2}, {"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CREAMY_FISH = {
		id = "056",
		name = "creamy_fish",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 40,
		sanity = 70,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"pondfish", 1}, {"goatmilk", 1}, {"onion", 1, nil, nil, "quagmire_onion.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	PIZZA = {
		id = "057",
		name = "pizza",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 130,
		sanity = 50,
		
		foodtype = FOODTYPE.MEAT,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"tomato", 2, nil, nil, "quagmire_tomato.tex"}},
		
		perish = TUNING.PERISH_FASTISH,
		
		temperature = 20, --adds this to player's current temperature.
		duration = 10, --for temperature
		
		oneat = nil 
	},	
	POT_ROAST = {
		id = "058",
		name = "pot_roast",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 80,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 1}, {"potato", 1, nil, nil, "quagmire_potato.tex"}, {"carrot", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CRAB_CAKE = {
		id = "059",
		name = "crab_cake",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 30,
		hunger = 60,
		sanity = 40,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_crabmeatp", 1, nil, nil, "quagmire_crabmeat.tex"}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"onion", 1, nil, nil, "quagmire_onion.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	STEAK_FRITES = {
		id = "060",
		name = "steak_frites",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 120,
		sanity = 30,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 2}, {"potato", 2, nil, nil, "quagmire_potato.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SHOOTER_SANDWICH = {
		id = "061",
		name = "shooter_sandwich",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 1,
		hunger = 45,
		sanity = 45,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 2}, {"rocks", 1}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	BACON_WRAPPED_MEAT = {
		id = "062",
		name = "bacon_wrapped_meat",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 40,
		sanity = 45,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 2}, {"smallmeat", 2}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},	
	CRAB_ROLL = {
		id = "063",
		name = "crab_roll",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 10,
		hunger = 40,
		sanity = 40,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_crabmeatp", 1, nil, nil, "quagmire_crabmeat.tex"}, {"foliage", 1}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	MEAT_WELLINGTON = {
		id = "064",
		name = "meat_wellington",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 0,
		hunger = 50,
		sanity = 80,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"meat", 2}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"garlic", 1, nil, nil, "quagmire_garlic.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CRAB_RAVIOLI = {
		id = "065",
		name = "crab_ravioli",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 20,
		hunger = 30,
		sanity = 120,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_crabmeatp", 1, nil, nil, "quagmire_crabmeat.tex"}, {"goatmilk", 1}, {"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CARAMEL_CUBE = {
		id = "066",
		name = "caramel_cube",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = -30,
		hunger = 30,
		sanity = 200,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_SLOW,
		
		ingredients = {{"gorge_syrupp", 2, nil, nil, "quagmire_syrup.tex"}, {"goatmilk", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	SCONE = {
		id = "067",
		name = "scone",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = -1,
		hunger = 15,
		sanity = 50,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"berries", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	TRIFLE = {
		id = "068",
		name = "trifle",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 0,
		hunger = 40,
		sanity = 90,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 2, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 1}, {"berries", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	CHEESECAKE = {
		id = "069",
		name = "cheesecake",
		plating = "plate",
		type = nil, --changes the actual plate, like "silver" makes it silver.
		
		health = 40,
		hunger = 30,
		sanity = 100,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FASTISH,
		
		ingredients = {{"gorge_flourp", 1, nil, nil, "quagmire_flour.tex"}, {"goatmilk", 2}, {"berries", 1}},
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
}