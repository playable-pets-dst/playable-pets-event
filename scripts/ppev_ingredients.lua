
return	{
	
	--[[
		PERISH_ONE_DAY = 1*total_day_time*perish_warp,
        PERISH_TWO_DAY = 2*total_day_time*perish_warp,
        PERISH_SUPERFAST = 3*total_day_time*perish_warp,
        PERISH_FAST = 6*total_day_time*perish_warp,
        PERISH_FASTISH = 8*total_day_time*perish_warp,
        PERISH_MED = 10*total_day_time*perish_warp,
        PERISH_SLOW = 15*total_day_time*perish_warp,
        PERISH_PRESERVED = 20*total_day_time*perish_warp,
        PERISH_SUPERSLOW = 40*total_day_time*perish_warp,
	]]
	GOAT_MILK = {
		nameoverride = "quagmire_goatmilk",
		name = "goatmilk",
		bank = "quagmire_goatmilk",
		build = "quagmire_goatmilk",
		anim = "idle",
		
		tags = {"catfood", "quagmire_stewable"},
		
		edible = true,
		
		stackable = 40,
		
		health = 1,
		hunger = 10,
		sanity = 1,
		
		foodtype = FOODTYPE.GENERIC,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil 
	},
	FLOUR = {
		nameoverride = "quagmire_flour",
		name = "flour",
		bank = "quagmire_flour",
		build = "quagmire_flour",
		anim = "idle",
		
		stackable = 40,
		
		tags = {"quagmire_stewable"},
	},
	WHEAT = {
		notassetname = true,
		nameoverride = "quagmire_wheat",
		name = "wheat",
		bank = "quagmire_crop_wheat",
		build = "quagmire_crop_wheat",
		anim = "idle",
		
		stackable = 40,
		
		tags = {},
	},
	SYRUP = {
		nameoverride = "quagmire_syrup",
		name = "syrup",
		bank = "quagmire_syrup",
		build = "quagmire_syrup",
		anim = "idle",
		
		stackable = 10,
		
		tags = {"quagmire_stewable"},
	},
	MUSHROOM = {
		nameoverride = "quagmire_mushrooms",
		name = "mushrooms",
		bank = "quagmire_mushrooms",
		build = "quagmire_mushrooms",
		anim = "raw",
		
		edible = true,
		
		health = 0,
		hunger = 3,
		sanity = 0,
		
		stackable = 40,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil, 
		
		tags = {"cookable","quagmire_stewable"},
		
		cookable = true,
		product = "gorge_mushrooms_cookedp",
	},
	MUSHROOM_COOKED = {
		notassetname = true,
		nameoverride = "quagmire_mushrooms_cooked",
		name = "mushrooms_cooked",
		bank = "quagmire_mushrooms",
		build = "quagmire_mushrooms",
		anim = "cooked",
		
		edible = true,
		
		stackable = 40,
		
		health = 1,
		hunger = 10,
		sanity = 1,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil, 
		
		tags = {"quagmire_stewable"},
	},
	MEATSCRAPS = {
		notassetname = true,
		nameoverride = "quagmire_smallmeat",
		name = "meat_small",
		bank = "quagmire_meat_small",
		build = "quagmire_meat_small",
		anim = "raw",
		
		edible = true,
		
		stackable = 40,
		
		health = 0,
		hunger = TUNING.CALORIES_SMALL,
		sanity = -TUNING.SANITY_SMALL,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil, 
		
		tags = {"cookable","quagmire_stewable"},
		
		cookable = true,
		product = "gorge_meat_small_cookedp",
	},
	MEATSCRAPS_COOKED = {
		notassetname = true,
		nameoverride = "quagmire_cookedsmallmeat",
		name = "meat_small_cooked",
		bank = "quagmire_meat_small",
		build = "quagmire_meat_small",
		anim = "cooked",
		
		stackable = 40,
		
		edible = true,
		
		health = TUNING.HEALING_TINY,
		hunger = TUNING.CALORIES_SMALL,
		sanity = 0,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		oneat = nil, 
		
		tags = {"quagmire_stewable"},
	},
	CRABMEAT = {
		nameoverride = "quagmire_crabmeat",
		name = "crabmeat",
		bank = "quagmire_crabmeat",
		build = "quagmire_crabmeat",
		anim = "idle",
		
		edible = true,
		
		stackable = 40,
		
		health = 0,
		hunger = TUNING.CALORIES_SMALL,
		sanity = -TUNING.SANITY_SMALL,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil, 
		
		tags = {"cookable","quagmire_stewable"},
		
		cookable = true,
		product = "gorge_crabmeat_cookedp",
	},
	CRABMEAT_COOKED = {
		notassetname = true,
		nameoverride = "quagmire_crabmeat_cooked",
		name = "crabmeat_cooked",
		bank = "quagmire_crabmeat",
		build = "quagmire_crabmeat",
		anim = "cooked",
		
		edible = true,
		
		stackable = 40,
		
		health = TUNING.HEALING_TINY,
		hunger = TUNING.CALORIES_SMALL,
		sanity = 0,
		
		foodtype = FOODTYPE.MEAT,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		oneat = nil, 
		
		tags = {"quagmire_stewable"},
	},
	TURNIP = {
		notassetname = true,
		nameoverride = "quagmire_turnip",
		name = "turnip",
		bank = "quagmire_crop_turnip",
		build = "quagmire_crop_turnip",
		anim = "idle",
		
		edible = true,
		
		stackable = 40,
		
		health = 0,
		hunger = TUNING.CALORIES_TINY,
		sanity = 0,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		
		oneat = nil, 
		
		tags = {"cookable","quagmire_stewable"},
		
		cookable = true,
		product = "gorge_turnip_cookedp",
	},
	TURNIP_COOKED = {
		notassetname = true,
		nameoverride = "quagmire_turnip_cooked",
		name = "turnip_cooked",
		bank = "quagmire_crop_turnip",
		build = "quagmire_crop_turnip",
		anim = "cooked",
		
		edible = true,
		
		stackable = 40,
		
		health = TUNING.HEALING_TINY,
		hunger = TUNING.CALORIES_SMALL,
		sanity = 0,
		
		foodtype = FOODTYPE.VEGGIE,
		
		perish = TUNING.PERISH_FAST,
		
		temperature = nil, --adds this to player's current temperature.
		duration = nil, --for temperature
		oneat = nil, 
		
		tags = {"quagmire_stewable"},
	},
}