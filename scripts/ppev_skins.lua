local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	kitcoonp = {
		forest = {
			name = "forest",
			fname = "Forest Kitcoon",
			build = "kitcoon_forest_build",
		},
		marsh = {
			name = "marsh",
			fname = "Marsh Kitcoon",
			build = "kitcoon_marsh_build",
		},
		moon = {
			name = "moon",
			fname = "Moon Kitcoon",
			build = "kitcoon_moon_build",
		},
		grass = {
			name = "grass",
			fname = "Grass Kitcoon",
			build = "kitcoon_grass_build",
		},
		desert = {
			name = "desert",
			fname = "Desert Kitcoon",
			build = "kitcoon_desert_build",
		},
		rocky = {
			name = "rocky",
			fname = "Rocky Kitcoon",
			build = "kitcoon_rocky_build",
		},
		savanna = {
			name = "savanna",
			fname = "Savanna Kitcoon",
			build = "kitcoon_savanna_build",
		},
		yot = {
			name = "yot",
			fname = "Special Kitcoon",
			build = "kitcoon_yot_build",
		},
	},
	crowhostp = {
		odd = {
			name = "odd",
			fname = "Odd Host",
			build = "carnival_host",
			hue = 0.5,
		},
		green = {
			name = "green",
			fname = "Green Host",
			build = "carnival_host",
			hue = 0.7,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Host",
			build = "carnival_host",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	clayhoundp = {
		green = {
			name = "green",
			fname = "Mythril Hound",
			build = "clayhound",
			hue = 0.5,
		},
	},
	golemp = {
		blue = {
			name = "blue",
			fname = "Blue Golem",
			build = "lavaarena_elemental_basic",
			hue = 0.5,
			projectilehue = 0.5
		},
		green = {
			name = "green",
			fname = "Green Golem",
			build = "lavaarena_elemental_basic",
			hue = 0.25,
			projectilehue = 0.25
		},
		purple = {
			name = "purple",
			fname = "Purple Golem",
			build = "lavaarena_elemental_basic",
			hue = 0.65,
			projectilehue = 0.65
		},
		pink = {
			name = "pink",
			fname = "Pink Golem",
			build = "lavaarena_elemental_basic",
			hue = 0.8,
			projectilehue = 0.8
		},
		rgb = {
			name = "rgb",
			fname = "RGB Golem",
			build = "lavaarena_elemental_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
		asunton = {
			name = "asunton",
			fname = "Asunton Golem",
			build = "asunton_golem",
		},
	},
	eyeofterror_minip = {
		friendly = {
			name = "friendly",
			fname = "Friendly Peeper",
			build = "eyeofterror_mini_basic",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Peeper",
			build = "eyeofterror_mini_mob_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	eyeofterrorp = {
		blue = {
			name = "blue",
			fname = "Blue Eye of Cthulu",
			build = "eyeofterror_basic_alt1",
		},
		ret = {
			name = "ret",
			fname = "Retinazer",
			build = "eyeofterror_twin1_build",
		},
		spaz = {
			name = "spaz",
			fname = "Retinazer",
			build = "eyeofterror_twin2_build",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Eye of Cthulu",
			build = "eyeofterror_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	dragonfly_yulep = {
		blue = {
			name = "blue",
			fname = "Odd Dragonfly",
			build = "dragonfly_yule_shiny_build_01",
			rage_build = "dragonfly_fire_yule_shiny_build_01"
		},
		rgb = {
			name = "rgb",
			fname = "RGB Dragonfly",
			build = "dragonfly_yule_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	babygoose_yulep = {

	},
	goose_yulep = {
		odd = {
			name = "odd",
			fname = "Odd Goose/Moose",
			build = "goosemoose_yule_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Goose/Moose",
			build = "goosemoose_yule",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	boaronp = {
		frigid = {
			name = "frigid",
			fname = "Frigid Pitpig",
			build = "boaron_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Pitpig",
			build = "boaron_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Pit Pig",
			build = "lavaarena_boaron_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	snapperp = {
		frigid = {
			name = "frigid",
			fname = "Frigid Crocommander",
			build = "snapper_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Crocommander",
			build = "snapper_shiny_build_03",
		},
		pirate = {
			name = "pirate",
			fname = "Pirate Crocommander",
			build = "snapper_shiny_build_05",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Crocommander",
			build = "lavaarena_snapper_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	goatmomp = {
		odd = {
			name = "odd",
			fname = "Odd Mumsy",
			build = "goatmom_shiny_build_01",
		},
		undertale = {
			name = "undertale",
			fname = "Toriel",
			build = "goatmom_shiny_build_06",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Mumsy",
			build = "quagmire_goatmom_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	goatkidp = {
		undertale = {
			name = "undertale",
			fname = "Asriel",
			build = "goatkid_shiny_build_06",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Billy",
			build = "quagmire_goatkid_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	trailsp = {
		frigid = {
			name = "frigid",
			fname = "Yeti Boarilla",
			build = "trails_shiny_build_01",
		},
		boarbotnik = {
			name = "boarbotnik",
			fname = "Boarbotnik",
			build = "boarbotnik_build",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Boarilla",
			build = "lavaarena_trails_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	tortankp = {
		frigid = {
			name = "frigid",
			fname = "Frigid Snortoise",
			build = "tortank_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Snortoise",
			build = "tortank_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Snortoise",
			build = "lavaarena_turtillus_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
		--Too low quality
		--[[
		odd = {
			name = "odd",
			fname = "???",
			build = "tortank_shiny_build_06",
		},]]
	},
	peghookp = {
		frigid = {
			name = "frigid",
			fname = "Frigid Scorpeon",
			build = "peghook_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Scorpeon",
			build = "peghook_shiny_build_03",
		},
		cultist = {
			name = "cultist",
			fname = "Cult Scorpeon",
			build = "peghook_shiny_build_06",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Scorpeon",
			build = "lavaarena_peghook_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	boarriorp = {
		frigid = {
			name = "frigid",
			fname = "Frigid Boarrior",
			build = "boarrior_shiny_build_01",
		},
		skeleton = {
			name = "skeleton",
			fname = "Skeleton Boarrior",
			build = "boarrior_shiny_build_06",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Boarrior",
			build = "lavaarena_boarrior_basic",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	rhinodrillp = {
		alt = {
			name = "alt",
			fname = "Flatbrim",
			alt = true,
			build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_alt1" or "lavaarena_rhinodrill_basic",
			build2 = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_clothed_alt1" or "lavaarena_rhinodrill_clothed_b_build",
			damaged_build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_damaged_alt1" or "lavaarena_rhinodrill_damaged",
		},
		rf = {
			name = "rf",
			fname = "Reforged Snapback",
			build = "rhinodrill_shiny_build_08",
			build2 = "rhinodrill_clothed_shiny_build_08",
			damaged_build = "rhinodrill_damaged_shiny_build_08",
		},
		rf_alt = {
			name = "rf_alt",
			fname = "Reforged Flatbrim",
			alt = true,
			build = "rhinodrill_shiny_build_08",
			build2 = "rhinodrill_clothed_shiny_build_08",
			damaged_build = "rhinodrill_damaged_shiny_build_08",
		},	
	},
	beetletaurp = {
		dark = {
			name = "dark",
			fname = "Dark Swineclops",
			build = "beetletaur_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Swineclops",
			build = "lavaarena_beetletaur",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	crowkidp = {
		naked = {
			name = "naked",
			fname = "Naked Crow",
			build = "crow_kids",
			scarf = 0,
		},
		red = {
			name = "red",
			fname = "Red Scarf Crow",
			build = "crow_kids",
			scarf = 2,
		},
		green = {
			name = "green",
			fname = "Green Jacket Crow",
			build = "crow_kids",
			scarf = 3,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Jacket Crow",
			build = "crow_kids",
			scarf = 2,
		},
	},
}

return SKINS