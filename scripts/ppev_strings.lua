------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

STRINGS.RECIPE_DESC.BATTLESTANDARD_DAMAGERP = "Increase nearby allies' damage by 25%"
STRINGS.RECIPE_DESC.BATTLESTANDARD_SHIELDP = "Increase nearby allies' defense by 25%"
STRINGS.RECIPE_DESC.BATTLESTANDARD_HEALP = "Periodically heal nearby allies"
STRINGS.RECIPE_DESC.FORGE_BANNER = "Mark your territory!"
STRINGS.RECIPE_DESC.FORGE_FENCE_ITEM = "Small but pointy"

STRINGS.NAMES.GORGE_OVENP = "Fancy Oven"
STRINGS.RECIPE_DESC.GORGE_OVENP = "A better oven."

STRINGS.NAMES.CHEFHATP = "Chef Hat"
STRINGS.RECIPE_DESC.CHEFHATP = "Become a master chef!"

STRINGS.NAMES.GINGERBREAD_PIG_HOUSEP = "Gingerbread House"
STRINGS.RECIPE_DESC.GINGERBREAD_PIG_HOUSEP = "A delicious home for a delicious pig."
STRINGS.RECIPE_DESC.GORGE_MERMHOUSEP = "Better than nothing."

STRINGS.RECIPE_DESC.GORGE_LAMPP = "Light your streets!"
STRINGS.RECIPE_DESC.GORGE_SAFEP = "Protect your valuables!"
STRINGS.RECIPE_DESC.GORGE_KEYP = "Used to unlock your safe."
STRINGS.RECIPE_DESC.GORGE_KEY_PARKP = "Used to unlock your gates."

STRINGS.RECIPE_DESC.GORGE_COIN1P = "Tradable currency."
STRINGS.RECIPE_DESC.GORGE_COIN2P = "Tradable currency."
STRINGS.RECIPE_DESC.GORGE_COIN3P = "Tradable currency."
STRINGS.RECIPE_DESC.GORGE_COIN4P = "Tradable currency."

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES.kitcoonp = "Kitcoon"
STRINGS.CHARACTER_NAMES.kitcoonp = STRINGS.CHARACTER_TITLES.kitcoonp
STRINGS.CHARACTER_DESCRIPTIONS.kitcoonp = "*Is a tiny catcoon\n*Is not very powerful\n*Is incredibly weak..."
STRINGS.CHARACTER_QUOTES.kitcoonp = "They will never love you"

STRINGS.CHARACTER_TITLES.herdling_chickp = "Scrambling Egg"
STRINGS.CHARACTER_NAMES.herdling_chickp = STRINGS.CHARACTER_TITLES.herdling_chickp
STRINGS.CHARACTER_DESCRIPTIONS.herdling_chickp = "*Is a wooden robot\n*Explodes as an attack\n*Is incredibly weak..."
STRINGS.CHARACTER_QUOTES.herdling_chickp = "They were balloon mains in a previous life"

STRINGS.CHARACTER_TITLES.crowkidp = "Crow Kid"
STRINGS.CHARACTER_NAMES.crowkidp = "CROW KID"
STRINGS.CHARACTER_DESCRIPTIONS.crowkidp = "*Is a crow\n*Can fly\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.crowkidp = "Lore of the crows is a complicated thing"

STRINGS.CHARACTER_TITLES.crowhostp = "Corvus Goodfeather"
STRINGS.CHARACTER_NAMES.crowhostp = "Corvus Goodfeather"
STRINGS.CHARACTER_DESCRIPTIONS.crowhostp = "*Is bulky\n*Can fly\n*Can attack"
STRINGS.CHARACTER_QUOTES.crowhostp = "His smug aura is pretty strong"

STRINGS.CHARACTER_TITLES.gingerbread_vargp = "Gingerbread Varg"
STRINGS.CHARACTER_NAMES.gingerbread_vargp = "GINGERBREAD VARG"
STRINGS.CHARACTER_DESCRIPTIONS.gingerbread_vargp = "*Is a hound\n*Is bulky\n*Is delicious"
STRINGS.CHARACTER_QUOTES.gingerbread_vargp = "He wants to eat you as much as you want to eat him"

STRINGS.CHARACTER_TITLES.gingerbread_pigp = "Gingerbread Pig"
STRINGS.CHARACTER_NAMES.gingerbread_pigp = "GINGERBREAD PIG"
STRINGS.CHARACTER_DESCRIPTIONS.gingerbread_pigp = "*Is small and can't attack\n*Can run fast\n*Is delicious"
STRINGS.CHARACTER_QUOTES.gingerbread_pigp = "They have a really rough life"

STRINGS.CHARACTER_TITLES.golemp = "Magma Golem"
STRINGS.CHARACTER_NAMES.golemp = "MAGMA GOLEM"
STRINGS.CHARACTER_DESCRIPTIONS.golemp = "*Is from another realm\n*Can hide with C\n*Has a ranged attack"
STRINGS.CHARACTER_QUOTES.golemp = "Hot Rock"

STRINGS.CHARACTER_TITLES.trails2p = "Trails"
STRINGS.CHARACTER_NAMES.trails2p = "TRAILS"
STRINGS.CHARACTER_DESCRIPTIONS.trails2p = "*Is from another realm\n*Is a wip"
STRINGS.CHARACTER_QUOTES.trails2p = "Biggest bully in the forge"

STRINGS.CHARACTER_TITLES.boaronp = "Pit Pig"
STRINGS.CHARACTER_NAMES.boaronp = "PIT PIG"
STRINGS.CHARACTER_DESCRIPTIONS.boaronp = "*Is from another realm\n*Runs fast\n*Can ram"
STRINGS.CHARACTER_QUOTES.boaronp = "This li'l piggy will do terrible things"

STRINGS.CHARACTER_TITLES.snapperp = "Crocommander"
STRINGS.CHARACTER_NAMES.snapperp = "CROCOMMANDER"
STRINGS.CHARACTER_DESCRIPTIONS.snapperp = "*Is from another realm\n*Can Craft\n*Can Spit"
STRINGS.CHARACTER_QUOTES.snapperp = "Secretly wears crocs when no one is looking"

STRINGS.CHARACTER_TITLES.tortankp = "Snortoises"
STRINGS.CHARACTER_NAMES.tortankp = "SNORTOISES"
STRINGS.CHARACTER_DESCRIPTIONS.tortankp = "*Is from another realm\n*Is super tanky\n*Can roll"
STRINGS.CHARACTER_QUOTES.tortankp = "Klei's attempt at a beyblade"

STRINGS.CHARACTER_TITLES.peghookp = "Scorpeons"
STRINGS.CHARACTER_NAMES.peghookp = "SCORPEONS"
STRINGS.CHARACTER_DESCRIPTIONS.peghookp = "*Is from another realm\n*Attacks faster when below half health\n*Can spit acid on foes"
STRINGS.CHARACTER_QUOTES.peghookp = "Call it poison one more time, I dare ya"

STRINGS.CHARACTER_TITLES.trailsp = "Boarilla"
STRINGS.CHARACTER_NAMES.trailsp = "BOARILLA"
STRINGS.CHARACTER_DESCRIPTIONS.trailsp = "*Is from another realm\n*Is very bulky\n*Has a powerful punch"
STRINGS.CHARACTER_QUOTES.trailsp = "No more monkey buisness"

STRINGS.CHARACTER_TITLES.boarriorp = "Grand Forge Boarrior"
STRINGS.CHARACTER_NAMES.boarriorp = "BOARRIOR"
STRINGS.CHARACTER_DESCRIPTIONS.boarriorp = "*Is from another realm\n*Is a Giant\n*Is a combat expert"
STRINGS.CHARACTER_QUOTES.boarriorp = "A lv. 100 pit pig"

STRINGS.CHARACTER_TITLES.rhinodrillp = "Rhinocebro"
STRINGS.CHARACTER_NAMES.rhinodrillp = "RHINOCEBRO"
STRINGS.CHARACTER_DESCRIPTIONS.rhinodrillp = "*Is from another realm\n*Works well with other Rhinocebros\n*Is a jerk"
STRINGS.CHARACTER_QUOTES.rhinodrillp = "Rhinocebros before Rhinocehoes"

STRINGS.CHARACTER_TITLES.beetletaurp = "Infernal Swineclops"
STRINGS.CHARACTER_NAMES.beetletaurp = "INFERNAL SWINECLOPS"
STRINGS.CHARACTER_DESCRIPTIONS.beetletaurp = "*Is from another realm\n*Is pretty brutal\n*Is a giant"
STRINGS.CHARACTER_QUOTES.beetletaurp = "YOU INFERNAL SWINE"

STRINGS.CHARACTER_TITLES.deerclops_yulep = "Feastclops"
STRINGS.CHARACTER_NAMES.deerclops_yulep = "FEASTCLOPS"
STRINGS.CHARACTER_DESCRIPTIONS.deerclops_yulep = "*Comes during Winterfeast\n*Is a Giant\n*Shoots lasers!"
STRINGS.CHARACTER_QUOTES.deerclops_yulep = "He knows when you're sleeping, he knows where you live..."

STRINGS.CHARACTER_TITLES.goose_yulep = "Jolly Moose/Goose"
STRINGS.CHARACTER_NAMES.goose_yulep = "JOLLY MOOSE/GOOSE"
STRINGS.CHARACTER_DESCRIPTIONS.goose_yulep = "*Comes during Winterfeast\n*Is a Giant\n*Resists the cold..."
STRINGS.CHARACTER_QUOTES.goose_yulep = "HONK"

STRINGS.CHARACTER_TITLES.babygoose_yulep = "Jolly Mossling"
STRINGS.CHARACTER_NAMES.babygoose_yulep = "JOLLY MOSSLING"
STRINGS.CHARACTER_DESCRIPTIONS.babygoose_yulep = "*Comes during Winterfeast\n*Is pretty fast\n*Has a big appetite"
STRINGS.CHARACTER_QUOTES.babygoose_yulep = "Still looks delicious as ever"

STRINGS.CHARACTER_TITLES.bearger_yulep = "Grumpy Bearger"
STRINGS.CHARACTER_NAMES.bearger_yulep = "GRUMPY BEARGER"
STRINGS.CHARACTER_DESCRIPTIONS.bearger_yulep = "*Comes during Winterfeast\n*Is a Giant\n*Resists the cold..."
STRINGS.CHARACTER_QUOTES.bearger_yulep = "Who dares disturb his slumber?!"

STRINGS.CHARACTER_TITLES.dragonfly_yulep = "Feastfly"
STRINGS.CHARACTER_NAMES.dragonfly_yulep = "FEASTFLY"
STRINGS.CHARACTER_DESCRIPTIONS.dragonfly_yulep = "*Comes during Winterfeast\n*Is a Giant\n*Is pretty much the same"
STRINGS.CHARACTER_QUOTES.dragonfly_yulep = "Who will end up being the feast?"

STRINGS.CHARACTER_TITLES.clayhoundp = "Clay Hound"
STRINGS.CHARACTER_NAMES.clayhoundp = "CLAY HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.clayhoundp = "*Comes during the Year of the Varg\n*Is a clay statue\n*Is slightly slower..."
STRINGS.CHARACTER_QUOTES.clayhoundp = "Decorations to die for"

STRINGS.CHARACTER_TITLES.clayvargp = "Clay Varg"
STRINGS.CHARACTER_NAMES.clayvargp = "CLAY VARG"
STRINGS.CHARACTER_DESCRIPTIONS.clayvargp = "*Comes during the Year of the Varg\n*Is a clay statue\n*Cannot summon hounds..."
STRINGS.CHARACTER_QUOTES.clayvargp = "Can only be calmed with sacrifices"

STRINGS.CHARACTER_TITLES.swamppigp = "Swamp Pig"
STRINGS.CHARACTER_NAMES.swamppigp = "SWAMP PIG"
STRINGS.CHARACTER_DESCRIPTIONS.swamppigp = "*Came from the Elder Bog\n*Can craft \n*Is slower than your average pig"
STRINGS.CHARACTER_QUOTES.swamppigp = "A survivor from the plague"

STRINGS.CHARACTER_TITLES.merm_trader1p = "Sammy"
STRINGS.CHARACTER_NAMES.merm_trader1p = "SAMMY"
STRINGS.CHARACTER_DESCRIPTIONS.merm_trader1p = "*Came from the Elder Bog\n*Can craft \n*Big stomach, but his mind is slipping"
STRINGS.CHARACTER_QUOTES.merm_trader1p = "Who was mumzy again?"

STRINGS.CHARACTER_TITLES.merm_trader2p = "Pipton"
STRINGS.CHARACTER_NAMES.merm_trader2p = "PIPTON"
STRINGS.CHARACTER_DESCRIPTIONS.merm_trader2p = "*Came from the Elder Bog\n*Can craft \n*Has his wits, but gets hungry quickly"
STRINGS.CHARACTER_QUOTES.merm_trader2p = "At least the plague didn't take my mustache hoho!"

STRINGS.CHARACTER_TITLES.goatmomp = "Mumsy"
STRINGS.CHARACTER_NAMES.goatmomp = "MUMSY"
STRINGS.CHARACTER_DESCRIPTIONS.goatmomp = "*Came from the Elder Bog\n*Can craft \n*Isn't the offensive type"
STRINGS.CHARACTER_QUOTES.goatmomp = "I'll protect my son!"

STRINGS.CHARACTER_TITLES.goatkidp = "Billy"
STRINGS.CHARACTER_NAMES.goatkidp = "BILLY"
STRINGS.CHARACTER_DESCRIPTIONS.goatkidp = "*Came from the Elder Bog\n*Can craft \n*Is oblivious to the threats around him"
STRINGS.CHARACTER_QUOTES.goatkidp = "Am I boy or a girl? You'll never know!"

STRINGS.CHARACTER_TITLES.pebblecrabp = "Pebble Crab"
STRINGS.CHARACTER_NAMES.pebblecrabp = "PEBBLE CRAB"
STRINGS.CHARACTER_DESCRIPTIONS.pebblecrabp = "*Came from the Elder Bog\n*Is adorable \n*Can hide"
STRINGS.CHARACTER_QUOTES.pebblecrabp = "Rock Lobster's little brother"

STRINGS.CHARACTER_TITLES.pidgeonp = "Pigeon"
STRINGS.CHARACTER_NAMES.pidgeonp = "PIGEON"
STRINGS.CHARACTER_DESCRIPTIONS.pidgeonp = "*Came from the Elder Bog\n*Can fly\n*Can't attack"
STRINGS.CHARACTER_QUOTES.pidgeonp = "The city bird"

STRINGS.CHARACTER_TITLES.pigelite_redp = "Ignatius"
STRINGS.CHARACTER_NAMES.pigelite_redp = "Ignatius"
STRINGS.CHARACTER_DESCRIPTIONS.pigelite_redp = "*One of the Elite 4\n*Can craft \n*Stronger than your average pig"
STRINGS.CHARACTER_QUOTES.pigelite_redp = "GO GO POWER PIGGIES"

STRINGS.CHARACTER_TITLES.pigelite_bluep = "Wade"
STRINGS.CHARACTER_NAMES.pigelite_bluep = "Wade"
STRINGS.CHARACTER_DESCRIPTIONS.pigelite_bluep = "*One of the Elite 4\n*Can craft \n*Stronger than your average pig"
STRINGS.CHARACTER_QUOTES.pigelite_bluep = "GO GO POWER PIGGIES"

STRINGS.CHARACTER_TITLES.pigelite_greenp = "Sawyer"
STRINGS.CHARACTER_NAMES.pigelite_greenp = "Sawyer"
STRINGS.CHARACTER_DESCRIPTIONS.pigelite_greenp = "*One of the Elite 4\n*Can craft \n*Stronger than your average pig"
STRINGS.CHARACTER_QUOTES.pigelite_greenp = "GO GO POWER PIGGIES"

STRINGS.CHARACTER_TITLES.pigelite_whitep = "Dmitri"
STRINGS.CHARACTER_NAMES.pigelite_whitep = "Dmitri"
STRINGS.CHARACTER_DESCRIPTIONS.pigelite_whitep = "*One of the Elite 4\n*Can craft\n*Stronger than your average pig"
STRINGS.CHARACTER_QUOTES.pigelite_whitep = "GO GO POWER PIGGIES"

STRINGS.CHARACTER_TITLES.eyeofterrorp = "Eye of Terror"
STRINGS.CHARACTER_NAMES.eyeofterrorp = "Eye of Terror"
STRINGS.CHARACTER_DESCRIPTIONS.eyeofterrorp = "*Is a Giant\n*Can summon suspicious peepers with V\n*Can fly away with C"
STRINGS.CHARACTER_QUOTES.eyeofterrorp = "He's a good kisser I'm sure"

STRINGS.CHARACTER_TITLES.eyeofterror_minip = "Suspicious Peeper"
STRINGS.CHARACTER_NAMES.eyeofterror_minip = "Suspicious Peeper"
STRINGS.CHARACTER_DESCRIPTIONS.eyeofterror_minip = "*Is a flying monster\n*Moves with its attacks\n*Is kind of cute"
STRINGS.CHARACTER_QUOTES.eyeofterror_minip = "Please don't say it..."
------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------

--Forge
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.boaronp = "*Can do a dash attack\n*Has 85% armor\n*Can be revived twice as quickly\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.snapperp = "*Can spawn banners with C\n*Has 85% armor\n*Revives others twice as fast\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.tortankp = "*Can do a spin when low on health\n*Has 95% armor, but can be flipped\n*Can temporarily shield with C\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.peghookp = "*Can spit acid on enemies\n*Has 85% armor and immune to acid\n*Is slow\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.trailsp = "*Has 90% armor and is a Giant\n*Can roll and jump when damaged\n*Can temporarily shield with C\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.boarriorp = "*The Forge's ex-champion\n*Variety of attacks, spin with C\n*3x slower revive, but always gets 50% health \n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rhinodrillp = "*The Forge's new champions\n*Has 85% armor\n*Reviving non-bros takes 2x longer.\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.beetletaurp = "*Most brutal warrior of the Forge\n*Can temporarily defend with C\n*Gets revived 3x slower\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.golemp = "*Has ranged attacks\n*Auto Revives\n*Can hide with C\n\nExpertise:\n None"
--Forged Forge
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.trails2p = "*Is a giant\n*Has 80% armor\n*Can trail with C\n\nExpertise:\nNone"
--Winterfeast
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.deerclops_yulep = "*Is a Giant\n*Laser attacks stun enemies\n*Strong attacks\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bearger_yulep = "*Is a Giant\n*Can groundpound with X\n*Strong attacks\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.goose_yulep = "*Is a Giant\n*Can debuff and knockback enemies with C\n*No aoes\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.dragonfly_yulep = "*Is a Giant\n*+25% defense buff while calm\n*Enrage with X for 50% more damage, but 25% less defense\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.babygoose_yulep = "*Spins to win\n*Can grab aggro with X\n*Is very fast\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.gingerbread_pigp = "*Can be revived 3x faster\n*+15% faster cooldowns\n*Can't attack\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.gingerbread_vargp = "*Is bulky\n*+15% faster cooldowns\n*Can spread icing with C\n\nExpertise:\nMelee"
--Gorge
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pidgeonp = "*Is very weak\n*Can fly temporarily\n*Can be revived easily\n\nExpertise:\n*Melees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.goatkidp = "*Can't attack\n*Heals others 25% better\n*Is very slow\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.goatmomp = "*Talks enemies to death\n*Is pretty fast\n*Revives others twice as fast\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.merm_trader1p = "*Is a merm\n*Is pretty fast\n*Has a chance to get extra loot\n\nExpertise:\nMelees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.merm_trader2p = "*Is a merm\n*Is pretty fast\n*Has a chance to get extra loot\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.swamppigp = "*Can use any weapon\n*Has a 25% damage bonus\n*Cooldowns take 20% longer\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pebblecrabp = "*Has 95% natural armor\n*Can be revived twice as fast\n*Gains full HP on revive\n\nExpertise:\nNone"

--Clay stuff
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.clayhoundp = "*Has 85% armor\n*Can fossilize with X\n*Is fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.clayvargp = "*Has 90% armor\n*Can fossilize with X\n*Has pet clayhounds\n\nExpertise:\nMelee"
--YotP
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigelite_redp = "*+40% more damage with weapons\n*Can pose with x\n*Gains another +25% damage bonus when low on health\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigelite_bluep = "*-25% self healing\n*Can pose with x\n*Cannot be stunned while in heals\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigelite_greenp = "*+25% defense while healing\n*Can pose with x\n*+50% def bonus when close to losing\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigelite_whitep = "*+40% more damage with weapons\n*Can pose with x\n*Fossilize spells are stronger\n\nExpertise:\nBooks, Staves"

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.NAMES.UNKNOWNP = "???"

STRINGS.NAMES.BOARONP = "Pit Pig"
STRINGS.NAMES.BOARON_P = "Pit Pig"

STRINGS.NAMES.TRAIL2SPIT = "Trails Spit"
STRINGS.NAMES.TRAIL2FIRE = "Trails"

STRINGS.NAMES.EVMONSTER_WPN = "Mysterious Power"

STRINGS.NAMES.FORGE_FENCE = "Forge Fence"
STRINGS.NAMES.FORGE_FENCE_ITEM = "Forge Fence"
STRINGS.NAMES.FORGE_BANNER = "Forge Banner"

STRINGS.NAMES.FORGE_BANNER = "Forge Banner"

STRINGS.NAMES.BATTLESTANDARD_DAMAGERP = "War Battlestand"
STRINGS.NAMES.BATTLESTANDARD_SHIELDP = "Defender Battlestand"
STRINGS.NAMES.BATTLESTANDARD_HEALP = "Medic Battlestand"

STRINGS.NAMES.GORGE_FENCEP = "Iron Fence"
STRINGS.NAMES.GORGE_FENCEP_ITEM = "Iron Fence"

STRINGS.NAMES.GORGE_LAMPP = "Streetlight"
STRINGS.NAMES.GORGE_LAMP2P = "Small Streetlight"
STRINGS.NAMES.GORGE_SAFEP = "Iron Safe"
STRINGS.NAMES.GORGE_KEYP = "Safe Key"
STRINGS.NAMES.GORGE_KEY_PARKP = "Gate Key"

STRINGS.NAMES.GORGE_GATEP = "Iron Gate"
STRINGS.NAMES.GORGE_GATEP_ITEM = "Iron Gate"

STRINGS.NAMES.GORGE_MERMHOUSEP = "Ruined House"
STRINGS.NAMES.GORGE_PIGHOUSEP = "Swamp Hut"

STRINGS.NAMES.GORGE_COIN1P = "Old Coin"
STRINGS.NAMES.GORGE_COIN2P = "Sapphire Medallion"
STRINGS.NAMES.GORGE_COIN3P = "Red Mark"
STRINGS.NAMES.GORGE_COIN4P = "Gnaw's Favor"

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

STRINGS.CHARACTERS.PIGELITE_BLUEP = require "speech_pigmanplayer"
STRINGS.CHARACTERS.PIGELITE_REDP = require "speech_pigmanplayer"
STRINGS.CHARACTERS.PIGELITE_WHITEP = require "speech_pigmanplayer"
STRINGS.CHARACTERS.PIGELITE_GREENP = require "speech_pigmanplayer"

STRINGS.CHARACTERS.CROWKIDP = STRINGS.CHARACTERS.GENERIC
STRINGS.CHARACTERS.CROWHOSTP = STRINGS.CHARACTERS.GENERIC

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

--STRINGS.CHARACTERS.GENERIC.DESCRIBE.EVMONSTER_WPN = 
--{
	--GENERIC = "I shouldn't have this",
--}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CHEFHATP = 
{
	GENERIC = "Wearing it makes us better cooks",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_OVENP = 
{
	GENERIC = "Finally, we can cook some good food",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BOARON_P = 
{
	GENERIC = "How did that thing get here?",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BATTLESTANDARD_DAMAGERP = 
{
	GENERIC = "Its emitting some sort of aura",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BATTLESTANDARD_SHIELDP = 
{
	GENERIC = "Its emitting some sort of aura",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BATTLESTANDARD_HEALP = 
{
	GENERIC = "Its emitting some sort of aura",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FORGE_FENCE = 
{
	GENERIC = "Maybe we shouldn't stick around to meet whoever built these",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FORGE_FENCE_ITEM = 
{
	GENERIC = "Used to mark Forge borders",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_FENCEP = 
{
	GENERIC = "Fancy!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_GATEP = 
{
	GENERIC = "Fancy!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_FENCEP_ITEM = 
{
	GENERIC = "Fancy!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_GATEP_ITEM = 
{
	GENERIC = "Fancy!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.COIN1P = 
{
	GENERIC = "Oooo, a penny!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.COIN2P = 
{
	GENERIC = "Probably worth a bit",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.COIN3P = 
{
	GENERIC = "Its menacing with its wealth",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.COIN4P = 
{
	GENERIC = "A treasure from Gnaw itself",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FORGE_BANNER = 
{
	GENERIC = "Usually means we shouldn't tresspass",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_MERMHOUSEP = 
{
	GENERIC = "Its seen better days",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GORGE_PIGHOUSEP = 
{
	GENERIC = "Not something you see very often",
	BURNT = "No roasted pig inside, shame",
}

------------------------------------------------------------------
-- Something Different skin strings
------------------------------------------------------------------

--STRINGS.SKIN_QUOTES.houndplayer_1 = "The Rare Varg Hound"
--STRINGS.SKIN_NAMES.houndplayer_1 = "Varg Hound"

--"Community"--


------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS