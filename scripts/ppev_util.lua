PlayablePets = {}
PlayablePets.ConfigData = PlayablePets.ConfigData or {}

local SETTING = require("ppev_settings")
local MOBTYPE = require("ppev_mobtype")

-- Used to call the function listed as a key in this table when the corresponding keyboard code is detected (key pressed)
local RPCKEYS =
{
	SetSleep_Event = 122, -- Z
	SpecialAbility1_Event = 120, -- X
	SpecialAbility2_Event = 99, -- C
	--SetRage = 111, -- O - Unused because of crashes
	ChangeCharacter = 112, -- P
}

------------------------------------------------------------------
-- Table utility functions
------------------------------------------------------------------

--[[
local next = next
function table.isEmpty(t)
	if next(t) == nil then
		return true
	else
		return false
	end
end

function table.clear(t)
	for k in pairs (table) do
			t[k] = nil
	end
end
--]]

function table.contains(t, element)
   --contact Phaze, t is nil when loaded via caves. 
  if t ~= nil then	
  for _, value in pairs(t) do
		if value == element then
			return true
		end
  end
  else
	
  return false
  end
end

-- Append the contents of t2 to t1
--function table.concat(t1, t2)
function TableConcat(t1, t2)
	for i=1,#t2 do
		t1[#t1+1] = t2[i]
	end
	
	return t1
end

------------------------------------------------------------------
-- Settings code
------------------------------------------------------------------

local configuratorModName = "Pet Configuration Manager"
local configuratorModEnabled = KnownModIndex:IsModEnabled(configuratorModName)

for _, mod in ipairs(KnownModIndex:GetModsToLoad()) do
	if mod == configuratorModName then
		configuratorModEnabled = true
	end
end

-- Retrieve the global Playable Pets configuration settings, if applicable
if configuratorModEnabled then
	local modConfig = KnownModIndex:LoadModConfigurationOptions(configuratorModName, false)
	
	-- Build a more convenient global configuration table to access, indexed by option name
	if modConfig and type(modConfig) == "table" then
		for k, v in ipairs(modConfig) do
			PlayablePets.ConfigData[v.name] = {}
			PlayablePets.ConfigData[v.name].default = v.default
			PlayablePets.ConfigData[v.name].saved = v.saved
		end
	end
end

------------------------------------------------------------------
-- Configuration functions
------------------------------------------------------------------

-- The configurator mod is intended to override all individual Playable Pets mods, if an option is set to
-- This function will first consult the configurator settings for an option, only using the local
--  setting if both the saved and default options are set to not override.
PlayablePets.GetModConfigData = function(optionName)
	local configData = PlayablePets.ConfigData[optionName]
	local optionValue = nil
	
	--local dataDump = DataDumper(PlayablePets.ConfigData, nil, false)
	
	-- If the configurator option isn't set to off, use it
	-- Otherwise, use the default if it isn't off
	if configData ~= nil then
		if configData.saved ~= nil and configData.saved ~= SETTING.OFF then
			optionValue = configData.saved
		elseif configData.default  ~= SETTING.OFF then
			optionValue = configData.default
		end
	end
	
	-- If the configurator option was set to override the local mod, use it
	-- Otherwise, get the local mod's option for the setting
	-- Forced to use function GetModConfigData(optionname, SETTING.MODNAME, get_local_config) due to this not being in modmain.lua
	--return optionValue ~= nil and optionValue or GetModConfigData(optionName)
	return optionValue ~= nil and optionValue or GetModConfigData(optionName, SETTING.MODNAME, false)
end

-- Consults various Configurator preset settings in addition to the default mod's setting for an individual mob to determine
--  whether or not the mob is enabled, and if various actions (like adding to the character select or skins) should be undertaken
function PlayablePets.MobEnabled(prefab)
	local mobs = PP_MobCharacters
	local MOBPRESET = PlayablePets.GetModConfigData("MobPreset")
	
	-- Spare performing all of the table and setting checks if the mob table already has the setting defined
	if mobs[prefab].enabled ~= nil then
		return mobs[prefab].enabled
	end
	
	local mobEnabled = PlayablePets.GetModConfigData(mobs[prefab].fancyname) == SETTING.ENABLE
	local isGiant = table.contains(mobs[prefab].mobtype, MOBTYPE.GIANT)
	local isBoss = table.contains(mobs[prefab].mobtype, MOBTYPE.BOSS)
	local isCrafty = table.contains(mobs[prefab].mobtype, MOBTYPE.CRAFTY)
	
	if ( (MOBPRESET == nil or MOBPRESET == SETTING.OFF) and mobEnabled )
	or ( MOBPRESET == SETTING.ALL_MOBS )
	or ( MOBPRESET == SETTING.GIANTS_ONLY and isGiant)
	or ( MOBPRESET == SETTING.BOSSES_ONLY and isBoss )
	or ( MOBPRESET == SETTING.CRAFTY_MOBS and isCrafty ) then
		mobs[prefab].enabled = true
	end
	
	if ( MOBPRESET == SETTING.NO_GIANTS and isGiant )
	or ( MOBPRESET == SETTING.NO_BOSSES and isBoss )
	or ( MOBPRESET == SETTING.CRAFTY_MOBS and not isCrafty ) then
		mobs[prefab].enabled = false
	end
	
	return mobs[prefab].enabled
end

------------------------------------------------------------------
-- Skin function
------------------------------------------------------------------

--This won't be used because mobs will have weird names or have special conditions
PlayablePets.SetSkin = function(inst, mob, wardrobe)
	if not inst:HasTag("playerghost") then
		inst.components.ppskin_manager:LoadSkin(mob, wardrobe)
	end	
end

------------------------------------------------------------------
-- RPC Keyhandler functions
------------------------------------------------------------------

--Checks to see if there is any type-able pop ups. Prevents RPCs from being activated.
local function IsDefaultScreen()
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name and type(TheFrontEnd:GetActiveScreen().name) == "string" and TheFrontEnd:GetActiveScreen().name == "HUD" then
		return true
	else
		return false
	end
end

-- Allows a player mob to sleep. Does what it says on the tin
PlayablePets.SetSleep_Event = function(player)

	if player.mobsleep_event ~= nil and player.mobsleep_event == true and player.sg:HasStateTag("sleeping") then
		player.sg:GoToState("wake")	
	elseif player.mobsleep_event ~= nil and player.mobsleep_event == true and not player.sg:HasStateTag("busy") and not player.sg:HasStateTag("sleeping") and not player.sg:HasStateTag("specialsleep") then
		player.sg:GoToState("sleep")	
	end
	
	if player.specialsleepev ~= nil and player.specialsleepev == true and not player.sg:HasStateTag("busy") then
		player.sg:GoToState("special_sleepev")
	elseif player.specialsleepev ~= nil and player.specialsleepev == false and not player.sg:HasStateTag("busy") then
		player.sg:GoToState("special_wakeev")
	end	
end

-- Performs a special abillity, usually a taunt, when the "x" key is pressed.
PlayablePets.SpecialAbility1_Event = function(player)
	if not player.sg:HasStateTag("busy") then
		if player.taunt_event and player.taunt_event == true and not player.sg:HasStateTag("busy") then
			player.sg:GoToState("special_atk1")
		end
		
		if player.isdragonrage_event ~= nil then
			if player.isdragonrage_event == true then
				player.sg:GoToState("transform_normal")
			else
				player.sg:GoToState("transform_fire")
			end
		end
	end
end

local function CheckWaterTile(inst)
	if SW_PP_MODE == true then
		return inst:GetIsOnWater() and true or false
	else
		return false
	end
end

-- Performs a secondary special abillity when "c" is pressed.
PlayablePets.SpecialAbility2_Event = function(player)

	if player.hider_event and player.hider_event == true and player.sg:HasStateTag("hiding") then
		player.sg:GoToState("hide_pst")
	elseif player.hider_event and player.hider_event == true and not player.sg:HasStateTag("hiding") and not player.sg:HasStateTag("busy") then
		player.sg:GoToState("special_atk2")
	end
			
	if not player.sg:HasStateTag("busy") then
		--if not player:HasTag("nospecial") then
				
				if player.taunt2_event and player.taunt2_event == true and not player.sg:HasStateTag("sleeping") and not player.sg:HasStateTag("busy") then
					player.sg:GoToState("special_atk2")
				end
			
			if player.israged ~= nil then
				player.sg:GoToState("rage")
			end
			
			
			
	end
	
	--for birds.
		if player._isflying ~= nil and player._isflying == true and not player.sg:HasStateTag("busy") and (not player._canlandinsea and TheWorld.Map:IsAboveGroundAtPoint(player:GetPosition():Get()) and CheckWaterTile(player) == false)  then
			player.sg:GoToState("land")
		end
end

--Supposed to send the player into "Rage" mode when "o" is pressed. Currently unused.
--[[
PlayablePets.SetRage = function(player) --Causes crash when it runs states with the "busy" Tag. Maybe try to add tags with ThePlayer instead.
	if inst.raged ~= nil and not player.sg:HasStateTag("busy") then
			--if inst.israged ~= nil then
				if inst.israged == true then
					inst.israged == false
				else
					inst.israged = true
				end
			--end
		end
	end
]]

-- Sends the player to the character select screen when "P" is pressed
PlayablePets.ChangeCharacter = function(player)
	if PlayablePets.GetModConfigData("Mobchange_Event") == SETTING.ENABLE and TheNet:GetServerGameMode() ~= "lavaarena" then
		if not player.sg:HasStateTag("busy") and not (player.isdespawning or player.isdespawning == true) then
		player.isdespawning = true
		local portal = FindEntity(player, 9001, nil, {"multiplayer_portal"}) or nil
		if portal and portal._savedata then
			portal._savedata[player.userid] = player:SaveForReroll()
		end
			player.components.inventory:DropEverything(true)
			player:DoTaskInTime(0.3, function(inst) TheWorld:PushEvent("ms_playerdespawnanddelete", inst) end)
		end
	end
end

------------------------------------------------------------------
--Common Functions
------------------------------------------------------------------
local SEASON_COLOURCUBES =
{
    autumn = "day05_cc.tex",
    winter = "snow_cc.tex",
    spring = "spring_day_cc.tex",
    summer = "summer_day_cc.tex",
}

local function GetColourCube()
	if not TheWorld:HasTag("cave") then
		if TheWorld.state.iswinter then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["winter"]
		elseif TheWorld.state.issummer then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["summer"]
		elseif TheWorld.state.isautumn then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["autumn"]
		elseif TheWorld.state.isspring then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["spring"]
		else
			return "images/colour_cubes/ruins_light_cc.tex"
		end
	else
		return "images/colour_cubes/ruins_light_cc.tex"
	end
end

local NIGHTVISION_COLOURCUBES =
{
    day = "images/colour_cubes/ruins_light_cc.tex",
    dusk = "images/colour_cubes/ruins_dim_cc.tex",
    night = "images/colour_cubes/ruins_light_cc.tex",
    full_moon = "images/colour_cubes/purple_moon_cc.tex",
}

PlayablePets.SetNightVision = function(inst, enable, cctable) 
    if (TheWorld.state.isnight or TheWorld:HasTag("cave")) and MOBNIGHTVISION == 0 then
        inst.components.playervision:ForceNightVision(true)
		--NIGHTVISION_COLOURCUBES.night = GetColourCube()
		inst.components.playervision:SetCustomCCTable(NIGHTVISION_COLOURCUBES)
		--TheWorld:PushEvent("overrideambientlighting", Point(0.25, 0.25, 0.25))
    else
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)
		--TheWorld:PushEvent("overrideambientlighting", nil)
    end
end

local function GetAttackedMultiplier(inst)
	if inst.components.combat then
		local lastattacked = inst.components.combat.lastwasattackedtime
		local currenttime = GetTime()
		if (lastattacked + 20) < currenttime then
			return 1
		elseif (lastattacked + 16)	< currenttime then
			return 0.66
		elseif (lastattacked + 12) < currenttime then
			return 0.25
		elseif (lastattacked + 8) < currenttime then
			return 0.05
		else
			return 0
		end
	else
		return 1
	end
end

PlayablePets.SleepHeal = function(inst, forced)
	if not forced and SLEEP_REGEN > 0 then
		local health = inst.components.health
		local combat = inst.components.combat
		if inst.components.hunger and not inst.components.hunger:IsStarving() then
			inst.components.health:DoDelta( (health.maxhealth/12) * GetAttackedMultiplier(inst) * SLEEP_REGEN_EVENT, true)
		end	
		if inst.components.health:GetPercent() < 1 then
			inst.components.hunger:DoDelta(-(inst.components.hunger.max * 0.055) * GetAttackedMultiplier(inst), true)
		end
	end
end

local valid_actions = {ACTIONS.HAMMER, ACTIONS.MINE, ACTIONS.CHOP}

local function IsWorkAction(inst)
	if inst.bufferedaction then
		for i, v in ipairs(valid_actions) do
			if inst.bufferedaction.action == v then
				return true
			end
		end
	else
		return false
	end
end

PlayablePets.DoWork = function (inst, strength)
	if inst.bufferedaction and IsWorkAction(inst) and inst.bufferedaction.target and inst.bufferedaction.target.components.workable then
		local worktar = inst.bufferedaction.target.components.workable
		worktar:SetWorkLeft(math.max(worktar.workleft - (strength - 1), 1)) --subtract one from strength because we perform an action to count as one.
	end
	inst:PerformBufferedAction()
end

PlayablePets.CommonOnEquip = function(inst)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	--TODO: Add monster_wpn support here? even though I loathe it?
end

local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

PlayablePets.KnockbackOther = function(inst, other, radius) --knockback
		if other.sg and other.sg.sg.events.knockback then
			other:PushEvent("knockback", {knocker = inst, radius = radius or 5})
		else
			if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
		
				if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
					if other.components.combat ~= nil then
						if other.Physics ~= nil then
							local x, y, z = inst.Transform:GetWorldPosition()
							local distsq = other:GetDistanceSqToPoint(x, 0, z)
					--if distsq < REPEL_RADIUS_SQ then
							if distsq > 0 then
								other:ForceFacePoint(x, 0, z)
							end
							local k = .5 * distsq / REPEL_RADIUS_SQ - 1
							other.speed = 60 * k
							other.dspeed = 2
							other.Physics:ClearMotorVelOverride()
							other.Physics:Stop()
						
							if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
								--Leo: Need to change this to check for bodyslot for these tags.
								other:DoTaskInTime(0.1, function(inst) 
								other.Physics:SetMotorVelOverride(-1, 0, 0) 
								end)
							else
								other:DoTaskInTime(0, function(inst) 
								other.Physics:SetMotorVelOverride(-3, 0, 0) 
								end)
							end
							other:DoTaskInTime(0.4, function(inst) 
							other.Physics:ClearMotorVelOverride()
							other.Physics:Stop()
							end)
						end
					end
				end
			end
		end
end

PlayablePets.GetAllyHelp = function(inst, target, tag)
	if target and tag and not (target.components.playercontroller and target:HasTag(tag)) then
		inst.components.combat:SetTarget(target)
		inst.components.combat:ShareTarget(target, 30, function(dude) return dude:HasTag(tag) and not dude.components.health:IsDead() end, 30)
	end
end

--SetChar is seperate from SetCommonStats, this gets called by things like setskindefault or "respawning from ghost" events. SetCommonStats is for masterpostinit almost exclusively.
PlayablePets.CommonSetChar = function(inst, mob, ignorehunger)
	if not inst:HasTag("playerghost") then
		inst.AnimState:SetBank(mob.bank)
		inst.AnimState:SetBuild(mob.build)
		inst:SetStateGraph(mob.stategraph)
		if MONSTERHUNGER == "Disable" and not ignorehunger then
			inst.components.hunger:Pause()
		end
		if inst.noplatformhopping then
			inst.components.locomotor:SetAllowPlatformHopping(false)
		end
	end
end

--TODO: Make a growable pets version?
PlayablePets.SetCommonStats = function(inst, mob, ishuman, ignorepvpmultiplier)
	if mob then
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.hunger:SetMax(mob.hunger)
		inst.components.sanity:SetMax(mob.sanity)
		inst.components.hunger:SetRate(mob.hungerrate)
		inst.components.hunger.hurtrate = mob.health / TUNING.STARVE_KILL_TIME
		
		if MONSTERHUNGER == "Disable" then
			inst.components.hunger:Pause()
		end
		
		--Combat--
		inst.components.combat.playerdamagepercent = MOBPVP_EVENT
		if ignorepvpmultiplier then 
			inst.components.combat.pvp_damagemod = 1 --this is for mobs that don't do double damage against non-playable characters.
		end		
		inst.components.combat:SetAttackPeriod(mob.attackperiod)
		inst.components.combat:SetRange(mob.range, mob.hit_range)
		inst.components.combat:SetDefaultDamage(mob.damage) --does double damage against mobs.
		
		inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
		
		inst.AnimState:SetBank(mob.bank)
		inst.AnimState:SetBuild(mob.build)
		inst:SetStateGraph(mob.stategraph)
		
		inst.components.locomotor.runspeed = (mob.runspeed)
		inst.components.locomotor.walkspeed = (mob.walkspeed)

		--Variables
		inst.mobplayer = true
		inst.ghostbuild = "ghost_monster_build"
		inst.mob_table = mob --could be useful for outside sources?
		
		if not inst.isshiny then --I don't think this check is needed since this gets overrided by onload stuff anyways, but just incase this gets used somewhere outside of masterpostinit...
			inst.isshiny = 0 --for identifying skins on the mob
		end
		
		if not ishuman then
			inst.components.talker:IgnoreAll()
			inst.components.sanity.ignore = true
			inst.components.locomotor.fasteronroad = false
			if MOBNIGHTVISION < 2  then
				inst.components.grue:AddImmunity("mobplayer")
			end
		else
			inst.ishuman = true
		end
	end
	if not inst.components.ppskin_manager then
		inst:AddComponent("ppskin_manager")
	end
end

local function SetLandHealthDrain(inst)
	if inst.landdraintask then
		inst.landdraintask:Cancel()
		inst.landdraintask = nil
	end
	inst.landdraintask = inst:DoPeriodicTask(0.75, function(inst) 
		local pos = inst:GetPosition()
		--doesn't count boat incase someone tries to get a cheesy boat kill.
		if not inst.landprotected and TheWorld.Map:IsVisualGroundAtPoint(pos.x, pos.y, pos.z) and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.y, pos.z) then
			inst.components.health:DoDelta(-inst.components.health.maxhealth/16, false)
		end
	end)
end

local function getrandomposition(inst)
		--print("DEBUG: getrandomposition running")
		local pt = inst:GetPosition()
		local ground = TheWorld
		local range = 900
        local result_offset = FindValidPositionByFan(range, range, range, function(offset)
            local test_point = pt + offset
			local tile = ground.Map:GetTileAtPoint(test_point.x, 0, test_point.z)
			--print("Test Points: "..test_point.x..", "..test_point.z)
			if tile == GROUND.OCEAN_ROUGH and not TheWorld.Map:IsVisualGroundAtPoint(test_point.x, test_point.y, test_point.z) and not TheWorld.Map:GetPlatformAtPoint(test_point.x, test_point.y, test_point.z) then
              return true
            end
            return false 
          end)
        local newpt = result_offset and pt + result_offset or nil
		--print(newpt)
		return newpt
end

local function SendToSea(inst)
	--print("DEBUG: SendToSea running")
	local ground = TheWorld
	if TheWorld.Map:IsVisualGroundAtPoint(inst:GetPosition():Get()) or TheWorld.Map:GetPlatformAtPoint(inst:GetPosition():Get()) then
	--print("DEBUG: Oceantile check passed")
    local pt = getrandomposition(inst)--Point(inst.Transform:GetWorldPosition())
		if pt ~= nil then
            pt.y = 0
            inst.Physics:Stop()
				
            inst.Physics:Teleport(pt.x,pt.y,pt.z)
        else    --inst:Hide()
			print("PP Event Error: Couldn't find an ocean position for "..inst:GetDisplayName().." as "..inst.prefab.."!")
		end	
	end
	inst.landprotected = nil
end

PlayablePets.SetAmphibious = function(inst, bank, bank_water, simple, oceanonly, noplatformhopping)
	inst.components.locomotor:SetAllowPlatformHopping(false)
	inst.noplatformhopping = true
	if TheNet:GetServerGameMode() ~= "lavaarena" and not TheWorld:HasTag("cave") then
		if not simple then --ghosts and such don't need this component
			if noplatformhopping == nil then
				inst.noplatformhopping = nil
				inst.components.locomotor:SetAllowPlatformHopping(true)
			end	
			inst:AddComponent("amphibiouscreature")
			inst.components.amphibiouscreature:SetBanks(bank, bank_water)
		end
		inst.components.drownable.enabled = false
		inst.amphibious = true
		if oceanonly then
			inst.components.locomotor:SetAllowPlatformHopping(false)
			inst.oceanonly = true
			inst.landprotected = true
			SetLandHealthDrain(inst)
			inst:DoTaskInTime(0, function(inst)
				SendToSea(inst)
			end)
		else
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end
	end
end

PlayablePets.SetCommonLootdropper = function (inst, prefaboverride)
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefaboverride and prefaboverride or inst.prefab)
end

PlayablePets.SetCommonWeatherResistances = function (inst, heat, cold, wetness)
	if inst.components.temperature then
		if heat then
			inst.components.temperature.maxtemp = heat --prevents overheating
		end
		if cold then
			inst.components.temperature.mintemp = cold --prevents freezing
		end
		if wetness then
			inst.components.moisture:SetInherentWaterproofness(1)
		end
	end
end

PlayablePets.SetCommonStatResistances = function (inst, fire_mult, acid_mult, poison_mult, freeze)
	if fire_mult then
		inst.components.health.fire_damage_scale = fire_mult --this really only accounts for damage caused by fire, not fire attacks
	end
	if acid_mult then
		inst.acidmult = acid_mult 
	end
	
	if poison_mult then --if you're looking to make it immune to poison use SetPoisonImmune instead
		inst.poisonmult = poison_mult
	end
	
	if freeze and inst.components.freezable then
		inst.components.freezable:SetResistance(freeze)
	end
end



PlayablePets.SetPoisonImmune = function(inst, immune)
	if immune and immune == true then
		inst.poisonimmune = true
		if SW_PP_MODE == true then
			--IA's poison component, this should work...
			if inst.components.poisonable then
				inst.components.poisonable.blockall = true
			end
		end
	end
end

--Posion v4.1: To keep track of what version this is.
local function CalcPoisonDamage(inst)
	if inst.components.health then
		local mult = inst.poisonmult ~= nil and inst.poisonmult or 1
		local issleeping = (inst.sg and inst.sg:HasStateTag("sleeping")) and 0.5 or 1
		local health = inst.components.health.maxhealth
		if health <= 1000 then 
			return health * 0.3 / -8 * mult * issleeping--Will take up 30% of the user's health.		
		else
		
			return health * (TheNet:GetServerGameMode() == "lavaarena" and 0.05 or 0.15) / -8 * mult * issleeping--Giants get reduced damage
		end
	else
		print("Poison Error: Target's health is nil! Setting tick damage to 4")
		return -4
	
	end	
end

local function DoPoison(inst)
if inst.ispoisoned and not inst:HasTag("playerghost") then 
	if inst and inst.components.health and not inst.components.health:IsDead() and not inst:HasTag("structure") then
		
		if math.random()<0.20 then
			local fx = SpawnPrefab("poisonbubblep_short")
			fx.Transform:SetPosition(inst:GetPosition():Get())
			if fx and (inst:HasTag("player") or inst.poison_symbol) then
				--players for some reason can't be a parent, but supposedly this method works.
				fx.entity:AddFollower()
				fx.Follower:FollowSymbol(inst.GUID, inst.poison_symbol and inst.poison_symbol or "torso", 0, 0, 0)		
			else
				--for mobs
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(0,0.1,0)
			end
			if inst:HasTag("largecreature") or inst:HasTag("epic") then
				fx.Transform:SetScale(3, 3, 3)
			end
		end	
		if inst:HasTag("player") and not (inst:HasTag("mobplayer") or 
		inst:HasTag("mobplayersw") or 
		inst:HasTag("mobplayercu") or 
		inst:HasTag("mobplayercave") or inst.mobplayer == true) then


			inst.components.health:DoDelta(-0.5, true, "poison")
			inst.components.sanity:DoDelta(-1, true, "poison") --shouldn't need to check this since players will always have sanity.
		else
			inst.components.health:DoDelta(CalcPoisonDamage(inst), true, "poison") --do more damage on mobs.
			--print("DEBUG: Poison DMG = "..CalcPoisonDamage(inst))
		end
		--Normal Poison: 0.5 health per 2 secs. 120 in one day.
		--Strong Poison: 1 health per 2 secs. 240 in one day.


	inst:DoTaskInTime(2, function(inst) 
		if inst:HasTag("poisoned") then --check to make sure the colors aren't messed up when cured.
			inst.AnimState:SetMultColour(0.5,1,0.2,1) 
		end 
	end) 

	
	end
else --you're cured!
	--inst:RemoveTag("poisoned")
	inst.ispoisoned = nil
	inst.AnimState:SetMultColour(1,1,1,1)
		if inst.poisontask ~= nil then
			inst.poisontask = nil
		end
	end
end

PlayablePets.SetPoison = function(inst, other, duration, perma) --Poison workaround.
	if other and not other.poisonimmune and other.components.health and not other.components.health:IsDead() then
		--other:AddTag("poisoned")
		other.ispoisoned = true
		if other.poisontask == nil then
			other.AnimState:SetMultColour(0.5,1,0.2,1)
			other.poisontask = other:DoPeriodicTask(2, DoPoison)
			if not other:HasTag("player") or other.mobplayer and not perma then --Players need to craft a cure, so mob players will have theirs cured intime.
				other.poisonwearofftask = other:DoTaskInTime(duration and duration or 16, function(inst) 
				if inst.poisontask then inst.poisontask:Cancel() 
					inst.poisontask = nil 
					inst.AnimState:SetMultColour(1,1,1,1) 
							--inst:RemoveTag("poisoned") 
					inst.ispoisoned = nil
				end 
				end)
			end
		elseif other.poisontask and other.poisonwearofftask then --if already poisoned, reset timer.
			other.poisonwearofftask:Cancel()
			other.poisonwearofftask = other:DoTaskInTime(duration and duration or 16, function(inst) 
				if inst.poisontask then inst.poisontask:Cancel() 
					inst.poisontask = nil 
					inst.AnimState:SetMultColour(1,1,1,1) 
							--inst:RemoveTag("poisoned") 
					inst.ispoisoned = nil
				end 
			end)
		end
	end
end	

------------------------------------------------------------------
-- Common Forge Functions
------------------------------------------------------------------
PlayablePets.SetForgeStats = function(inst, MOB) 
	if MOB then
		inst.components.health:SetMaxHealth(MOB.HEALTH)
	
		inst.components.combat:SetDefaultDamage(MOB.DAMAGE)
		inst.components.combat:SetAttackPeriod(MOB.ATTACKPERIOD)
	
		inst.components.locomotor.walkspeed = MOB.WALKSPEED
		inst.components.locomotor.runspeed = MOB.RUNSPEED
	
		inst.components.combat:SetRange(MOB.ATTACK_RANGE, MOB.HIT_RANGE)
		if MOB.AOE_RANGE and MOB.AOE_DMGMULT then
			inst.components.combat:SetAreaDamage(MOB.AOE_RANGE, MOB.AOE_DMGMULT)
		end	
		
		if inst:HasTag("epic") then
			inst.healmult = 2
			inst.acid_mult = 2.5
		end
		
		inst:DoTaskInTime(0, function(inst)
			inst.components.locomotor:SetAllowPlatformHopping(false)
			inst.noplatformhopping = true
			inst.Physics:CollidesWith(COLLISION.LIMITS)	
		end)
	else
		print("WARNING: You didn't set up a tuning table for this name!")		
	end	
end	

	local function CommonActualRez(inst)
    inst.player_classified.MapExplorer:EnableUpdate(true)

    if inst.components.revivablecorpse ~= nil then
        inst.components.inventory:Show()
    else
        inst.components.inventory:Open()
    end

    inst.components.health.canheal = true
    if not GetGameModeProperty("no_hunger") then
        inst.components.hunger:Resume()
    end
    if not GetGameModeProperty("no_temperature") then
        inst.components.temperature:SetTemp() --nil param will resume temp
    end
    inst.components.frostybreather:Enable()

    MakeMediumBurnableCharacter(inst, "torso")
    inst.components.burnable:SetBurnTime(TUNING.PLAYER_BURN_TIME)
    inst.components.burnable.nocharring = true

    MakeLargeFreezableCharacter(inst, "torso")
    inst.components.freezable:SetResistance(4)
    inst.components.freezable:SetDefaultWearOffTime(TUNING.PLAYER_FREEZE_WEAR_OFF_TIME)

    inst:AddComponent("grogginess")
    inst.components.grogginess:SetResistance(3)
    --inst.components.grogginess:SetKnockOutTest(ShouldKnockout)

    inst.components.moisture:ForceDry(false)

    inst.components.sheltered:Start()

    inst.components.debuffable:Enable(true)

    --don't ignore sanity any more
    inst.components.sanity.ignore = GetGameModeProperty("no_sanity")

    inst.components.age:ResumeAging()

    --ConfigurePlayerLocomotor(inst)
    --ConfigurePlayerActions(inst)

    if inst.rezsource ~= nil then
        local announcement_string = GetNewRezAnnouncementString(inst, inst.rezsource)
        if announcement_string ~= "" then
            TheNet:AnnounceResurrect(announcement_string, inst.entity)
        end
        inst.rezsource = nil
    end
    inst.remoterezsource = nil
end

local function DoActualRezFromCorpse(inst, source)
    if not inst:HasTag("corpse") then
        return
    end

    SpawnPrefab("lavaarena_player_revive_from_corpse_fx").entity:SetParent(inst.entity)

    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")

    --inst:SetStateGraph("SGbearplayer")
    inst.sg:GoToState("corpse_rebirth")

    inst.player_classified:SetGhostMode(false)

    local respawn_health_precent = inst.components.revivablecorpse ~= nil and inst.components.revivablecorpse:GetReviveHealthPercent() or 1

    if source ~= nil and source:IsValid() then
        if source.components.talker ~= nil then
            source.components.talker:Say(GetString(source, "ANNOUNCE_REVIVED_OTHER_CORPSE"))
        end

        if source.components.corpsereviver ~= nil then
            respawn_health_precent = respawn_health_precent + source.components.corpsereviver:GetAdditionalReviveHealthPercent()
        end
    end

    --V2C: Let stategraph do it
    --[[inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)]]

    CommonActualRez(inst)

    inst.components.health:SetCurrentHealth(inst.components.health:GetMaxWithPenalty() * math.clamp(respawn_health_precent, 0, 1))
    inst.components.health:ForceUpdateHUD(true)

    inst.components.revivablecorpse:SetCorpse(false)
    if TheWorld.components.lavaarenaevent ~= nil and not TheWorld.components.lavaarenaevent:IsIntermission() then
        inst:AddTag("NOCLICK")
    end

    inst:PushEvent("ms_respawnedfromghost", { corpse = true, reviver = source })
end

PlayablePets.OnRespawnFromMobCorpse = function(inst, data)
    if not inst:HasTag("corpse") then
        return
    end

    inst.deathclientobj = nil
    inst.deathcause = nil
    inst.deathpkname = nil
    inst.deathbypet = nil
    if inst.components.talker ~= nil then
        inst.components.talker:ShutUp()
    end

    inst:DoTaskInTime(0, DoActualRezFromCorpse, data and data.source or nil)
    inst.remoterezsource = nil

    inst.rezsource =
        data ~= nil and (
            (data.source ~= nil and data.source.prefab ~= "reviver" and data.source.name) or
            (data.user ~= nil and data.user:GetDisplayName())
        ) or
        STRINGS.NAMES.SHENANIGANS
end


--Mob players often lose some of their initial stats, this fixes that. Warning: Do not use this for pets that grow 
PlayablePets.RevRestore = function(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	if mob then
		inst.components.locomotor.runspeed = (mob.runspeed)
		inst.components.locomotor.walkspeed = (mob.walkspeed)
	end
	if not ishuman then
		inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
		inst.components.health:SetPercent(1) --Returns health to max health upon reviving
		
		inst.components.locomotor.fasteronroad = false
		
		inst.components.talker:IgnoreAll()
		if MONSTERHUNGER == "Disable" then
			inst.components.hunger:Pause()
		end
		inst.components.sanity.ignore = true
	end
	if isflying then
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end
	if noshadow then
		inst.DynamicShadow:Enable(false)
	end
	if MOBNIGHTVISION < 2 and not ishuman then
		inst.components.grue:AddImmunity("mobplayer")
	end
	
	inst.components.health:SetInvincible(false)
    if inst.components.playercontroller ~= nil then
       inst.components.playercontroller:Enable(true)
    end
	
	if inst.noplatformhopping then
		inst.components.locomotor:SetAllowPlatformHopping(false)
	end
	
	if inst.amphibious then
		if inst.oceanonly then
			inst.components.drownable.enabled = false
			inst.landprotected = true
			SendToSea(inst) --will I need to make a check for this?
		else
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end	
	end

    inst:ShowHUD(true)
    inst:SetCameraDistance()

    SerializeUserSession(inst)
	
end
------------------------------------------------------------------
-- PLAYABLE PETS INIT FUNCTION (MAKES STUFF GO) (WIP)
------------------------------------------------------------------

PlayablePets.Init = function(modName)
	SETTING.MODNAME = modName

	------------------------------------------------------------------
	-- Keyhandlers
	------------------------------------------------------------------
	for fn, keycode in pairs(RPCKEYS) do
		AddModRPCHandler(SETTING.MODNAME, fn, PlayablePets[fn])
		
		TheInput:AddKeyDownHandler(keycode, function()
			if ThePlayer and not ThePlayer.HUD:IsChatInputScreenOpen() and not ThePlayer.HUD:IsConsoleScreenOpen() and IsDefaultScreen() then
				SendModRPCToServer(MOD_RPC[SETTING.MODNAME][fn])
			end
		end)
	end
end

return PlayablePets