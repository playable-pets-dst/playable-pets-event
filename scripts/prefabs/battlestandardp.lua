local function CreatePulse(target)
    local inst = CreateEntity()

    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("lavaarena_battlestandard")
    inst.AnimState:SetBuild("lavaarena_battlestandard")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst.entity:SetParent(target.entity)

    return inst
end

local function ApplyPulse(ent, params)
    if not (ent.replica.health ~= nil and ent.replica.health:IsDead()) then
        local fx = CreatePulse(ent)
        fx:ListenForEvent("animover", fx.Remove)
        fx.AnimState:PlayAnimation("heal_fx")
    end
end

local function OnPulse(inst)
    if inst.fxanim ~= nil and inst.fx ~= nil then
        inst.fx.AnimState:PlayAnimation(inst.fxanim)
    end
	
	local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, {"INLIMBO", "battlestandard", "playerghost"}, {"LA_mob"})
        for i, v in ipairs(ents) do
        ApplyPulse(v, { fxanim = inst.fxanim })	
		end
end

local AURA_INCLUDE_TAGS = { "LA_mob" }
local AURA_INCLUDE_TAGS_NO_PVP = { "LA_mob", "player" }

local function GetTags(inst)
if TheNet:GetPVPEnabled() then
	return AURA_INCLUDE_TAGS
else
	return AURA_INCLUDE_TAGS_NO_PVP
end
end

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.atkbuffed then
		if target.components.combat ~= nil then
			ApplyPulse(target, "attack_fx3")
			local olddmgmult = target.components.combat.damagemultiplier or nil
			if olddmgmult ~= nil then
			target.components.combat.damagemultiplier = olddmgmult + 0.5
			else
			target.components.combat.damagemultiplier = 1.5
			end
			
			target.atkbuffed = true
			target:DoTaskInTime(5, function(inst) 			
				target.components.combat.damagemultiplier = olddmgmult
				inst.atkbuffed = nil			
			end)		
		end
	end
end

local function DoAttackAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        DoAttackBuff(v)
    end
end

local function DoDefenseBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and not target:HasTag("structure") then
		if target.components.combat ~= nil and not (target.sg:HasStateTag("hiding") or target.sg:HasStateTag("sheilding") ) then
			ApplyPulse(target, "defend_fx")
			local oldarmor = target.components.health.absorb
			target.components.health.absorb = oldarmor + 0.5
			if target.components.health.absorb >= 1 then
				target.components.health.absorb = 0.9 --Softcap to prevent invincibility
			end
			
			target.defbuffed = true
			target:DoTaskInTime(5, function(inst) 			
				if not (target.sg:HasStateTag("hiding") or target.sg:HasStateTag("sheilding") ) then
				target.components.health.absorb = oldarmor
				end
				inst.defbuffed = nil			
			end)		
		end
	end
end



local function DoDefenseAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        DoDefenseBuff(v)	
    end
end

local function DoHealBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.healbuffed then
		if target.components.health ~= nil and not target.components.health:IsDead() then
			ApplyPulse(target, "heal_fx")
			target.healbuffed = true
			target.components.health:StartRegen(1, 0.25)
			target:DoTaskInTime(10, function(inst)
				inst.components.health:StopRegen()
				inst.healbuffed = nil
			end)
			
		end
	end
end

local function DoHealAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        if v:IsValid() and v.components.health and not v.components.health:IsDead() and not v.buffimmune then
			DoHealBuff(v)	
		end
    end
end

local function OnRemove(inst)
	inst:Remove()
end

local function OnKilled(inst)
	if inst.DoAura ~= nil then
	inst.DoAura:Cancel()
	end
    inst.AnimState:PlayAnimation("break")
	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner_break")
	--inst.components.lootdropper:DropLoot(inst:GetPosition())
	inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, OnRemove)
end

local function OnHit(inst)
	if not inst.components.health:IsDead() then
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", true)
	inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_wood_armour_sharp")
	end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
    inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner")
end

local function MakeBattleStandard(name, build_swap, debuffprefab, fx_anim, aura)
    local assets =
    {
        Asset("ANIM", "anim/lavaarena_battlestandard.zip"),
    }
    if build_swap ~= nil then
        table.insert(assets, Asset("ANIM", "anim/"..build_swap..".zip"))
    end

    local prefabs =
    {
        --debuffprefab,
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank("lavaarena_battlestandard")
        inst.AnimState:SetBuild("lavaarena_battlestandard")
        if build_swap ~= nil then
            inst.AnimState:AddOverrideBuild(build_swap)
        end
        inst.AnimState:PlayAnimation("place")
		inst.AnimState:PushAnimation("idle", true)
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner")

        inst:AddTag("battlestandard")
		inst:AddTag("character")
		inst:AddTag("structure")
		inst:AddTag("monster")
        inst:AddTag("LA_mob")

        inst.fxanim = fx_anim

        --Dedicated server does not need local fx
        if not TheNet:IsDedicated() then
           	inst.fx = CreatePulse(inst)
        end
        --inst.pulse = net_event(inst.GUID, "lavaarena_battlestandard_damager.pulse")

        inst:SetPrefabNameOverride("lavaarena_battlestandard")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            --inst:ListenForEvent("lavaarena_battlestandard_damager.pulse", OnPulse)

            return inst
        end

        --inst.debuffprefab = debuffprefab
        --inst.OnPulse = OnPulse
		
		if aura ~= nil then
			if aura == "Attack" then
				inst.DoAura = inst:DoPeriodicTask(0.25, DoAttackAura)
			elseif aura == "Defense" then
				inst.DoAura = inst:DoPeriodicTask(0.25, DoDefenseAura)
			elseif aura == "Heal" then
				inst.DoAura = inst:DoPeriodicTask(0.25, DoHealAura)	
			end		
		end
		
		inst:AddComponent("health")
        inst.components.health:SetMaxHealth(50)
		
		inst:AddComponent("combat")
        inst.components.combat:SetOnHit(OnHit)
        inst:ListenForEvent("death", OnKilled)
		
		inst:AddComponent("inspectable")
		
		--inst:AddComponent("lootdropper")
		
		inst:ListenForEvent("onbuilt", onbuilt)

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

------------------------------------------------------------------------------

return MakeBattleStandard("battlestandard_damagerp", "lavaarena_battlestandard_attack_build", "lavaarena_battlestandard_damagerbuff", "attack_fx3", "Attack"),
    MakeBattleStandard("battlestandard_shieldp", nil, "lavaarena_battlestandard_shieldbuff", "defend_fx", "Defense"),
    MakeBattleStandard("battlestandard_healp", "lavaarena_battlestandard_heal_build", "lavaarena_battlestandard_healbuff", "heal_fx", "Heal"),
	MakePlacer("battlestandardp_placer", "lavaarena_battlestandard", "lavaarena_battlestandard", "idle")
