local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


local assets = 
{
	Asset("ANIM", "anim/lavaarena_beetletaur.zip"),
    Asset("ANIM", "anim/lavaarena_beetletaur_basic.zip"),
    Asset("ANIM", "anim/lavaarena_beetletaur_actions.zip"),
    Asset("ANIM", "anim/lavaarena_beetletaur_block.zip"),
    Asset("ANIM", "anim/lavaarena_beetletaur_fx.zip"),
    Asset("ANIM", "anim/lavaarena_beetletaur_break.zip"),
    Asset("ANIM", "anim/healing_flower.zip"),
	Asset("ANIM", "anim/beetletaur_shiny_build_03.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and 42500 or 4500,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 10,
	walkspeed = 4,
	damage = BOSS_STATS and 200*2 or 75*2,
	attackperiod = 4,
	range = BOSS_STATS and 3 or 5,
	hit_range = BOSS_STATS and 3 or 5,
	bank = "beetletaur",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_beetletaur_alt1" or  "lavaarena_beetletaur",
	scale = 1.05,
	shiny = "beetletaur",
	--build2 = "alternate build here",
	stategraph = "SGbeetletaurp",
	minimap = "beetletaurp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('beetletaurp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00},
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
})

-------------------------------------------------------
--Knockback--

local function OnHitOther(inst, other) --knockback
	if inst.sg:HasStateTag("slamming") then
		PlayablePets.KnockbackOther(inst, other, 5)
	end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

-----------Collision-----------------
local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function oncollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        Vector3(inst.Physics:GetVelocity()):LengthSq() >= 1 and other:HasTag("tree") and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end
-----------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end


--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function OnHitOther_Forge(inst, target)
	if inst.sg:HasStateTag("groundpound") and target then 
		target:PushEvent("flipped", {flipper = inst})
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.SWINECLOPS)
	
	inst.mobsleep = false
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	inst.acidmult = 2
	inst.healmult = 2
	inst.components.health:SetAbsorptionAmount(0.90)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetRange(7, 0) --TODO, implement a more complex way to slam?
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('beetletaurp')
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("head", 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	--inst:AddTag("special_atk1") --allows taunting
	
	inst:AddTag("monster")
	inst:AddTag("LA_mob")
    inst:AddTag("beetletaur")
	inst:AddTag("epic")
	
	inst.AnimState:AddOverrideBuild("fossilized")
	
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.currentcombo = 0
	inst.maxpunches = 1
	inst.knocker = true
	
	inst.canbuff = true
	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	inst:SetPhysicsRadiusOverride(1.75)
    MakeCharacterPhysics(inst, 500, inst.physicsradiusoverride)
    inst.DynamicShadow:SetSize(4.5, 2.25)
    inst.Transform:SetFourFaced()
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > 0.9 then
			inst.maxpunches = 1
		elseif health >  0.7 and health <= 0.9 then
			inst.maxpunches = 2
		elseif health > 0.5 and health <= 0.7 then
			inst.maxpunches = 5
		elseif health > 0.15 and health <= 0.5 then
			inst.maxpunches = 7
		elseif health <= 0.15 then
			inst.maxpunches = 20
		end	
	
	end)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

local function OnTick(inst, target)
	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx" or "attack_fx3")
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
	--inst.tickcount = 0
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
	
	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx_pre" or "attack_fx3_pre")
	
	if target.components.combat then
		if inst.type and inst.type == "defense" then
			if TheNet:GetServerGameMode() == "lavaarena" then
				target.components.combat:AddDamageBuff("swineclops_defensebuff", 0.5, true)
			else
				target.components.health:SetAbsorptionAmount(0.5)
			end
		else
			if TheNet:GetServerGameMode() == "lavaarena" then
				target.components.combat:AddDamageBuff("swineclops_attackbuff", 1.5, false)
			else
				target.components.combat.damagemultiplier = 1.5
			end
		end
	end
	
    inst:ListenForEvent("death", function()
        --inst.components.debuff:Stop()
    end, target)
end

local function OnDetached(inst, target)
	if target.type and target.type == "defense" then
		if TheNet:GetServerGameMode() == "lavaarena" then
			target.components.combat:RemoveDamageBuff("swineclops_defensebuff")
		else		
			target.components.health:SetAbsorptionAmount(0)
		end	
	else
		if TheNet:GetServerGameMode() == "lavaarena" then
			target.components.combat:RemoveDamageBuff("swineclops_attackbuff")
		else
			target.components.combat.damagemultiplier = 1
		end	
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == "swineclopsbuffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)

	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx_pre" or "attack_fx3_pre")
	
	if inst.task then
		inst.task:Cancel()	
		inst.task = nil
	end	
	inst.task = inst:DoPeriodicTask(3, OnTick, nil, target)
end

local function fx_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("beetletaur_break")
    inst.AnimState:SetBuild("lavaarena_beetletaur_break")
    inst.AnimState:PlayAnimation("anim", false)

    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	inst:ListenForEvent("animover", inst.Remove)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false 

    return inst
end

--too lazy to make a proper function for these right now. Will fix before proper release.

local function dot_fn()
	local inst = CreateEntity()
	
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	

    inst.AnimState:SetBank("lavaarena_beetletaur_fx")
    inst.AnimState:SetBuild("lavaarena_beetletaur_fx")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.type = "defense"
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
	
	--inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.
	
	return inst
	
end

local function dot2p_fn()
	local inst = CreateEntity()
	
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	

    inst.AnimState:SetBank("lavaarena_beetletaur_fx")
    inst.AnimState:SetBuild("lavaarena_beetletaur_fx")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.type = "attack"
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
	
	inst:AddComponent("timer")
		inst:DoTaskInTime(0, function() -- in case we want to change duration
			inst.components.timer:StartTimer("swineclopsbuffover", 10)
		end)
	inst:ListenForEvent("timerdone", OnTimerDone)
	
	--inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.
	
	return inst
	
end


return MakePlayerCharacter("beetletaurp", prefabs, assets, common_postinit, master_postinit, start_inv),
Prefab("swineclops_defensebuffp", dot_fn),
Prefab("swineclops_attackbuffp", dot2p_fn),
Prefab("swineclops_break_fxp", fx_fn)
