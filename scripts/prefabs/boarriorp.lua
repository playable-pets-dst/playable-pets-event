local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/boarrior_shiny_build_01.zip"),
	Asset("ANIM", "anim/boarrior_shiny_build_06.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and 34000 or 5000,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 300,
	runspeed = 5,
	walkspeed = 5,
	damage = BOSS_STATS and 200*2 or 80*2,
	attackperiod = BOSS_STATS and 3 or 3,
	hit_range = BOSS_STATS and 6 or 5,
	range = BOSS_STATS and 3 or 5,
	bank = "boarrior",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_boarrior_alt1" or "lavaarena_boarrior_basic",
	scale = 1,
	shiny = "boarrior",
	--build2 = "alternate build here",
	stategraph = "SGboarriorp",
	minimap = "boarriorp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('boarriorp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00},
	{'meat',			1.00},
	{'meat',			1.00},   
	{'meat',			1.00},
	{'meat',			1.00},
	{'meat',			1.00},
	{'meat',			1.00},
	{'pigskin',			1.00},
	{'pigskin',			0.50},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "shadow", "notarget", "boarrior_pet", "playerghost", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "shadow", "notarget", "boarrior_pet"}
	else	
		return {"player", "companion", "INLIMBO", "shadow", "notarget", "LA_mob", "battlestandard"}
	end
end
-------------------------------------------------------
--Knockback--

local function OnHitOther(inst, other) --knockback
	if not inst.is_doing_special then
		PlayablePets.KnockbackOther(inst, other, 5)
	end
	if inst.is_doing_special and other then 
		other:PushEvent("flipped", {flipper = inst})
	end
end
-------------------------------------------------------
--Banner Functions--
local Banners =
{
	"battlestandard_damagerp",
	"battlestandard_shieldp",
	"battlestandard_healp",
}

local function SpawnBanners(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	
	local rand = math.random(1,3)
	local banner = Banners[rand]
	
	
	
	if TheWorld.Map:IsPassableAtPoint(x, 0, z + 15) then
	local banner1 = SpawnPrefab(banner)
	banner1.Transform:SetPosition(x, 0, z + 15)
	end
	
	
	if TheWorld.Map:IsPassableAtPoint(x - 15, 0, z) then
	local banner2 = SpawnPrefab(banner)
	banner2.Transform:SetPosition(x - 15, 0, z)
	end
	
	
	if TheWorld.Map:IsPassableAtPoint(x + 15, 0, z) then
	local banner3 = SpawnPrefab(banner)
	banner3.Transform:SetPosition(x + 15, 0, z)
	end
	
	
	if TheWorld.Map:IsPassableAtPoint(x, 0, z - 15) then
	local banner4 = SpawnPrefab(banner)
	banner4.Transform:SetPosition(x, 0, z - 15)
	end
end

local MINION_RADIUS = .3
local MINION_SPAWN_PERIOD = .75
local NUM_RINGS = 3
local RING_SIZE = 7.5 / NUM_RINGS
local RING_TOTAL = 1
for i = 2, NUM_RINGS do
    RING_TOTAL = RING_TOTAL + i * i
end

local function DoSummonPigs(inst)
    local pt = table.remove(inst.minionpoints, math.random(#inst.minionpoints))
    local minion = SpawnPrefab("boaron_p")
	minion.Transform:SetPosition(pt:Get())
    minion:ForceFacePoint(pt)
	if inst.components.combat.target then
		minion.components.combat:SetTarget(inst.components.combat.target)
	end
	inst.components.leader:AddFollower(minion)
	minion.persists = false
    if #inst.minionpoints <= 0 then
        inst.miniontask:Cancel()
        inst.miniontask = nil
        inst.minionpoints = nil
    end
end

local function SummonPigs(inst, target)
	inst.spawned_pigs = true
    local count = 8

    if count <= 0 or inst.miniontask ~= nil then
        return
    end

    local stargate = inst
    local x, y, z = inst.Transform:GetWorldPosition()
    local map = TheWorld.Map
    inst.minionpoints = {}
    for ring = 1, NUM_RINGS do
        local ringweight = ring * ring / RING_TOTAL
        local ringcount = math.floor(count * ringweight + .5)
        if ringcount > 0 then
            local delta = 2 * PI / ringcount
            local radius = 25
            for i = 1, ringcount do
                local angle = delta * i
                local x1 = x + radius * math.cos(angle) + math.random() - .5
                local z1 = z + radius * math.sin(angle) + math.random() - .5
                if map:IsAboveGroundAtPoint(x1, 0, z1) then
                    table.insert(inst.minionpoints, Vector3(x1, 0, z1))
                end
            end
        end
    end
    if #inst.minionpoints > 0 then
        inst.miniontask = inst:DoPeriodicTask(0.1, DoSummonPigs, 0)
    else
        inst.minionpoints = nil
    end
end

local function EnterPhase2Trigger(inst)
    if inst.is_enraged ~= true and TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.is_enraged = true
		inst.altattack = true
		inst.sg:GoToState("banner_pre")
	end
end

-------------------------------------------------------
--Slam Attack Functions--

local groundlifts =
{
}

function DoTrailp(inst, targetposx, targetposz, trailend)
	inst.stopslam = nil --might fix no slam bug?
	local startingpos = inst:GetPosition()
	inst:ForceFacePoint(targetposx, 0, targetposz)
	--if TheWorld.Map:IsAboveGroundAtPoint(targetposx, 0, targetposz) and TheWorld.Pathfinder:IsClear(startingpos.x, startingpos.y, startingpos.z, targetposx, 0, targetposz, {ignorewalls = false, ignorecreep = true}) then
		local targetpos = {x = targetposx, y = 0, z = targetposz}
		local found_players = {}
		
		local angle = -inst.Transform:GetRotation() * DEGREES
		local angled_offset = {x = 1.25 * math.cos(angle+90), y = 0, z = 1.25 * math.sin(angle+90)}
		local angled_offset2 = {x = 1.25 * math.cos(angle-90), y = 0, z = 1.25 * math.sin(angle-90)}
		local impact_distance = 36
					
		local maxtrails = 12 + 5
		for i = 1, maxtrails do
			inst:DoTaskInTime(FRAMES*math.ceil(1+i/ (trailend and 1.5 or 3.5)), function()
				local offset = (targetpos - startingpos):GetNormalized()*(i)
				local x, y, z = (startingpos + offset):Get()
				if TheWorld.Map:IsAboveGroundAtPoint((startingpos+offset):Get()) and not inst.stopslam then
					if i > 6 then
						SpawnPrefab(trailend ~= nil and "lavaarena_groundliftp" or "lavaarena_groundliftrocksp").Transform:SetPosition((startingpos+offset+angled_offset):Get())
						SpawnPrefab(trailend ~= nil and "lavaarena_groundliftp" or "lavaarena_groundliftrocksp").Transform:SetPosition((startingpos+offset+angled_offset2):Get())
						if not inst:HasTag("groundspike") then
							inst:AddTag("groundspike")
						end
					end
					local ents = TheSim:FindEntities(x, y, z, 2.5, { "locomotor" }, GetExcludeTags())
					for _,ent in ipairs(ents) do
						if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and not ent.hasbeenhit then
							inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
							--Leo:Temporary fix for preventing multiple hits.
							ent.components.combat:GetAttacked(inst, i < 6 and inst.components.combat:CalcDamage(ent) or (inst.components.combat:CalcDamage(ent) * 0.75 * (not trailend and 0.5 or 1)))
							ent.hasbeenhit = true
							ent:DoTaskInTime(0.25, function(inst) inst.hasbeenhit = nil end)
						--SpawnPrefab("forgespear_fx"):SetTarget(ent)
						end
					end
				else
					inst.stopslam = true
				end	
				if i == maxtrails then
					inst.stopslam = nil
				end
			end)
		end
	--end
end
-------------------------------------------------------

----
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.is_enraged = data.is_enraged or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.is_enraged = inst.is_enraged or false
end
----

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function oncollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        Vector3(inst.Physics:GetVelocity()):LengthSq() >= 1 and other:HasTag("tree") and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end


--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.BOARRIOR)
	
	inst.mobsleep = false
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	inst.components.health:SetAbsorptionAmount(0.9)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst:RemoveComponent("healthtrigger")
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetRange(10, mob.range)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('boarriorp')
	----------------------------------
	--Tags--
	
	inst:AddTag("monster")
    inst:AddTag("LA_mob")
	inst:AddTag("epic")
	
	inst.altattack = true
	inst.altattack3 = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.banner_calls = 0
	inst.is_enraged = false
	
	inst.AnimState:AddOverrideBuild("fossilized")
	
	inst.DoTrailp = DoTrailp
	inst.SummonPigs = SummonPigs
	inst.SpawnBanners = SpawnBanners
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(0.5, EnterPhase2Trigger)
	
	inst.hit_recovery = 0.75
	
	inst.components.combat.notags = {"boarrior_pet"}
	local body_symbol = "pelvis"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("head", 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 500, 1.5)
    inst.DynamicShadow:SetSize(5.25, 1.75)
    inst.Transform:SetFourFaced()
    ---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	inst:ListenForEvent("healthdelta", function()
		if inst.is_enraged == true and inst.components.health:GetPercent() >= 0.75 then 
			inst.spawned_pigs = nil
			inst.is_enraged = false
			inst.components.combat:SetAttackPeriod((BOSS_STATS or TheNet:GetServerGameMode() == "lavaarena") and 3 or 5)
		end		
	end)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("boarriorp", prefabs, assets, common_postinit, master_postinit, start_inv)
