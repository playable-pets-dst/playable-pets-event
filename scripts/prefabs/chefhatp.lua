require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/lavaarena_chefhat.zip"),
}

local prefabs =
{

}

local function onequip(inst, owner, symbol_override)
	owner.AnimState:OverrideSymbol("swap_hat", "lavaarena_chefhat", "swap_hat")
    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAIR_HAT")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Hide("HEAD")
        owner.AnimState:Show("HEAD_HAT")
	end
	owner:AddTag("master_chef")
end

local function onunequip(inst, owner)
	owner.AnimState:ClearOverrideSymbol("swap_hat")
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAIR_HAT")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")

    if owner:HasTag("player") then
		owner.AnimState:Show("HEAD")
        owner.AnimState:Hide("HEAD_HAT")
    end
	owner:RemoveTag("master_chef")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst:AddTag("hat")

    inst.AnimState:SetBank("lavaarena_chefhat")
    inst.AnimState:SetBuild("lavaarena_chefhat")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/chefhatp.xml"
	
    inst:AddComponent("inspectable")

    inst:AddComponent("tradable")

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	inst:AddComponent("fueled")
    inst.components.fueled.fueltype = FUELTYPE.USAGE
    inst.components.fueled:InitializeFuelLevel(TUNING.STRAWHAT_PERISHTIME)
    inst.components.fueled:SetDepletedFn(--[[generic_perish]]inst.Remove)

    MakeHauntableLaunch(inst)
	
    return inst
end

return Prefab("chefhatp", fn, assets, prefabs)
