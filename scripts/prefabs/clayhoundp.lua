local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/hound_basic.zip"),
    Asset("ANIM", "anim/hound_basic_water.zip"),
    Asset("ANIM", "anim/hound.zip"),
    Asset("ANIM", "anim/hound_ocean.zip"),
    Asset("ANIM", "anim/hound_red.zip"),
    Asset("ANIM", "anim/hound_red_ocean.zip"),
    Asset("ANIM", "anim/hound_ice.zip"),
    Asset("ANIM", "anim/hound_ice_ocean.zip"),
    Asset("ANIM", "anim/hound_mutated.zip"),
	Asset("ANIM", "anim/clayhound_shiny_build_01.zip"),
	Asset("ANIM", "anim/clayhound_shiny_build_03.zip"),
	Asset("ANIM", "anim/clayhound.zip"),
}



local prefabs = 
{	
	"clayhound",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 350,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.CLAYHOUND_SPEED - 1.5,
	walkspeed = TUNING.CLAYHOUND_SPEED - 1.5,
	attackperiod = 0,
	damage = 30,
	range = 3,
	bank = "clayhound",
	build = "clayhound",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGclayhoundp",
	minimap = "clayhoundp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('clayhoundp',
-----Prefab---------------------Chance------------
{    
   {'redpouch',                0.50},
   {'houndstooth',             0.50},
})

local sounds_clay =
{
    pant = "dontstarve/creatures/together/clayhound/pant",
    attack = "dontstarve/creatures/together/clayhound/attack",
    bite = "dontstarve/creatures/together/clayhound/bite",
    bark = "dontstarve/creatures/together/clayhound/bark",
    death = "dontstarve/creatures/together/clayhound/death",
    sleep = "dontstarve/creatures/together/clayhound/sleep",
    growl = "dontstarve/creatures/together/clayhound/growl",
    howl = "dontstarve/creatures/together/clayhound/howl",
}


local function OnEyeFlamesDirty(inst)
    if TheWorld.ismastersim then
        if not inst._eyeflames:value() then
            inst.AnimState:SetLightOverride(0)
            inst.SoundEmitter:KillSound("eyeflames")
        else
            inst.AnimState:SetLightOverride(.07)
            if not inst.SoundEmitter:PlayingSound("eyeflames") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/torch_LP", "eyeflames")
                inst.SoundEmitter:SetParameter("eyeflames", "intensity", 1)
            end
        end
        if TheNet:IsDedicated() then
            return
        end
    end

    if inst._eyeflames:value() then
        if inst.eyefxl == nil then
            inst.eyefxl = SpawnPrefab("eyeflame")
            inst.eyefxl.entity:AddFollower()
            inst.eyefxl.Follower:FollowSymbol(inst.GUID, "hound_eye_left", 0, 0, 0)
        end
        if inst.eyefxr == nil then
            inst.eyefxr = SpawnPrefab("eyeflame")
            inst.eyefxr.entity:AddFollower()
            inst.eyefxr.Follower:FollowSymbol(inst.GUID, "hound_eye_right", 0, 0, 0)
        end
    else
        if inst.eyefxl ~= nil then
            inst.eyefxl:Remove()
            inst.eyefxl = nil
        end
        if inst.eyefxr ~= nil then
            inst.eyefxr:Remove()
            inst.eyefxr = nil
        end
    end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
----------
--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function OnHitOther_Forge(inst, target)
	if inst.is_doing_special and target then 
		target:PushEvent("flipped", {flipper = inst})
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.CLAYHOUND)
	
	inst.mobsleep = false

	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.knockbackimmune = true
	
	inst:AddComponent("fossilizer")
	if inst.components.fossilizer.SetRange then
		inst.components.fossilizer:SetRange(5)
	end
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------

local common_postinit = function(inst) 
	
	inst._eyeflames = net_bool(inst.GUID, "clayhound._eyeflames", "eyeflamesdirty")
    inst:ListenForEvent("eyeflamesdirty", OnEyeFlamesDirty)

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end



local master_postinit = function(inst) 
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.10, 0, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hurt")
	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('clayhoundp')
	----------------------------------
	--Tags--
	inst:AddTag("clay")
	inst:AddTag("hound")
	inst:AddTag("houndfriend")
	
	
	inst.taunt = true
	--inst.taunt2 = true
	inst.mobsleep = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	
	
	inst.sounds = sounds_clay
	inst.poisonimmune = true
	inst.components.burnable.canlight = false
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()
	 ---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end




return MakePlayerCharacter("clayhoundp", prefabs, assets, common_postinit, master_postinit, start_inv)
