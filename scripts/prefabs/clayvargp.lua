local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/claywarg_shiny_build_01.zip"),
	Asset("ANIM", "anim/claywarg_shiny_build_03.zip"),
	Asset("ANIM", "anim/claywarg.zip")
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.WARG_HEALTH,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = 5.5,
	walkspeed = 5.5,
	damage = 65*2,
	attackperiod = 3,
	range = 5,
	bank = "claywarg",
	build = "claywarg",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGclayvargp",
	minimap = "clayvargp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('clayvargp',
-----Prefab---------------------Chance------------
{
    {'redpouch',                1.00},
    {'redpouch',                1.00},
    {'redpouch',                1.00},
    {'redpouch',                1.00},
    {'redpouch',                0.50},
    {'redpouch',                0.50},

    {'houndstooth',             1.00},
    {'houndstooth',             0.66},
    {'houndstooth',             0.33},   
})

local sounds_clay =
{
    idle = "dontstarve_DLC001/creatures/together/claywarg/idle",
    howl = "dontstarve_DLC001/creatures/together/claywarg/howl",
    hit = "dontstarve_DLC001/creatures/together/claywarg/hit",
    attack = "dontstarve_DLC001/creatures/together/claywarg/attack",
    death = "dontstarve_DLC001/creatures/together/claywarg/death",
    sleep = "dontstarve_DLC001/creatures/together/claywarg/sleep",
    alert = "dontstarve_DLC001/creatures/together/claywarg/alert",
}

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("hound") and not dude.components.health:IsDead() end, 10)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("hound") and not dude.components.health:IsDead() end, 10)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function OnClayReanimated(inst)

end

local function OnClayBecameStatue(inst)

end

local function OnEyeFlamesDirty(inst)
    if TheWorld.ismastersim then
        if not inst._eyeflames:value() then
            inst.AnimState:SetLightOverride(0)
            inst.SoundEmitter:KillSound("eyeflames")
        else
            inst.AnimState:SetLightOverride(.07)
            if not inst.SoundEmitter:PlayingSound("eyeflames") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/torch_LP", "eyeflames")
                inst.SoundEmitter:SetParameter("eyeflames", "intensity", 1)
            end
        end
        if TheNet:IsDedicated() then
            return
        end
    end

    if inst._eyeflames:value() then
        if inst.eyefxl == nil then
            inst.eyefxl = SpawnPrefab("eyeflame")
            inst.eyefxl.entity:AddFollower()
            inst.eyefxl.Follower:FollowSymbol(inst.GUID, "warg_eye_left", 0, 0, 0)
        end
        if inst.eyefxr == nil then
            inst.eyefxr = SpawnPrefab("eyeflame")
            inst.eyefxr.entity:AddFollower()
            inst.eyefxr.Follower:FollowSymbol(inst.GUID, "warg_eye_right", 0, 0, 0)
        end
    else
        if inst.eyefxl ~= nil then
            inst.eyefxl:Remove()
            inst.eyefxl = nil
        end
        if inst.eyefxr ~= nil then
            inst.eyefxr:Remove()
            inst.eyefxr = nil
        end
    end
end

local function OnRemoveEntity(inst)
    if inst.eyefxl ~= nil then
        inst.eyefxl:Remove()
        inst.eyefxl = nil
    end
    if inst.eyefxr ~= nil then
        inst.eyefxr:Remove()
        inst.eyefxr = nil
    end
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function UpdatePetLevel(inst, level, force_level)
	-- Only level up if the current level will change
	if level and level ~= 0 or force_level and force_level ~= inst.current_level then
		inst.current_level = force_level or inst.current_level + level
		-- Update Attack
		inst.components.combat.defaultdamage = 10 * inst.current_level
		inst.components.combat:SetAttackPeriod(inst.current_level > 1 and 0 or 3) -- TODO is this attack period correct? 0 for level 2 spiders?
		-- Update Appearance
		inst.Transform:SetScale(level, level, level)
	end
end

local function ShouldWakeUp(inst)
    return inst.components.follower.leader and not inst.components.follower.leader.components.health:IsDead()
end

local function ShouldSleep(inst)
    return inst.components.follower.leader == nil or inst.components.follower.leader.components.health:IsDead()
end

--Copy Pasted from FF rework to work in the pub branch, delete this afterwards.
local MIN_LAST_PLAYER_ATTACK_TIME_IN_CC = 0.5
local function PetIsValidTarget(inst, target)
    return inst.components.combat:CanTarget(target) and
	not (target.sg and (target.sg:HasStateTag("sleeping") or target.sg:HasStateTag("fossilized"))) and -- Don't attack targets in cc
	(target:HasTag("_isinheals") and (GetTime() - target.components.combat.last_player_attack < MIN_LAST_PLAYER_ATTACK_TIME_IN_CC) or not target:HasTag("_isinheals")) -- Stop attacking targets in heal if players have not attacked target recently
end

local function PetRetargetFn(inst, target)
	inst:AddTag("notarget")
    local leader = inst.components.follower.leader
    return leader and not leader.components.health:IsDead() and leader.components.combat.target or nil
end

local function PetKeepTarget(inst, target)
    return PetIsValidTarget(inst, target) and
    not (inst.components.follower.leader.components.combat.target and inst.components.follower.leader.components.combat.target ~= target) -- Only keep leaders target
end
-----------------------------------------------
local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.CLAYVARG)
	
	inst.mobsleep = false

	inst.components.health:SetAbsorptionAmount(0.9)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "armors"})
	end)
	
	inst.knockbackimmune = true
	
	inst:AddComponent("fossilizer")
	if inst.components.fossilizer.SetRange then
		inst.components.fossilizer:SetRange(7)
	end
	
	local max_baby_spiders = 2
	inst.components.petleash.maxpets = 2
	inst.components.petleash.petprefab = "clayhound"
	inst:DoTaskInTime(0, function(inst) 
		for i = 1, max_baby_spiders do
			local theta = (i / max_baby_spiders) * 2 * _G.PI--math.random() * 2 * _G.PI
			local pt = inst:GetPosition()
			local radius = 1--math.random(3, 6) -- TODO constant radius
			local offset = _G.FindWalkableOffset(pt, theta, radius, 3, true, true) or {x = 0, y = 0, z = 0}
			local pet = inst.components.petleash:SpawnPetAt(pt.x + offset.x, 0, pt.z + offset.z)
			pet.Transform:SetScale(0.5, 0.5, 0.5)
			pet.current_level = 1
			pet.UpdatePetLevel = UpdatePetLevel
			pet:AddTag("notarget")
			pet.components.health:SetAbsorptionAmount(1)
			pet.components.health:SetInvincible(true)
			pet.components.follower:KeepLeaderOnAttacked() -- TODO make sure the other companions need this
			pet.components.follower.keepdeadleader = true -- TODO make sure the other companions need this
			pet.components.combat:SetRetargetFunction(1, PetRetargetFn)
			pet.components.combat:SetKeepTargetFunction(PetKeepTarget)
			pet:AddComponent("sleeper")
			pet.components.sleeper.testperiod = GetRandomWithVariance(2, 1)
			pet.components.sleeper:SetSleepTest(ShouldSleep)
			pet.components.sleeper:SetWakeTest(ShouldWakeUp)
			pet.components.combat:SetDefaultDamage(5)
		end
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	inst.sounds = sounds_clay
	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)
	inst._eyeflames = net_bool(inst.GUID, "claywarg._eyeflames", "eyeflamesdirty")
    inst:ListenForEvent("eyeflamesdirty", OnEyeFlamesDirty)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.10, 0, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('clayvargp')
	----------------------------------
	--Tags--
	inst:AddTag("monster")
    inst:AddTag("clay")
    inst:AddTag("houndfriend")
    inst:AddTag("largecreature")
	
	inst.taunt = true
	inst.mobsleep = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	
	inst.sounds = sounds_clay
	inst.noidlesound = true
	inst.hit_recovery = 1
	inst.poisonimmune = true
	inst.components.burnable.canlight = false
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	inst.OnReanimated = OnClayReanimated
    inst.OnBecameStatue = OnClayBecameStatue
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1000, 1)
    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetSixFaced()
	
	inst.noidlesound = false
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end




return MakePlayerCharacter("clayvargp", prefabs, assets, common_postinit, master_postinit, start_inv)
