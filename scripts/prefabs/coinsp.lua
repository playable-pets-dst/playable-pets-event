local assets =
{
    Asset("ANIM", "anim/quagmire_coins.zip"),
}


local function MakeCoin(id)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()
		
        inst.AnimState:SetBank("quagmire_coins")
        inst.AnimState:SetBuild("quagmire_coins")
        inst.AnimState:PlayAnimation("idle")
        if id > 1 then
            inst.AnimState:OverrideSymbol("coin01", "quagmire_coins", "coin0"..tostring(id))
            inst.AnimState:OverrideSymbol("coin_shad1", "quagmire_coins", "coin_shad"..tostring(id))
		else
			inst:AddTag("commontier") --lowest tier
        end
		
		--if id == 4 then
			--inst.AnimState:PlayAnimation("opal", true)
			--inst:Show("coin01")
		--end
		
		if id > 2 then
			inst:AddTag("hightier")
		end

        inst:AddTag("quagmire_coin")
		inst:AddTag("currency")

        MakeInventoryPhysics(inst)
		
		MakeInventoryFloatable(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst:SetStateGraph("SGgorge_coinp")

		inst:AddComponent("stackable")
        inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM		

        inst:AddComponent("inspectable")
        inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/gorge_coin"..id.."p.xml"
		
		--onupdate = onupdate

        return inst
    end

    return Prefab("gorge_coin"..id.."p", fn, assets)
end

return MakeCoin(1),
    MakeCoin(2),
    MakeCoin(3),
    MakeCoin(4)
