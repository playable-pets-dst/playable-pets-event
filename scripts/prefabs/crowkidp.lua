local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "crowkidp"

local assets = 
{
	Asset("ANIM", "anim/mutated_crow.zip"),   
    Asset("ANIM", "anim/bird_mutant_build.zip"),   
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	
	health = TUNING[string.upper(prefabname).."_HEALTH"],
	hunger = TUNING[string.upper(prefabname).."_HUNGER"],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname).."_SANITY"],
	runspeed = TUNING.CARNIVAL_CROWKID_RUN_SPEED,
	walkspeed = TUNING.CARNIVAL_CROWKID_WALK_SPEED,
	damage = 20,
	range = 1.5,
	hit_range = 2,
	attackperiod = 0,
	bank = "crow_kids",
	build = "crow_kids",
	scale = 1,	
	stategraph = "SG"..prefabname,
	minimap = prefabname.."p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'drumstick',		  1.00},  
	{'feather_crow',	  1.00},  
	{'feather_crow',	  1.00},  
	{'feather_crow',	  1.00},  
})

--FORGE_STATS = PPCU_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function OnEat(inst, food)
	if food and food.prefab == "corn_cooked" then
		inst.sg:GoToState("eat_popcorn")
	end
end

--==============================================
--				Custom Common Functions
--==============================================	
local function SetSkinDefault(inst, data)
	if data then
		if data.scarf then
			if data.scarf == 1 then
				inst.AnimState:ClearOverrideSymbol("scarf_1")
			else
				inst.AnimState:OverrideSymbol("scarf_1", data.build, "scarf_"..tostring(data.scarf))
			end
		end
		inst.AnimState:SetBuild(data.build)
	else
		inst.AnimState:ClearOverrideSymbol("scarf_1")
		inst.AnimState:SetBuild(mob.build)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("character")
	----------------------------------
	
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	inst.components.talker.fontsize = 30
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(165/255, 180/255, 200/255)
    inst.components.talker.offset = Vector3(0, -400, 0)
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison
	PlayablePets.SetPassiveHeal(inst)
	----------------------------------
	--Variables		
	inst.AnimState:AddOverrideBuild("crow_kids2")
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst._isflying = false
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetCanEatRaw()
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, 1.75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob)  end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
