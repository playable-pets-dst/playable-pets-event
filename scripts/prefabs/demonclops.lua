--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    --Asset("ANIM", "anim/lavaarena_trails_basic.zip"),
}

local prefabs =
{

}

------------------------------
--ToDo List--
------------------------------
--Adjust speed and hit/attack range of Boarilla to ensure you need speedboosts to dodge incoming punchs.
--Make small adjustments to CalcJumpSpeed in the SG to better mimic the slam ingame.
-----------------------------

local brain = require("brains/demonclopsbrain")

SetSharedLootTable( 'demonclops',
{
    --{'monstermeat', 1.000},
})

local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }


local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local function OnHitOther(inst, other) --knockback
	if inst.sg:HasStateTag("slamming") then
		if other.sg and other.sg.sg.events.knockback then
			other:PushEvent("knockback", {knocker = inst, radius = 5})
		else
		--this stuff below is mostly left here for creatures in basegame. For modders that are reading this, use the knockback event above.
		if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
		
			if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
				if other.components.combat ~= nil then
					--other.components.combat:GetAttacked(inst, 10)
					if other.Physics ~= nil then
						local x, y, z = inst.Transform:GetWorldPosition()
						local distsq = other:GetDistanceSqToPoint(x, 0, z)
						--if distsq < REPEL_RADIUS_SQ then
							if distsq > 0 then
								other:ForceFacePoint(x, 0, z)
							end
							local k = .5 * distsq / REPEL_RADIUS_SQ - 1
							other.speed = 60 * k
							other.dspeed = 2
							other.Physics:ClearMotorVelOverride()
							other.Physics:Stop()
						
							if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
							--Leo: Need to change this to check for bodyslot for these tags.
							other:DoTaskInTime(0.1, function(inst) 
								other.Physics:SetMotorVelOverride(-TUNING.FORGE.KNOCKBACK_RESIST_SPEED, 0, 0) 
							end)
							else
								other:DoTaskInTime(0, function(inst) 
									other.Physics:SetMotorVelOverride(-TUNING.FORGE.SWINECLOPS.ATTACK_KNOCKBACK, 0, 0) 
								end)
							end
								other:DoTaskInTime(0.4, function(inst) 
								other.Physics:ClearMotorVelOverride()
								other.Physics:Stop()
							end)
						end
					end
				end
			end
		end
	end
end
-------------------------------------------------
--------------------------------------------------------------------------

local function OnCameraFocusDirty(inst)
    if inst._camerafocus:value() then
        if inst:HasTag("NOCLICK") then
            --death
            TheFocalPoint.components.focalpoint:StartFocusSource(inst, nil, nil, 6, 22, 3)
        else
            --pose
            TheFocalPoint.components.focalpoint:StartFocusSource(inst, nil, nil, 60, 60, 3)
            TheCamera:SetDistance(30)
            TheCamera:SetControllable(false)
        end
    else
        TheFocalPoint.components.focalpoint:StopFocusSource(inst)
        TheCamera:SetControllable(true)
    end
end

local function EnableCameraFocus(inst, enable)
    if enable ~= inst._camerafocus:value() then
        inst._camerafocus:set(enable)
        if not TheNet:IsDedicated() then
            OnCameraFocusDirty(inst)
        end
    end
end

--------------------------------------------------------------------------

local function CreateFlower()
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("healing_flower")
    inst.AnimState:SetBuild("healing_flower")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:Hide("shadow")

    return inst
end

local function OnFlowerHitGround(flower)
    flower.AnimState:Show("shadow")
    flower.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt", nil, .35)
end

local function CheckPose(flower, inst)
    if not (inst:IsValid() and inst.AnimState:IsCurrentAnimation("end_pose_loop")) then
        ErodeAway(flower, 1)
    end
end

local function OnSpawnFlower(inst)
    local flower = CreateFlower()
    local x, y, z = inst.Transform:GetWorldPosition()
    local vec = TheCamera:GetRightVec()
    flower.Physics:Teleport(x, 7, z)
    flower.Physics:SetVel(vec.x * 5, 20, vec.z * 5)
    flower:DoTaskInTime(1.23, OnFlowerHitGround)
    flower:DoPeriodicTask(.5, CheckPose, nil, inst)
end

--------------------------------------------------------------------------

-------------------------------------------------

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	return FindEntity(inst, 200, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and  end, nil, {"LA_mob", "battlestandard" })
end

local function KeepTarget(inst, target)
	return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and (not target:HasTag("wall") and inst:IsNear(target, 6))
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") then
		inst.components.combat:SetTarget(data.attacker)
		
	end
end
-----------

local function OnAttackOther(inst, data)
    --inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("LA_mob") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
end

local function EnterPhase1Trigger(inst)
    --don't remove this yet.
end

local function ShieldCheck(inst)
	if not inst.isguarding and inst.components.health:GetPercent() < 0.9 and (inst.components.combat.laststartattacktime + 8) < GetTime() then
		inst:PushEvent("entershield")
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst.DynamicShadow:SetSize(4.5, 2.25)
    inst.Transform:SetFourFaced()
    inst.Transform:SetScale(1.05, 1.05, 1.05)

    inst:SetPhysicsRadiusOverride(1.75)
    MakeCharacterPhysics(inst, 500, inst.physicsradiusoverride)

    inst.AnimState:SetBank("beetletaur")
    inst.AnimState:SetBuild("lavaarena_beetletaur")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddTag("LA_mob")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("largecreature")
    inst:AddTag("epic")

    --fossilizable (from fossilizable component) added to pristine state for optimization
    inst:AddTag("fossilizable")

    inst._bufftype = net_tinybyte(inst.GUID, "beetletaur._bufftype", "bufftypedirty")
    inst._camerafocus = net_bool(inst.GUID, "beetletaur._camerafocus", "camerafocusdirty")

    inst._spawnflower = net_event(inst.GUID, "beetletaur._spawnflower")
    inst:ListenForEvent("beetletaur._spawnflower", OnSpawnFlower)

    inst:AddComponent("spawnfader")
	
	inst.nameoverride = "beetletaur" --So we don't have to make the describe strings.
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("fossilizable")

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = TUNING.FORGE.SWINECLOPS.WALKSPEED
    inst.components.locomotor.runspeed = TUNING.FORGE.SWINECLOPS.RUNSPEED
    inst:SetStateGraph("SGswineclops")
	
	inst.sg.mem.radius = inst.physicsradiusoverride

    inst:SetBrain(brain)

    inst:AddComponent("follower")
	
	inst:AddComponent("bloomer")
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.SWINECLOPS.HEALTH)
	if TheWorld.components.lavaarenaevent:GetCurrentRound() == #TheWorld.components.lavaarenaevent.rounds_data then
		inst.EnableCameraFocus = EnableCameraFocus
		inst.components.health.nofadeout = true
	else
		inst.components.health.destroytime = 7
	end
	inst.PoseFocus = EnableCameraFocus
    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	
	inst.altattack = true
	inst.is_enraged = false
	inst.currentcombo = 0
	inst.maxpunches = 1
	inst.shieldtime_end = true

	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > 0.9 then
			inst.maxpunches = 1
		elseif health >  0.7 and health <= 0.9 then
			inst.maxpunches = 2
		elseif health > 0.5 and health <= 0.7 then
			inst.maxpunches = 5
		elseif health > 0.25 and health <= 0.5 then
			inst.maxpunches = 7
		elseif health <= 0.25 then
			inst.maxpunches = 998
		end	
	
	end)
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	
	--inst:DoTaskInTime(TUNING.FORGE.SWINECLOPS.SHEILD_CD, function(inst) inst.canshield = true end)
	--inst:DoTaskInTime(0, GetVariation)
	
	
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(0.9, EnterPhase1Trigger) 

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE.SWINECLOPS.ATTACK_RANGE, TUNING.FORGE.SWINECLOPS.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.SWINECLOPS.DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.SWINECLOPS.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.battlecryenabled = false
	inst.components.combat:SetDamageType(1)
	
	inst:DoTaskInTime(5, function(inst) inst:PushEvent("entershield") end)
	
	inst:AddComponent("armorbreak_debuff")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('swineclops')

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
	inst.components.combat.onhitotherfn = OnHitOther
	
    MakeHauntablePanic(inst)
	
	MakeMediumBurnableCharacter(inst, "body")
	
	inst:DoPeriodicTask(1, ShieldCheck)
	
	

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)

    return inst
end

local function OnTick(inst, target)
	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx" or "attack_fx3")
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
	--inst.tickcount = 0
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
	
	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx_pre" or "attack_fx3_pre")
	
	if target.components.combat then
		if inst.type and inst.type == "defense" then
			target.components.combat:AddDamageBuff("swineclops_defensebuff", 0.5, true)
		else
			target.components.combat:AddDamageBuff("swineclops_attackbuff", 1.5, false)
		end
	end
	
    inst:ListenForEvent("death", function()
        --inst.components.debuff:Stop()
    end, target)
end

local function OnDetached(inst, target)
	if target.components.combat then
		if inst.type and inst.type == "defense" then
			target.components.combat:RemoveDamageBuff("swineclops_defensebuff", true)
		else
			target.components.combat:RemoveDamageBuff("swineclops_attackbuff")
		end
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == "swineclopsbuffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)

	inst.AnimState:PlayAnimation(inst.type == "defense" and "defend_fx_pre" or "attack_fx3_pre")
	
	if inst.task then
		inst.task:Cancel()	
		inst.task = nil
	end	
	inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
end

local function fx_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("beetletaur_break")
    inst.AnimState:SetBuild("lavaarena_beetletaur_break")
    inst.AnimState:PlayAnimation("anim", false)

    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	inst:ListenForEvent("animover", inst.Remove)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false 

    return inst
end

--too lazy to make a proper function for these right now. Will fix before proper release.

local function dot_fn()
	local inst = CreateEntity()
	
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	

    inst.AnimState:SetBank("lavaarena_beetletaur_fx")
    inst.AnimState:SetBuild("lavaarena_beetletaur_fx")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.type = "defense"
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
	
	--inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.
	
	return inst
	
end

local function dot2_fn()
	local inst = CreateEntity()
	
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	

    inst.AnimState:SetBank("lavaarena_beetletaur_fx")
    inst.AnimState:SetBuild("lavaarena_beetletaur_fx")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.type = "attack"
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
	
	--inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.
	
	return inst
	
end


return ForgePrefab("swineclops", fn, assets, prefabs, nil, "ENEMIES", false, "images/swineclops_icon.xml", "swineclops_icon.tex", nil, TUNING.FORGE.SWINECLOPS, STRINGS.FORGED_FORGE.ENEMIES.SWINECLOPS.ABILITIES),
Prefab("swineclops_defensebuff", dot_fn),
Prefab("swineclops_attackbuff", dot2_fn),
Prefab("swineclops_break_fx", fx_fn)