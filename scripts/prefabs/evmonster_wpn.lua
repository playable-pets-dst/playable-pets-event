local assets =
{
	Asset( "IMAGE", "images/inventoryimages/evmonster_wpn.tex" ),
    Asset( "ATLAS", "images/inventoryimages/evmonster_wpn.xml" ),
}

local function Remove(inst)
	if TheNet:GetServerGameMode() ~= "lavaarena" then --don't get deleted in forge
		inst:DoTaskInTime(1.2, function() inst:Remove() end)
	end	
end


local function onequip(inst, owner)
    if not owner:HasTag("monster_user") and owner._ismonster == nil then --eventually remove the tag entirely.
		inst:Remove()	
	end	
end

local function onunequip(inst, owner)
    inst.components.weapon:SetDamage(0)
	inst.components.weapon:SetRange(1)
	inst.components.weapon:SetProjectile(nil)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("nightmarefuel")
    inst.AnimState:SetBuild("nightmarefuel")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:SetMultColour(1, 1, 1, 0.5)

    inst:AddTag("monsterwpn")
	inst:AddTag("nosteal")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(0)

    -------

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	
	inst.components.inventoryitem.atlasname = "images/inventoryimages/evmonster_wpn.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	

	--inst.components.inventoryitem:SetOnPickupFn(onequip)
	inst.components.inventoryitem:SetOnDroppedFn(Remove)
	--inst.components.inventoryitem.canbepickedup = false
	--inst.components.inventoryitem.cangoincontainer = false
	--inst:ListenForEvent("forgetiteminventory", Remove(inst))
    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("evmonster_wpn", fn, assets)