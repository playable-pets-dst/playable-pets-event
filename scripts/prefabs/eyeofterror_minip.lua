local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local assets = 
{
	Asset("ANIM", "anim/eyeofterror_mini_actions.zip"),
    Asset("ANIM", "anim/eyeofterror_mini_basic.zip"),
    Asset("ANIM", "anim/eyeofterror_mini_mob_build.zip"),
}

local prefabs = 
{	

}
	
local prefabname = "eyeofterror_minip"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.EYEOFTERROR_MINIP_HEALTH,
	hunger = TUNING.EYEOFTERROR_MINIP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.EYEOFTERROR_MINIP_SANITY,
	runspeed = 4,
	walkspeed = 4,
	
	attackperiod = 0,
	damage = TUNING.EYEOFTERROR_MINI_DAMAGE*2,
	range = TUNING.EYEOFTERROR_MINI_ATTACK_RANGE,
	hit_range = TUNING.EYEOFTERROR_MINI_HIT_RANGE,
	
	bank = "eyeofterror_mini",
	shiny = "eyeofterror_mini",
	build = "eyeofterror_mini_mob_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGeyeofterror_minip",
	minimap = "eyeofterror_minip.tex",
	
}

local creature_sounds =
{
    hit = "terraria1/mini_eyeofterror/hit",
    death = "terraria1/mini_eyeofterror/death",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable("eyeofterror_minip",
-----Prefab---------------------Chance------------
{
    {"monstermeat",                     1.00},
})
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Mob Functions
--==============================================

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.EYEOFTERRORP)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	MakeInventoryFloatable(inst, "med", 0.1, 0.75)    

	--------Tags-------------
	inst:AddTag("eyeofterror")
    inst:AddTag("flying")
    inst:AddTag("monster")
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end

local master_postinit = function(inst) 
	inst.sounds = creature_sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound(creature_sounds.hit)
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--
	
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	inst.mobsleep = true
	inst.shouldwalk = true
	
	local body_symbol = "glomling_body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, "glomling_body")
    MakeTinyFreezableCharacter(inst, "glomling_body")
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	inst.components.combat.hiteffectsymbol = "glomling_body"
	----------------------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	----------------------------------------------
	--Components--	
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_LARGE
	----------------------------------------------
	--Physics and Scale--
	MakeFlyingCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(.8, .5)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) 
			PlayablePets.RevRestore(inst, mob, true, true)
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
