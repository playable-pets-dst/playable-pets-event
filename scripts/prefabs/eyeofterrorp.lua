local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local assets = 
{
	Asset("ANIM", "anim/eyeofterror_action.zip"),
    Asset("ANIM", "anim/eyeofterror_basic.zip"),
	Asset("ANIM", "anim/eyeofterror_basic_alt1.zip"),
	Asset("ANIM", "anim/eyeofterror_twin1_build.zip"),
    Asset("ANIM", "anim/eyeofterror_twin2_build.zip"),
}

local prefabs = 
{	

}
	
local prefabname = "eyeofterrorp"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.EYEOFTERRORP_HEALTH,
	hunger = TUNING.EYEOFTERRORP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.EYEOFTERRORP_SANITY,
	runspeed = 4.5,
	walkspeed = 4.5,
	
	attackperiod = TUNING.EYEOFTERROR_ATTACKPERIOD,
	damage = BOSS_STATS and TUNING.EYEOFTERROR_DAMAGE*2 or 75*2,
	range = TUNING.EYEOFTERROR_ATTACK_RANGE,
	hit_range = TUNING.EYEOFTERROR_ATTACK_RANGE,
	
	bank = "eyeofterror",
	shiny = "eyeofterror",
	build = "eyeofterror_basic",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGeyeofterrorp",
	minimap = "eyeofterrorp.tex",
	
}

local soundpath = "terraria1/eyeofterror/"

local EYE_DATA =
{
    cooldowns =
    {
        charge =                TUNING.EYEOFTERROR_CHARGECD,
        mouthcharge =           TUNING.EYEOFTERROR_MOUTHCHARGECD,
        spawn =                 TUNING.EYEOFTERROR_SPAWNCD,
        focustarget =           TUNING.EYEOFTERROR_FOCUSCD,
    },
    chargedata =
    {
        eyechargespeed =        TUNING.EYEOFTERROR_CHARGESPEED,
        eyechargetimeout =      1.00,
        mouthchargespeed =      1.25*TUNING.EYEOFTERROR_CHARGESPEED,
        mouthchargetimeout =    1.00,
        tauntchance =           1.00,
    },
    health = TUNING.EYEOFTERROR_HEALTH,
    damage = TUNING.EYEOFTERROR_DAMAGE,
    chompdamage = TUNING.EYEOFTERROR_AOE_DAMAGE,
    mouthspawncount = TUNING.EYEOFTERROR_MINGUARDS_PERSPAWN,
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable("eyeofterrorp",
-----Prefab---------------------Chance------------
{
    {"eyemaskhat",                      1.00},
    {"milkywhites",                     1.00},
    {"milkywhites",                     1.00},
    {"milkywhites",                     1.00},
    {"milkywhites",                     0.50},
    {"milkywhites",                     0.50},
    {"monstermeat",                     1.00},
    {"monstermeat",                     1.00},
    {"monstermeat",                     0.50},
    {"monstermeat",                     0.50},
})
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		if data.is_transformed then
			inst.AnimState:Show("mouth")
			inst.AnimState:Show("ball_mouth")

			inst.AnimState:Hide("eye")
			inst.AnimState:Hide("ball_eye")

			inst.sg.mem.transformed = true
		end
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	if inst.sg.mem.transformed then
		data.is_transformed = inst.sg.mem.transformed
	end
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Mob Functions
--==============================================
local function eyeofterror_should_transform(inst, health_data)
    if health_data and health_data.newpercent < TUNING.EYEOFTERROR_TRANSFORMPERCENT and not inst.sg.mem.transformed then
        inst:PushEvent("health_transform")
	elseif inst.sg.mem.transformed and (inst.components.health:GetPercent() >= 1 or (health_data and health_data.newpercent >= 1)) then
		inst.sg.mem.transformed = false
		
		inst.AnimState:Hide("mouth")
        inst.AnimState:Hide("ball_mouth")

        inst.AnimState:Show("eye")
        inst.AnimState:Show("ball_eye")
    end
end

local function eyeofterror_setspawntarget(inst, target)
    inst.components.combat:SetTarget(target)
end

local function OnAttacked(inst, data)
    -- Target our attackers, unless it's one of our soldiers somehow.
    if data.attacker and not inst.components.commander:IsSoldier(data.attacker) then
        inst.components.commander:ShareTargetToAllSoldiers(data.attacker)
    end
end

local function ClearRecentlyCharged(inst)
    inst._recentlycharged = nil
end

local function on_other_collided(inst, other)
    if not other.components.health or other.components.health:IsDead() then
        return
    end

    -- Lazy initialize the recently charged list if it doesn't exist yet.
    -- If it does, check if there's an existing timestamp for this "other".
    local current_time = GetTime()
    local prev_value = nil
    if inst._recentlycharged == nil then
        inst._recentlycharged = {}
    else
        prev_value = inst._recentlycharged[other]
    end

    -- If we had a timestamp for this "other" and hit it too recently, don't hit it again.
    if prev_value ~= nil and prev_value - current_time < 3 then
        return
    end
    inst._recentlycharged[other] = current_time

    inst.components.combat:DoAttack(other)
end

local function OnCollide(inst, other)
    if other ~= nil and other:IsValid() then
        on_other_collided(inst, other)
    end
end

local function GetDesiredSoldiers(inst)
   return (inst.sg.mem.transformed and TUNING.EYEOFTERROR_MOUTH_MINGUARDS)
            or TUNING.EYEOFTERROR_EYE_MINGUARDS
end

local function OnFinishedLeaving(inst)
    inst._leftday = TheWorld.state.cycles
end

local HEALTH_PER_DAY = TUNING.EYEOFTERROR_HEALTHPCT_PERDAY*TUNING.EYEOFTERROR_HEALTH
local MAX_GAIN_DAYS = 1/TUNING.EYEOFTERROR_HEALTHPCT_PERDAY
local function FlybackHealthUpdate(inst)
    if inst._leftday ~= nil then
        local day_difference = math.min(TheWorld.state.cycles - inst._leftday, MAX_GAIN_DAYS)
        if day_difference > 0 then
            inst.components.health:DoDelta(day_difference * HEALTH_PER_DAY)
        end

        if inst._transformonhealthupdate then
            if inst.components.health:GetPercent() > TUNING.EYEOFTERROR_TRANSFORMPERCENT then
                inst.AnimState:Hide("mouth")
                inst.AnimState:Hide("ball_mouth")
                inst.AnimState:Show("eye")
                inst.AnimState:Show("ball_eye")
                inst.sg.mem.transformed = false
            end
        end

        inst._leftday = nil
    end
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.EYEOFTERRORP)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	MakeInventoryFloatable(inst, "large")

    

	--------Tags-------------
	inst:AddTag("eyeofterror")
    inst:AddTag("epic")
    inst:AddTag("flying")
    inst:AddTag("largecreature")
    inst:AddTag("monster")
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end

local master_postinit = function(inst) 
	inst._soundpath = soundpath
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--
	
	inst:AddTag("monster")
    inst:AddTag("LA_mob")
	
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	inst.mobsleep = true
	inst.shouldwalk = true
	inst._isflying = false
	inst.minions = {}
	
	inst.hit_recovery = 1

    inst._cooldowns = EYE_DATA.cooldowns
    inst._chargedata = EYE_DATA.chargedata
    inst._mouthspawncount = EYE_DATA.mouthspawncount
    inst._chompdamage = EYE_DATA.chompdamage
	
	inst.OnCollide = OnCollide
    inst.ClearRecentlyCharged = ClearRecentlyCharged
    inst.GetDesiredSoldiers = GetDesiredSoldiers
    inst.FlybackHealthUpdate = FlybackHealthUpdate
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	inst.components.combat.hiteffectsymbol = "swap_fire"
	
	inst.AnimState:Hide("ball_mouth")
    inst.AnimState:Hide("mouth")
	----------------------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	----------------------------------------------
	--Components--
	inst:AddComponent("timer")
	
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_LARGE
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.EYEOFTERROR_EPICSCARE_RANGE)
	
	inst:AddComponent("commander")
	inst.components.commander:SetTrackingDistance(40)
	----------------------------------------------
	--Physics and Scale--
	MakeTinyFlyingCharacterPhysics(inst, 100, 1.5) 
    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("healthdelta", eyeofterror_should_transform)
	inst:ListenForEvent("attacked", OnAttacked)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) 
			PlayablePets.RevRestore(inst, mob, true, true)
			inst.AnimState:Hide("mouth")
			inst.AnimState:Hide("ball_mouth")

			inst.AnimState:Show("eye")
			inst.AnimState:Show("ball_eye")
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
