require "prefabutil"

local assets =
{
	Asset("ANIM", "anim/lavaarena_banner.zip"),
}

local prefabs =
{
    "collapse_small",
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PushAnimation("idle")
    end
end

local function onsave(inst, data)
    
end

local function onload(inst, data)

end

local function onbuilt(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/sign_craft")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, .3)

    inst.AnimState:SetBank("lavaarena_banner")
    inst.AnimState:SetBuild("lavaarena_banner")
    inst.AnimState:PlayAnimation("idle")
	
	inst.Transform:SetEightFaced()
	inst.Transform:SetScale(1.5, 1.5, 1.5)

    MakeSnowCoveredPristine(inst)

    inst:AddTag("structure")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
    MakeSnowCovered(inst)

	inst:AddComponent("savedrotation")
    --MakeSmallBurnable(inst, nil, nil, true)
    --MakeSmallPropagator(inst)
    inst.OnSave = onsave
    inst.OnLoad = onload

    MakeHauntableWork(inst)
    inst:ListenForEvent("onbuilt", onbuilt)

    return inst
end

return Prefab("forge_banner", fn, assets, prefabs),
    MakePlacer("forge_banner_placer", "lavaarena_banner", "lavaarena_banner", "idle", nil, nil, nil, nil, -90, "eight")
