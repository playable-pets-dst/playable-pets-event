require "prefabutil"
require "recipes"

local assets =
{
    Asset("ANIM", "anim/gingerbread_house1.zip"),
    Asset("ANIM", "anim/gingerbread_house2.zip"),
    Asset("ANIM", "anim/gingerbread_house3.zip"),
    Asset("ANIM", "anim/gingerbread_house4.zip"),
}

local prefabs =
{
    "collapse_small",
    "crumbs"
}

local loot =
{
    "crumbs",
    "crumbs",
	"crumbs",
	"crumbs",
	"crumbs"
}

local animdata = 
{
    { build = "gingerbread_house1", bank = "gingerbread_house1" },
    { build = "gingerbread_house3", bank = "gingerbread_house2" },
    { build = "gingerbread_house2", bank = "gingerbread_house2" },
    { build = "gingerbread_house4", bank = "gingerbread_house1" },
}

local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    
	local x, y, z = inst.Transform:GetWorldPosition()
	
	if not inst:HasTag("burnt") then
		for i, v in ipairs(loot) do
			inst.components.lootdropper:SpawnLootPrefab(v, Point(x,y,z))
		end
        if math.random() < 0.3 then
            local gingerdeadman = SpawnPrefab("gingerdeadpig")
            gingerdeadman.Transform:SetPosition(x, y, z)            
        end
    end
	
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then 
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

local function onbuilt(inst)

end

local function OnSave(inst, data)
    data.build = inst.build
    data.bank = inst.bank
end

local function OnLoad(inst, data)
    if data ~= nil then
        inst.build = data.build or animdata[1].build
        inst.bank = data.bank or animdata[1].bank

        inst.AnimState:SetBuild(inst.build)
        inst.AnimState:SetBank(inst.bank)
    end
end

local function onignite(inst)
    inst.components.sleepingbag:DoWakeUp()
end

local function onburntup(inst)
    inst.AnimState:PlayAnimation("burnt_rundown")
end

local function wakeuptest(inst, phase)
    if phase ~= inst.sleep_phase then
        inst.components.sleepingbag:DoWakeUp()
    end
end

local function onwake(inst, sleeper, nostatechange)
    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
        inst.sleeptask = nil
    end

    inst:StopWatchingWorldState("phase", wakeuptest)
    sleeper:RemoveEventCallback("onignite", onignite, inst)

    if not nostatechange then
        if sleeper.sg:HasStateTag("tent") then
            sleeper.sg.statemem.iswaking = true
        end
		inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        sleeper.sg:GoToState("wakeup")
    end

    if inst.sleep_anim ~= nil then
        --inst.AnimState:PushAnimation("idle", true)
    end

    --inst.components.finiteuses:Use()
end

local function onsleeptick(inst, sleeper)
    local isstarving = sleeper.components.beaverness ~= nil and sleeper.components.beaverness:IsStarving()
	
	if not sleeper:HasTag("gingerbread_pig") then
		--print ("Too bad he isn't a spider!")
		 inst.components.sleepingbag:DoWakeUp()
	end
	
    if sleeper.components.hunger ~= nil then
        sleeper.components.hunger:DoDelta(inst.hunger_tick, true, true)
        isstarving = sleeper.components.hunger:IsStarving()
    end

    if sleeper.components.sanity ~= nil and sleeper.components.sanity:GetPercentWithPenalty() < 1 then
        sleeper.components.sanity:DoDelta(TUNING.SLEEP_SANITY_PER_TICK, true)
    end

    if not isstarving and sleeper.components.health ~= nil then
        sleeper.components.health:DoDelta(TUNING.SLEEP_HEALTH_PER_TICK * 2, true, inst.prefab, true)
    end

    if sleeper.components.temperature ~= nil then
        if inst.is_cooling then
            if sleeper.components.temperature:GetCurrent() > TUNING.SLEEP_TARGET_TEMP_TENT then
                sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
            end
        elseif sleeper.components.temperature:GetCurrent() < TUNING.SLEEP_TARGET_TEMP_TENT then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
        end
    end

    if isstarving then
        inst.components.sleepingbag:DoWakeUp()
    end
end

local function onsleep(inst, sleeper)
    inst:WatchWorldState("phase", wakeuptest)
    sleeper:ListenForEvent("onignite", onignite, inst)
	

    --if inst.sleep_anim ~= nil then
        --inst.AnimState:PlayAnimation(inst.sleep_anim, true)
    --end

    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
    end
	
	
	inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, sleeper)
	
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    local index = math.random(#animdata)
    inst.build = animdata[index].build
    inst.bank  = animdata[index].bank

    inst.AnimState:SetBank (inst.bank)
    inst.AnimState:SetBuild(inst.build)
    inst.AnimState:PlayAnimation("idle", true)
	
	inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK
	
	inst:AddTag("tent")
    inst:AddTag("structure")

    inst.nameoverride = "gingerbreadhouse"

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	
	inst:AddComponent("sleepingbag")
    inst.components.sleepingbag.onsleep = onsleep
    inst.components.sleepingbag.onwake = onwake
    inst.components.sleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)
	
    inst:AddComponent("lootdropper")

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)

    inst:AddComponent("inspectable")

	MakeSmallBurnable(inst, nil, nil, true)
    MakeMediumPropagator(inst)
	
    return inst
end

return Prefab("gingerbread_pig_housep", fn, assets, prefabs),
	MakePlacer("gingerbread_pig_housep_placer", "gingerbread_house2", "gingerbread_house2", "idle")
