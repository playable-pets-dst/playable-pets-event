local MakePlayerCharacter = require "prefabs/player_common"

---------------------------



local assets = 
{
	Asset("ANIM", "anim/warg_actions.zip"),
	Asset("ANIM", "anim/warg_gingerbread.zip"),
	Asset("ANIM", "anim/warg_gingerbread_build.zip"),
}



local prefabs = 
{	
	"warg_gooicing",
	"wintersfeastfuel",
    "houndstooth",
	"crumbs",
}
	
local prefabname = "gingerbread_vargp"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.WARG_HEALTH,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.WARG_RUNSPEED,
	walkspeed = TUNING.WARG_RUNSPEED/2,
	
	attackperiod = 3,
	damage = 65*2,
	range = 3,
	hit_range = 5,
	
	bank = "warg",
	shiny = "warg_gingerbread",
	build = "warg_gingerbread_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",
	
}

local sounds_gingerbread =
{
    idle = "dontstarve_DLC001/creatures/vargr/idle",
    howl = "dontstarve_DLC001/creatures/vargr/howl",
    hit = "dontstarve_DLC001/creatures/vargr/hit",
    attack = "dontstarve_DLC001/creatures/vargr/attack",
    death = "dontstarve_DLC001/creatures/vargr/death",
    sleep = "dontstarve_DLC001/creatures/vargr/sleep",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('gingerbread_vargp',
{
    {'wintersfeastfuel',		1.00},
    {'wintersfeastfuel',		1.00},
    {'wintersfeastfuel',		1.00},
    {'wintersfeastfuel',		1.00},
    {'wintersfeastfuel',		1.00},
    {'wintersfeastfuel',        0.66},
    {'wintersfeastfuel',        0.33},
    {'crumbs',					1.00},
    {'crumbs',					1.00},
    {'crumbs',					0.50},
    {'crumbs',					0.50},

    {'houndstooth',             1.00},
    {'houndstooth',             0.66},
    {'houndstooth',             0.33},
})
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function LaunchGooIcing(inst)
	local theta = math.random() * 2 * PI
	local r = inst:GetPhysicsRadius(0) + 0.25 + math.sqrt(math.random()) * TUNING.WARG_GINGERBREAD_GOO_DIST_VAR
	local x, y, z = inst.Transform:GetWorldPosition()
	local dest_x, dest_z = math.cos(theta) * r + x, math.sin(theta) * r + z

	local goo = SpawnPrefab("warg_gooicingp")
    goo.Transform:SetPosition(x, y, z)
	goo.Transform:SetRotation(theta / DEGREES)
	goo._caster = inst

	Launch2(goo, inst, 1.5, 1, 3, .75)

	inst._next_goo_time = GetTime() + TUNING.WARG_GINGERBREAD_GOO_COOLDOWN
end

local function NoGooIcing()
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.GINGERBREADVARG)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books","staves"})
	end)
	
	inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.15, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	inst.acidmult = 1.5
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false)
		end
	end)
	
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/vargr/hit")
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--
	
	inst:AddTag("monster")
	inst:AddTag("houndfriend")
	inst:AddTag("gingerbread")
	inst:AddTag("largecreature")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true

	inst.sounds = sounds_gingerbread
	
	
	
	inst.hit_recovery = 1
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.LaunchGooIcing = LaunchGooIcing
	----------------------------------------------
	--Eater--
    --inst.components.eater:SetDiet({ FOODTYPE.GENERIC, FOODTYPE.GENERIC }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	----------------------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 1000, 1.5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
