local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/quagmire_goatkid_basic.zip"),
	Asset("ANIM", "anim/goatkid_shiny_build_06.zip"),
}

local getskins = {"6"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 150,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 250,
	runspeed = 4,
	walkspeed = 4,
	damage = 5, --*5 for nagging attack
	attackperiod = 0,
	range = 8, --*for nagging attack
	bank = "quagmire_goatkid_basic",
	shiny = "goatkid",
	build = "quagmire_goatkid_basic",
	scale = 0.8,
	--build2 = "alternate build here",
	stategraph = "SGgoatkidp",
	minimap = "goatkidp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('goatkidp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
})

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/goat_kid/talk")
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
------------------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.GOATKID)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	inst:AddComponent("buffable")
	--if inst.components.buffable then
		inst.components.buffable:AddBuff("heal_dealt",{{name = "heal_dealt", val = 1.25, type = "mult"}})
	--end
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.reforged_items = {[2] = {"lavaarena_spatula"}, [3] = {"lavaarena_chefhat"}}
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)
	inst:AddTag("masterchef")
    inst:AddTag("professionalchef")
    inst:AddTag("expertchef")
	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
     PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('goatkidp')
	----------------------------------
	--Tags--	
    inst:AddTag("QM_mob")
	
	inst.specialsleepev = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)	
	
	inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .4)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("goatkidp", prefabs, assets, common_postinit, master_postinit, start_inv)
