local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------
local prefabname = "golemp"

local assets = 
{
	Asset("ANIM", "anim/lavaarena_elemental_basic.zip"),
	Asset("ANIM", "anim/asunton_golem.zip"),
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{

}

-----------------------
local mob = 
{
	health = 150,
	hunger = 20,
	hungerrate = 0,
	sanity = 20,
	runspeed = 10,
	walkspeed = 5,
	attackperiod = 0,
	damage = 32,
	hit_range = 99,
	range = 15, 
	bank = "lavaarena_elemental_basic",
	build = "lavaarena_elemental_basic",
	shiny = "golem",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SG"..prefabname.."",
	minimap = prefabname..".tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
})
------------------------------------------------------
 local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.MAGMA_GOLEM)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"staves", "books", "melees", "darts"})
	end)
	
	inst.revive_delay = 1
	inst.components.health:StopRegen()
	
	inst.components.revivablecorpse.revivespeedmult = 999
	
	inst.components.combat:SetDamageType(2)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------------
local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)
		inst.projectilehue = data.projectilehue or nil
	else
		inst.AnimState:SetBuild(mob.build)
		inst.projectilehue = nil
	end	
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	
end

local RND_OFFSET = 1

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local spit = SpawnPrefab(TheNet:GetServerGameMode() == "lavaarena" and "forge_fireball_projectile" or "fireball_projectilep")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		if inst.projectilehue then
			spit.AnimState:SetHue(inst.projectilehue)
			spit.projectilehue = inst.projectilehue
			spit.AnimState:SetSaturation(1.3)
		end
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end


local master_postinit = function(inst) 
	-------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, 0, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.health:StartRegen(2, 3)
	inst.AnimState:SetLightOverride(0.7)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefabname)
	----------------------------------
	--Tags--
	
	inst.altattack = true
	inst.taunt2 = true
	inst._ismonster = true
	inst.poisonimmune = true
	inst.shouldwalk = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.mobplayer = true
	
	inst.hit_recovery = 0.75
	inst.FireProjectile = FireProjectile
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("body", 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 450, 0.65)
	inst.DynamicShadow:SetSize(1.1, .7)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end




return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
