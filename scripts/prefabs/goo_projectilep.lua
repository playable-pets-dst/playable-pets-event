local assets=
{
	Asset("ANIM", "anim/fireball_2_fx.zip"),
    Asset("ANIM", "anim/deer_fire_charge.zip"),
}

local prefabs = {}

function RotateToTarget(dest)
    local direction = (dest - inst:GetPosition()):GetNormalized()
    local angle = math.acos(direction:Dot(Vector3(1, 0, 0))) / DEGREES
    inst.Transform:SetRotation(angle)
    inst:FacePoint(dest)
end

function OnUpdate(dt)
    local target = target
    if homing and target ~= nil and target:IsValid() and not target:IsInLimbo() then
        dest = target:GetPosition()
    end
    local current = inst:GetPosition()
    if range ~= nil and distsq(start, current) > range * range then
        Miss(target)
    elseif not homing then
        if target ~= nil and target:IsValid() and not target:IsInLimbo() then
            local range = target:GetPhysicsRadius(0) + hitdist
            -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
            -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
            if distsq(current, target:GetPosition()) < range * range then
                Hit(target)
            end
        end
    elseif target ~= nil
        and target:IsValid()
        and not target:IsInLimbo()
        and target.entity:IsVisible()
        and (target.sg == nil or
            not (target.sg:HasStateTag("flight") or
                target.sg:HasStateTag("invisible"))) then
        local range = target:GetPhysicsRadius(0) + hitdist
        -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
        -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
        if distsq(current, target:GetPosition()) < range * range then
            Hit(target)
        else
            local direction = (dest - current):GetNormalized()
            local projectedSpeed = speed * TheSim:GetTickTime() * TheSim:GetTimeScale()
            local projected = current + direction * projectedSpeed
            if direction:Dot(dest - projected) >= 0 then
                RotateToTarget(dest)
            else
                Hit(target)
            end
        end
    elseif owner == nil or
        not owner:IsValid() or
        owner.components.combat == nil or
        inst.components.weapon == nil or
        inst.components.weapon.attackrange == nil then
        -- Lost our target, e.g. bird flew away
        Miss(target)
    else
        -- We have enough info to make our weapon fly to max distance before "missing"
        local range = owner.components.combat.attackrange + inst.components.weapon.attackrange
        if distsq(owner:GetPosition(), current) > range * range then
            Miss(target)
        end
    end
end

local function OnHit(inst, owner, target)
    if target ~= nil and target:IsValid() then
        --target.components.combat:GetAttacked(owner, 50)
    end
	inst.DoTrail:Cancel()
	if inst.type then
		local fx = SpawnPrefab("fireball_projectile_hitp")
		if inst.projectilehue then
			fx.AnimState:SetHue(inst.projectilehue)
		end
		fx.Transform:SetPosition(inst:GetPosition():Get())
		fx:DoTaskInTime(fx.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, fx.Remove)
		inst:Remove()
	else
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_hit")
		inst.AnimState:PlayAnimation("blast")
		inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)
	end	
end

local function OnMiss(inst)
	inst.DoTrail:Cancel()
	if inst.type and inst.type == "fire" then
		local fx = SpawnPrefab("fireball_projectile_hitp")
		if inst.projectilehue then
			fx.AnimState:SetHue(inst.projectilehue)
		end
		fx.Transform:SetPosition(inst:GetPosition():Get())
		inst:Remove()
	else
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_hit")
		inst.AnimState:PlayAnimation("blast")
		inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)
	end	
end

local function OnThrown(inst)
	inst.DoTrail = inst:DoPeriodicTask(0.1, function()
		local pos = inst:GetPosition()
		
		local trail = inst.type and  SpawnPrefab("fire_tailp") or SpawnPrefab("peghook_tailp")
		trail.Transform:SetPosition(pos.x, pos.y, pos.z)	
		if inst.projectilehue then
			trail.AnimState:SetHue(inst.projectilehue)
			trail.AnimState:SetSaturation(1.3)
		end
		
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	end)
end



local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
		inst.trailtask = nil
	end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("gooball_fx")
	inst.AnimState:SetBuild("gooball_fx")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst.AnimState:SetMultColour(.2, 1, 0, 1)
	inst:SetPrefabNameOverride("snapper")
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	
	if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("projectile")
	inst.components.projectile:SetSpeed(20)
	inst.components.projectile:SetRange(30)
    inst.components.projectile:SetOnThrownFn(OnThrown)
    inst.components.projectile:SetOnHitFn(OnHit)
    inst.components.projectile:SetOnMissFn(OnMiss)
	
	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local function fn_fire()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("fireball_fx")
	inst.AnimState:SetBuild("fireball_2_fx")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst:SetPrefabNameOverride("snapper")

	inst.AnimState:SetLightOverride(1)
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst.type = "fire"

	inst:AddComponent("projectile")
	inst.components.projectile:SetSpeed(20)
	inst.components.projectile:SetRange(30)
    inst.components.projectile:SetOnThrownFn(OnThrown)
    inst.components.projectile:SetOnHitFn(OnHit)
    inst.components.projectile:SetOnMissFn(OnMiss)
	
	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local function fn_fire_hit()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

	inst.AnimState:SetBank("fireball_fx")
	inst.AnimState:SetBuild("deer_fire_charge")
	inst.AnimState:PlayAnimation("blast")
	inst:SetPrefabNameOverride("snapper")
	inst.AnimState:SetLightOverride(1)
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst.OnRemoveEntity = onremove

	inst:DoTaskInTime(2, function(inst) inst:Remove() end)
	
	inst.persists = false

	return inst
end

return Prefab("goo_projectilep", fn, assets, prefabs),
Prefab("fireball_projectilep", fn_fire, assets, prefabs),
Prefab("fireball_projectile_hitp", fn_fire_hit, assets, prefabs)