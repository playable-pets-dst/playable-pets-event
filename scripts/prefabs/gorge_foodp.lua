local assets =
{
    Asset("ANIM", "anim/gorge_foodp.zip"),
	Asset( "IMAGE", "images/inventoryimages/gorge_foodsp.tex" ),
    Asset( "ATLAS", "images/inventoryimages/gorge_foodsp.xml" ),
} 

local function MakeFood(data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

		MakeInventoryFloatable(inst)
		
        inst.AnimState:SetBank("gorge_foodp")
        inst.AnimState:SetBuild("gorge_foodp")
        inst.AnimState:PlayAnimation(data.plating == "bowl" and "idle" or "silver") --TODO: This is to fix a weird anim bug where the wrong plates are playing. Silver makes "plates" plain and idle keeps bowls from going invisible. fix this,
		
		if data.name then
			inst.AnimState:OverrideSymbol("tomato_soup", "gorge_foodp", data.name)
		end
		
		if data.plating and data.plating ~= "bowl" then
			inst.AnimState:OverrideSymbol("bowl", "gorge_foodp", data.plating)
		end
		
		if data.id then
			inst.nameoverride = "quagmire_food_"..data.id
		end

        inst:AddTag("gorge_food")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

		inst:AddComponent("inspectable")
		
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/gorge_foodsp.xml"
		
		inst:AddComponent("edible")
        inst.components.edible.healthvalue = data.health
        inst.components.edible.hungervalue = data.hunger
        inst.components.edible.foodtype = data.foodtype or FOODTYPE.GENERIC
        inst.components.edible.sanityvalue = data.sanity or 0
        inst.components.edible.temperaturedelta = data.temperature or 0
        inst.components.edible.temperatureduration = data.temperatureduration or 0
        if data.oneat then
            inst.components.edible:SetOnEatenFn(data.oneat)
        end
		
		inst:AddComponent("perishable")
        inst.components.perishable:SetPerishTime(data.perishtime or TUNING.PERISH_FASTISH)
        inst.components.perishable:StartPerishing()
        inst.components.perishable.onperishreplacement = "spoiled_food"
		
        return inst
    end

	
    return Prefab("gorge_"..data.name.."p", fn, assets)
end

local prefs = {}

local foods = require("ppev_foods")
for k,v in pairs(foods) do
    table.insert(prefs, MakeFood(v))
end

return unpack(prefs)
--return MakeFood("pizza", "plate")
