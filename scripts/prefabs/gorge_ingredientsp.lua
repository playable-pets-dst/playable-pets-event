local assets =
{
	Asset("ANIM", "anim/quagmire_flour.zip"),
	Asset("ANIM", "anim/quagmire_meat_small.zip"),
	Asset("ANIM", "anim/quagmire_crop_turnip.zip"),
	Asset("ANIM", "anim/quagmire_crop_wheat.zip"),
} 

local function MakeIngredient(data) 
	if data.nameoverride and not data.notassetname then
			table.insert(assets, Asset("ANIM", "anim/"..data.nameoverride..".zip"))
	elseif not data.notassetname then
			table.insert(assets, Asset("ANIM", "anim/gorge_"..data.name.."p.zip"))
	end
	
	local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

		MakeInventoryFloatable(inst)
		
        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim or "idle")
		
		if data.nameoverride then
			inst.nameoverride = data.nameoverride
		end

		if data.tags then
			for i, v in ipairs(data.tags) do
				inst:AddTag(v)
			end
		end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

		inst:AddComponent("inspectable")
		
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.imagename = data.nameoverride or nil
		inst.components.inventoryitem.atlasname = "images/inventoryimages.xml"
		
		if data.edible then
			inst:AddComponent("edible")
			inst.components.edible.healthvalue = data.health or 0
			inst.components.edible.hungervalue = data.hunger or 10
			inst.components.edible.foodtype = data.foodtype or FOODTYPE.GENERIC
			inst.components.edible.sanityvalue = data.sanity or 0
			inst.components.edible.temperaturedelta = data.temperature or 0
			inst.components.edible.temperatureduration = data.temperatureduration or 0
			if data.oneat then
				inst.components.edible:SetOnEatenFn(data.oneat)
			end
		end
		
		if data.stackable then
			inst:AddComponent("stackable")
			inst.components.stackable.maxsize = data.stackable
		end
		
		if data.perish then
			inst:AddComponent("perishable")
			inst.components.perishable:SetPerishTime(data.perish or TUNING.PERISH_FASTISH)
			inst.components.perishable:StartPerishing()
			inst.components.perishable.onperishreplacement = "spoiled_food"
		end
		
		if data.cookable then
			inst:AddComponent("cookable")
			inst.components.cookable.product = data.product
		end
		
        return inst
    end

    return Prefab("gorge_"..data.name.."p", fn, assets)
end

local prefs = {}

local ingredients = require("ppev_ingredients")
for k,v in pairs(ingredients) do
    table.insert(prefs, MakeIngredient(v))
end

return unpack(prefs)
