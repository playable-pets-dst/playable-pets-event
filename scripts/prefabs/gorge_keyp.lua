local assets =
{
    Asset("ANIM", "anim/quagmire_key.zip"),
} 

local function SetID(inst, owner)
	if inst.components.klaussackkey and inst.components.klaussackkey.keytype == "" then
		if owner and owner.userid ~= nil then
			inst.components.klaussackkey.keytype = owner.userid
			if owner:GetDisplayName() ~= nil then
				inst.components.named:SetName(owner:GetDisplayName().."'s Safe Key")
			end
		end
	end
	--print("DEBUG: Key Made, id = "..inst.components.klaussackkey.keytype)
end

local function OnSave(inst, data)
    data.id = inst.components.klaussackkey and inst.components.klaussackkey.keytype or ""
	data.isunlocked = inst._isunlocked ~= nil and inst._isunlocked:value() or nil
	data.displayname = inst.displayname or "Key"
end

local function OnLoad(inst, data)
    if data ~= nil then
		inst.displayname = data.displayname or "Key"	
		if inst.components.klaussackkey then
			inst.components.klaussackkey.keytype = data.id or ""
		end		
    end
end

local function MakeKey(name, anim)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.AnimState:SetBank("quagmire_key")
        inst.AnimState:SetBuild("quagmire_key")
        inst.AnimState:PlayAnimation(anim)

        inst:AddTag("key")

        --klaussackkey (from klaussackkey component) added to pristine state for optimization
        inst:AddTag("klaussackkey")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

		inst:AddComponent("inspectable")
		inst:SetPrefabNameOverride("quagmire_key") --So we don't have to make the describe string
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/"..name..".xml"
		
		
        inst:AddComponent("klaussackkey")
		inst.components.klaussackkey.keytype = ""
		
		
		inst:AddComponent("named")
		
		inst:ListenForEvent("onputininventory", SetID)
		
		inst.OnSave = OnSave 
		inst.OnLoad = OnLoad
		
        return inst
    end

    return Prefab(name, fn, assets)
end

return MakeKey("gorge_keyp", "safe_key"),
    MakeKey("gorge_key_parkp", "park_key")
