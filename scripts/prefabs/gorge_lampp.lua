require "prefabutil"
require "recipes"

local assets =
{
    Asset("ANIM", "anim/quagmire_lamp_post.zip"),
	 Asset("ANIM", "anim/quagmire_lamp_short.zip")
}

local prefabs =
{
    --"merm",
    "collapse_big",

    --loot:
}

local loot =
{
    "cutstone",    
}

local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        if inst.components.childspawner ~= nil then
            inst.components.childspawner:ReleaseAllChildren(worker)
        end
        --inst.AnimState:PlayAnimation("hit_rundown")
        inst.AnimState:PushAnimation("hit")
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("idle")
end

local function onsave(inst, data)
    data.invincible = inst.invincible or nil
end

local function onload(inst, data)
	inst.invincible = data.invincible or nil
end


local function LightFn(bank_build, radius, falloff, intensity, color)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 0.5)


    inst.AnimState:SetBank(bank_build)
    inst.AnimState:SetBuild(bank_build)
	
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:PlayAnimation("idle")
	
    inst:AddTag("structure")
	inst:AddTag("lamppost")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	if inst.invincible == nil then
	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	end
	
	
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetLoot(loot)

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)

	inst:SetPrefabNameOverride(bank_build) --So we don't have to make the describe strings.
	
    inst:AddComponent("inspectable")
	
	inst.Light:Enable(true)
    inst.Light:SetRadius(radius)
    inst.Light:SetFalloff(falloff)
    inst.Light:SetIntensity(intensity)
    inst.Light:SetColour(color / 255, color / 255, color / 255)

    return inst
end


return Prefab("gorge_lampp", LightFn("quagmire_lamp_post", 3.5, 3, 1, 235), assets),
	MakePlacer("gorge_lampp", "quagmire_lamp_post", "quagmire_lamp_post", "idle"),
    Prefab("gorge_lamp2p", LightFn("quagmire_lamp_short", 2, 2.5, 0.75, 200), assets),
	MakePlacer("gorge_lamp2p", "quagmire_lamp_short", "quagmire_lamp_short", "idle")
	
	
