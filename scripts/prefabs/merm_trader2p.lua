local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("ANIM", "anim/merm_trader2_build.zip"),
    Asset("SOUND", "sound/pig.fsb"),
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 125,
	hunger = 75,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = 6,
	walkspeed = 3,
	damage = 30,
	attackperiod = 0,
	range = TUNING.WORM_ATTACK_DIST,
	bank = "pigman",
	shiny = "pipton",
	build = "merm_trader2_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGswamppigp",
	minimap = "merm_trader2p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('merm_trader2p',
-----Prefab---------------------Chance------------
{
    {'fish',			1.00},
	{'froglegs',		1.00},
})

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack")
end

local function UnEquip(inst)
	inst.AnimState:Show("HAT")
end

local function CheckforHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
		inst.AnimState:Show("hat")
	else
		inst.AnimState:Show("hat")
	end 
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
		inst.AnimState:Show("hat")
	else
		inst.AnimState:Show("hat")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
	if not head then
		inst.AnimState:Show("hat")
	end
end 

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("merm") and not dude.components.health:IsDead() end, 10)
end

-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local T3 =
{
	"bacontome",
	"splintmail",
	"steadfastarmor",
	"steadfastgrandarmor",
	"jaggedgrandarmor",
	"silkengrandarmor",
	"whisperinggrandarmor",
	"flowerheadband",
	"noxhelm",
	"resplendentnoxhelm",
	"blossomedwreath",
	"clairvoyantcrown",
	"spiralspear",
	"infernalstaff",
	"blacksmithsedge"

}

local T2 =
{
	"forginghammer",
	"moltendarts",
	"pithpike",
	"reedtunic",
	"featheredwreath",
	"jaggedarmor",
	"silkenarmor",	
	"barbedhelm",
	"wovengarland",
	"firebomb",
	
}

local T1 =
{
	"petrifyingtome",
	"forgedarts",
	"reedtunic",
	"crystaltiara",
	"forge_woodarmor",
}

local T1_CHANCE = 60
local T2_CHANCE = 90
local T3_CHANCE = 100

local function ForgeSpecialAttack(inst, target)
	local roll1 = math.random(1, 100)
	local item1 = ""
	--Leo:okay, this is dumb. Why can't I think of the correct away to do this.
	if roll1 then
		if roll1 <= T1_CHANCE then
			item1 = T1[math.random(1, #T1)]
		elseif roll1 > T1_CHANCE and roll1 <= T2_CHANCE then
			item1 = T2[math.random(1, #T2)]
		elseif roll1 > T2_CHANCE then
			item1 = T3[math.random(1, #T3)]
		end	
	end	
	
	if item1 then
		local item = SpawnPrefab(item1)
		item.Transform:SetPosition(target:GetPosition():Get())
		Launch(item, target, 2)
	end
end

local function OnKillOther_Forge(inst, data)
	if data and data.victim and data.victim.components.lootdropper then
		if math.random(0,10) <= 0.5 then
			ForgeSpecialAttack(inst, data.victim)
		end
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.MERM2)
	
	inst.mobsleep = false
	
	inst.components.combat:AddDamageBuff("atk_banner", 1.2, false)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:ListenForEvent("killed", OnKillOther_Forge)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

-----------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
     PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, nil, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0.5) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('merm_trader2p')
	----------------------------------
	--Tags--
	
	inst:AddTag("merm")
    inst:AddTag("QM_mob")
	
	--inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	
	
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	CheckforHat(inst)	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1.5) --This might multiply food stats.
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", Equip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", Unequip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end




return MakePlayerCharacter("merm_trader2p", prefabs, assets, common_postinit, master_postinit, start_inv)
