local assets=
{
	Asset("ANIM", "anim/gooball_fx.zip"),
	Asset("ANIM", "anim/lavaarena_hits_splash.zip"),
}

--please don't look...
--TODO fucking fix this mess!

local tuning_values = TUNING.PEGHOOKP
local prefabs = {}

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"boarrior_pet", "notarget", "INLIMBO", "shadow", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local function CalcPoisonDamage(inst)
	if inst.components.health then
		local mult = inst.acidmult ~= nil and inst.acidmult or 1
		local issleeping = (inst.sg and inst.sg:HasStateTag("sleeping")) and 0.5 or 1
		local health = inst.components.health.maxhealth
		if health <= 1000 and health > 300 then 
			return health * 0.3 / -8 * mult 	
		elseif health <= 300 then
			return 70 / -8 * mult 
		else	
			return health * 0.15 / -8 * mult 
		end
	else
		print("Acid Error: Target's health is nil!")
		return -4
	end	
end

local function SetPoison(inst)
	if inst.ispoisoned and not inst:HasTag("playerghost") then 
		if inst and inst.components.health and not inst.components.health:IsDead() and not inst:HasTag("structure") then
			inst.components.health:DoDelta(CalcPoisonDamage(inst), false, nil, nil, inst.my_killer or nil, true ) --do more damage on mobs.
			inst:DoTaskInTime(2, function(inst) if inst:HasTag("poisoned") then --check to make sure the colors aren't messed up when cured.
			inst.AnimState:SetAddColour(0.1,0.5,0.1,0) end end) 
		end
	else --you're cured!
		inst.ispoisoned = nil
		inst.my_killer = nil
		inst.AnimState:SetAddColour(0,0,0,0)
		if inst.poisontask ~= nil then
			inst.poisontask = nil
		end
	end
end

local colourtoadd = { 0.1, 0.5, 0.1 }
local colouraddspeed = 1 -- #seconds it takes to reach color

local function OnHit(other, data) --Poison workaround.
	if other.acidimmune == nil and not (other:HasTag("wall") or other:HasTag("structure") or other:HasTag("shadow") or other:HasTag("notarget")) then
		other.ispoisoned = true
		if other and other.components.health and not other.components.health:IsDead() then
			if other.poisontask == nil then
				other.AnimState:SetAddColour(0.1,0.5,0.1, 0)
				other.poisontask = other:DoPeriodicTask(2, SetPoison)
				if not other.acidimmune and not other.poisonable then
					other.poisonwearofftask = other:DoTaskInTime(16, function(inst) 
						if inst.poisontask then 
							inst.poisontask:Cancel() 
							inst.my_killer = nil 
							inst.poisontask = nil 
							if inst.components.colourtweener then
								inst.components.colourtweener:StartTween({1,1,1,1}, 2)
								if inst.components.bloomer then
									inst.components.bloomer:PopBloom("acid")
								end
							else
								inst.AnimState:SetMultColour(1,1,1,1) 
							end
							inst.AnimState:SetAddColour(0, 0, 0, 0)
							inst.ispoisoned = nil
						end 
					end)
				end
			end
		end
	end
end	

local function onthrown(inst, thrower, pt, time_to_target)
    inst.Physics:SetFriction(.2)
	--print("DEBUG: We spawned!")
    -- local shadow = SpawnPrefab("warningshadow")
    -- shadow.Transform:SetPosition(pt:Get())
    -- shadow:shrink(time_to_target, 1.75, 0.5)
	inst.DoTrail = inst:DoPeriodicTask(0.1, function()
		local pos = inst:GetPosition()
		
		local trail = SpawnPrefab("peghook_tailp")
		trail.Transform:SetPosition(pos.x, pos.y, pos.z)	
	end)
	inst.TrackHeight = inst:DoPeriodicTask(FRAMES, function()
		local pos = inst:GetPosition()

		if pos.y <= 1 then
		    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, 4, nil, GetExcludeTagsp(inst))

		    for k,v in pairs(ents) do
	            if v.components.combat and v ~= inst and v.prefab ~= "peghookp" then
	                v.components.combat:GetAttacked(inst, 10)
					v.my_killer = thrower
					OnHit(v)
	            end
		    end

			local pt = inst:GetPosition()
			--if inst:GetIsOnWater() then
				--local splash = SpawnPrefab("kraken_ink_splatp")
				--splash.Transform:SetPosition(pos.x, pos.y, pos.z)

				
				inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_drop")
				local ink = SpawnPrefab("peghook_splashfxp")
				ink.Transform:SetPosition(pos.x, pos.y, pos.z)
			--end

			inst:Remove()
		end
	end)
end

local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
		inst.trailtask = nil
	end
end

local function projectile_onhitother(inst, target)
	--local fx = SpawnPrefab("scorpeon_splashfx")
	--fx.Transform:SetPosition(target:GetPosition():Get())
	
	target.components.combat:GetAttacked(inst.thrower and inst.thrower or inst, 10, inst.thrower, "acid")
	--Leo: Added acidimmune to make it easy for modded characters to be immune to acid if they so well please.
	if target.components.health and not target.acidimmune and not (target.sg and target.sg:HasStateTag("noattack")) then
		OnHit(target)
	end
end

local function projectile_onland(inst)
	local splash = SpawnPrefab("peghook_splashfxp")
	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_drop")
	splash.Transform:SetPosition(inst:GetPosition():Get())
	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, 4.5, nil, GetExcludeTagsp(inst)) or {}
	for k, v in pairs(ents) do
		if TheNet:GetServerGameMode() == "lavaarena" then
			if v.components.combat and v.components.health and not v.components.health:IsDead() and v.components.debuffable and v ~= inst.thrower then
				v.components.debuffable:AddDebuff("scorpeon_dot", "scorpeon_dot")
			end
		else
			if v.components.combat and v.components.health and not v.components.health:IsDead() and v ~= inst.thrower then
				projectile_onhitother(inst, v)
			end
		end	
	end
	inst:Remove()
end

local function CreateTail()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)
	
	inst.AnimState:SetBank("gooball_fx")
	inst.AnimState:SetBuild("gooball_fx")
	inst.AnimState:PlayAnimation("disappear")
	inst.AnimState:SetMultColour(.2, 1, 0, 1)
	inst.AnimState:SetFinalOffset(-1)
	
	inst.entity:SetPristine()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

	return inst
end

local function CreateTail2()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)
	
	inst.AnimState:SetBank("fireball_fx")
	inst.AnimState:SetBuild("fireball_2_fx")
	inst.AnimState:PlayAnimation("disappear")
	inst.AnimState:SetFinalOffset(-1)
	inst.AnimState:SetLightOverride(1)
	
	inst.entity:SetPristine()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

	return inst
end

local function OnUpdateProjectileTail(inst, tails)
    local x, y, z = inst.Transform:GetWorldPosition()
    for tail, _ in pairs(tails) do
        tail:ForceFacePoint(x, y, z)
    end
    if inst.entity:IsVisible() then
        local tail = CreateTail()
        local rot = inst.Transform:GetRotation()
        tail.Transform:SetRotation(rot)
        rot = rot * DEGREES
        local offsangle = math.random() * 2 * PI
        local offsradius = math.random() * .2 + .2
        local hoffset = math.cos(offsangle) * offsradius
        local voffset = math.sin(offsangle) * offsradius
        tail.Transform:SetPosition(x + math.sin(rot) * hoffset, y + voffset, z + math.cos(rot) * hoffset)
        --tail.Physics:SetMotorVel(speed * (.2 + math.random() * .3), 0, 0)]]
        tails[tail] = true
        inst:ListenForEvent("onremove", function(tail) tails[tail] = nil end, tail)
        --[[tail:ListenForEvent("onremove", function(inst)
            tail.Transform:SetRotation(tail.Transform:GetRotation() + math.random() * 30 - 15)
        end, inst)]]
    end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("gooball_fx")
	inst.AnimState:SetBuild("gooball_fx")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst.AnimState:SetMultColour(.2, 1, 0, 1)
	--inst:SetPrefabNameOverride("peghook")
	
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
        return inst
    end


	inst:AddTag("thrown")
	inst:AddTag("projectile")

	
	inst:AddComponent("complexprojectile")
	--TODO: Set Tuning variables.
    inst.components.complexprojectile:SetHorizontalSpeed(15)
    inst.components.complexprojectile:SetGravity(-50)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0.2, 2.5, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)
	
	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
	
	inst.DoTrail = inst:DoPeriodicTask(0.1, function()
		local pos = inst:GetPosition()
		
		local trail = SpawnPrefab("peghook_tailp")
		trail.Transform:SetPosition(pos.x, pos.y, pos.z)	
	end)

	
	inst:DoTaskInTime(20, function (inst) inst:Remove() end)
	
	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local lerp_time = 30

local function OnNotifyNearbyPlayers(inst, self, rangesq)
    local x, y, z = inst.Transform:GetWorldPosition()
    for i, v in ipairs(AllPlayers) do
        if not v:HasTag("playerghost") and v:GetDistanceSqToPoint(x, y, z) < rangesq then
            v:PushEvent("unevengrounddetected", { inst = inst, radius = self.radius, period = self.detectperiod })
        end
    end
end

local function OnRemove(inst)
	inst:Remove()
end

local function splashfx_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBank("lavaarena_hits_splash")
    inst.AnimState:SetBuild("lavaarena_hits_splash")
    inst.AnimState:PlayAnimation("aoe_hit")
    inst.AnimState:SetMultColour(.2, 1, 0, 1)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetScale(.54, .54)

	
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
        return inst
    end
	
	--inst:AddComponent("sizetweener")
	
	local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 5, { "hostile", "monster", "player", "character", "animal" }, { "ghost", "shadow", "shadowminion", "noauradamage", "structure", "wall", "INLIMBO" })
    for i, v in ipairs(ents) do
        if v.components.health and not v.components.health:IsDead() then
            OnHit(v)
        end
	end	
	inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)

	return inst
end

return Prefab("peghook_projectilep", fn, assets, prefabs),
Prefab("peghook_tailp", CreateTail),
Prefab("fire_tailp", CreateTail2),
Prefab("peghook_splashfxp", splashfx_fn)