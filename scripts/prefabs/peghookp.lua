local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
--Notes--
--Fix the scorpeon dot, its literally as old as forged forge...
---------------------------
local prefabname = "peghookp"

local assets = 
{
	Asset("ANIM", "anim/peghook_shiny_build_01.zip"),
	Asset("ANIM", "anim/peghook_shiny_build_03.zip"),
	Asset("ANIM", "anim/peghook_shiny_build_06.zip"),
}


local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE,
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	scale        = MOB_TUNING.SCALE,

	bank = "peghook",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_peghook_alt1" or "lavaarena_peghook_basic",
	
	--build2 = "alternate build here",
	stategraph = "SGpeghookp",
	minimap = "peghookp.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('peghookp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
	{'monstermeat',			1.00}, 
})

local RND_OFFSET = 1

local function DoSpit(inst)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 4.25
	local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
	local spit = SpawnPrefab("peghook_projectilep")
	spit.Transform:SetPosition(inst:GetPosition():Get())
	spit.thrower = inst
	spit.components.complexprojectile:Launch(targetpos, inst)
	inst.components.playablepet:CooldownAbility(PP_ABILITY_SLOTS.SKILL2, MOB_TUNING.SPIT_CD)
end

local function OnHealthDelta(inst)
	local health = inst.components.health:GetPercent()
	if health <= MOB_TUNING.BESERK_TRIGGER and not inst._isbeserk then
		inst._isbeserk = true
		inst.components.combat:SetAttackPeriod(mob.attackperiod * MOB_TUNING.BESERK_MULT)
	elseif health > MOB_TUNING.BESERK_TRIGGER then
		inst._isbeserk = false
		inst.components.combat:SetAttackPeriod(mob.attackperiod)
	end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	----------------------------------
	--Tags--
	
	inst:AddTag("monster")
	inst:AddTag("LA_mob")
	----------------------------------

    inst:ListenForEvent("phasechange", PlayablePets.SetNightVision)
	PlayablePets.SetNightVision(inst)
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.SCORPEON)
	
	inst.mobsleep = false
	
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:RemoveTag("LA_mob")
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end


local master_postinit = function(inst) 
    ------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.75, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Variables--
	inst._isbeserk = false

	inst.DoSpit = DoSpit
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefabname)
	----------------------------------
    --Playable Pets
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SLEEP, PP_ABILITIES.PP_SLEEP)
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL1, PP_ABILITIES.SKILL1)
	inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL2, PP_ABILITIES.SKILL2)	
	----------------------------------	
	--Debuff Symbols
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 150, .8)
    inst.DynamicShadow:SetSize(3, 1.5)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("healthdelta", OnHealthDelta)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("peghookp", prefabs, assets, common_postinit, master_postinit, start_inv)
