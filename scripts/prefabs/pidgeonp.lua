local MakePlayerCharacter = require "prefabs/player_common"

---------------------------

--This was written on an outdated source of mandrake. Check with updated prefab before running!
---------------------------


local assets = 
{
	Asset("ANIM", "anim/quagmire_pigeon_build.zip"),
}




local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}
-----------------------
--Stats--
local mob = 
{
	health = 50,
	hunger = 100,
	hungerrate = 0.075, --I think .15 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	attackperiod = 0,
	damage = 10,
	range = 3.5,
	bank = "crow",
	build = "quagmire_pigeon_build",
	--build2 = "parrot_pirate_build",
	scale = 1,	
	stategraph = "SGbird_event",
	minimap = "pidgeonp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('pidgeonp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00}, 
})

local sounds = 
	{
		takeoff = "dontstarve/birds/takeoff_quagmire_pigeon",
        chirp = "dontstarve/birds/chirp_quagmire_pigeon",
        flyin = "dontstarve/birds/flyin",
	}

--------------------------------------------------
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
------------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      ThePlayer:EnableMovementPrediction(false)
   end
end)

end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.PIDGEON)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('pidgeonp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("birdwhisperer")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.mobplayer = true
	inst.taunt2 = true
	inst._isflying = false
	
	
	
	inst.components.locomotor:SetAllowPlatformHopping(false)
	
	local body_symbol = "crow_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.sounds = sounds
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,5,5) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)

    inst.DynamicShadow:SetSize(1, .75)
    inst.DynamicShadow:Enable(true)
    inst.Transform:SetTwoFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("pidgeonp", prefabs, assets, common_postinit, master_postinit, start_inv)
