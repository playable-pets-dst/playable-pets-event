local MakePlayerCharacter = require "prefabs/player_common"
---------------------------
local prefabname = "pigelite_bluep"

local assets = 
{
	Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("ANIM", "anim/ds_pig_elite.zip"),
    Asset("ANIM", "anim/ds_pig_elite_intro.zip"),
    Asset("ANIM", "anim/pig_elite_build.zip"),
    Asset("ANIM", "anim/pig_guard_build.zip"),
    Asset("ANIM", "anim/slide_puff.zip"),
    Asset("SOUND", "sound/pig.fsb"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE, --range is 1.8 in stategraph.
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	scale		 = MOB_TUNING.SCALE,

	bank = "pigman",
	shiny = "pigelite_blue",
	build = "pig_guard_build",

	--build2 = "alternate build here",
	stategraph = "SGpigelitep",
	minimap = "pigelite_bluep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('pigelitep',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'pigskin',			1.00},
})

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker and not data.attacker:HasTag("minigame_participator") then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 10)
    end
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end

local function onhitother2(inst, other)
	if not other:HasTag("minigame_participator") then
		inst.components.combat:SetTarget(other)
		inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 10)
	end
end
 
----------------------------------------- 
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function GetAlivePlayers(inst)
	local alive_allies = {}
	local ents = TheSim:FindEntities(pos.x,0,pos.z, range and range or 255, {"player"}, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" })
	for i, v in ipairs(ents) do
		if v.components.health and not v.components.health:IsDead() then
			table.insert(alive_allies, v)
		end	
	end
	return #alive_allies or 0
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.PIGELITE_BLUE)
	
	inst.mobsleep = false
	
	inst.healmult = 0.75
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	
	--inst:AddComponent("buffable")
	--inst.components.buffable:AddBuff("heal_dealt", {{name = "heal_dealt", val = 50, type = "mult"}})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

--in order: blue, red, white, green
local BUILD_VARIATIONS =
{
    ["1"] = { "pig_ear", "pig_head", "pig_skirt", "pig_torso", "spin_bod" },
    ["2"] = { "pig_arm", "pig_ear", "pig_head", "pig_skirt", "pig_torso", "spin_bod" },
    ["3"] = { "pig_arm", "pig_ear", "pig_head", "pig_skirt", "pig_torso", "spin_bod" },
    ["4"] = { "pig_head", "pig_skirt", "pig_torso", "spin_bod" },
}

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('pigelitep')
	----------------------------------
	--Tags--
	
	inst:AddTag("pig")
    inst:AddTag("pigmanplayer")
	
	--inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)	
	
	inst.var = "1"
	inst.shouldwalk = false
	
	for i, v in ipairs(BUILD_VARIATIONS[inst.var]) do
        inst.AnimState:OverrideSymbol(v, "pig_elite_build", v.."_"..inst.var)
    end

	PlayablePets.SetSkeleton(inst, "pp_skeleton_pig")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquipPig) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", PlayablePets.CommonOnUnequip)
	inst:ListenForEvent("attacked", onattacked)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("pigelite_bluep", prefabs, assets, common_postinit, master_postinit, start_inv)
