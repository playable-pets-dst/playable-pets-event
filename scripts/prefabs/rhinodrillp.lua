local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/lavaarena_rhinodrill_basic.zip"),
    Asset("ANIM", "anim/lavaarena_rhinodrill_damaged.zip"),
	Asset("ANIM", "anim/lavaarena_rhinodrill_clothed_b_build.zip"),
	
	Asset("ANIM", "anim/rhinodrill_shiny_build_08.zip"),
	Asset("ANIM", "anim/rhinodrill_damaged_shiny_build_08.zip"),
	Asset("ANIM", "anim/rhinodrill_clothed_shiny_build_08.zip"),
}

local prefabs = 
{	
	"rhinobuff",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and 12750 or 3750,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = 7,
	walkspeed = 7,
	attackperiod = 3,
	damage = BOSS_STATS and 150*2 or 80*2,
	range = 5,
	bank = "rhinodrill",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_alt1" or "lavaarena_rhinodrill_basic",
	scale = 1.15,
	shiny = "rhinodrill",
	--build2 = "alternate build here",
	stategraph = "SGrhinodrillp",
	minimap = "rhinodrillp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('rhinodrillp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00}, 
	{'meat',			1.00}, 
	{'meat',			1.00}, 
	{'meat',			1.00}, 
})

-------------------------------------------------------
--Knockback--

local function OnHitOther(inst, other) --knockback
	if inst.sg:HasStateTag("charging") then
		PlayablePets.KnockbackOther(inst, other, 1)
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.var = data.var or 1
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.var = inst.var or 1
end

local function setVariation(inst)
	if inst.var == nil then
		local var = math.random(1,2)
		inst.var = var		
	end	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function oncollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        Vector3(inst.Physics:GetVelocity()):LengthSq() >= 1 and other:HasTag("tree") and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local function SetLoadBuild(inst) --sets character when the player loads.
	if not inst:HasTag("playerghost") then
		setVariation(inst)
		inst.AnimState:SetBank(mob.bank)
		inst.AnimState:SetBuild(mob.build)
		if inst.var and inst.var == 2 then
			inst.AnimState:AddOverrideBuild(TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_clothed_alt1" or "lavaarena_rhinodrill_clothed_b_build")
		end
		if inst.components.health and inst.components.health:GetPercent() <= 0.3 then
			inst.AnimState:AddOverrideBuild(TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_damaged_alt1" or "lavaarena_rhinodrill_damaged")
		end
	end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)	
		if data.alt then
			inst.var = 2
			inst.AnimState:AddOverrideBuild(data.build2)
		else
			inst.var = 1
			inst.AnimState:ClearAllOverrideSymbols()
		end
		if inst.components.health and inst.components.health:GetPercent() <= 0.3 then
			inst.AnimState:AddOverrideBuild(data.damaged_build)
		end
	else
		inst.var = 1
		inst.AnimState:ClearAllOverrideSymbols()
		inst.AnimState:SetBuild(mob.build)	
		if inst.components.health and inst.components.health:GetPercent() <= 0.3 then
			inst.AnimState:AddOverrideBuild(TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_rhinodrill_damaged_alt1" or "lavaarena_rhinodrill_damaged")
		end
	end	
	inst.AnimState:OverrideSymbol("fx_wipe", "wilson_fx", "fx_wipe")
	inst.AnimState:AddOverrideBuild("fossilized")
end

local function CanRevive(inst, reviver)
	return reviver:HasTag("rhinobro") and TheNet:GetServerGameMode() ~= "lavaarena"
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
	inst:AddComponent("revivablecorpse")
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.components.revivablecorpse:SetCanBeRevivedByFn(CanRevive)
	end
	
end

-------------Forge------------------------------------------

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.RHINOCEBRO)
	
	inst.healmult = 1.5
	inst.mobsleep = false
	inst.altattack = false --no charging
	inst:RemoveTag("LA_mob") --you're a traitor now
	--inst.acidmult = 1.25
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst:AddComponent("corpsereviver")
	if inst.components.corpsereviver then
		inst.components.corpsereviver.reviver_speed_mult = 2
		inst.components.corpsereviver.additional_revive_health_percent = 0.2
	end
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.75, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetRange(10, 0)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('rhinodrillp')
	inst.AnimState:AddOverrideBuild("fossilized")
	----------------------------------
	--Tags--
	
	inst:AddTag("monster")
	inst:AddTag("LA_mob")
    inst:AddTag("rhinobro")
	inst:AddTag("epic")
	
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	inst.knocker = true
	inst.var = 1

	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.cancheer = true
	inst.cheer_loop = 0
	inst.rams = 0
	inst.brostacks = 0
	inst.AnimState:OverrideSymbol("fx_wipe", "wilson_fx", "fx_wipe")
	----------------------------------
	--SanityAura--
	--inst:AddComponent("sanityaura")
	
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	inst:SetPhysicsRadiusOverride(1)
    MakeCharacterPhysics(inst, 400, inst.physicsradiusoverride)
    inst.DynamicShadow:SetSize(2.75, 1.25)
    inst.Transform:SetSixFaced()
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	
	inst:ListenForEvent("healthdelta", function(inst)
		inst.components.ppskin_manager:LoadSkin(mob, true)
	end)
	
	inst:AddComponent("revivablecorpse")
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

local function OnTick(inst, target)
	inst.AnimState:PlayAnimation("attack_fx3")
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
	--inst.tickcount = 0
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
	
	local fx = SpawnPrefab("rhinobuff")
	fx.Transform:SetPosition(inst:GetPosition():Get())
	--fx.entity:SetParent(target.entity)
	fx.AnimState:PlayAnimation("in", false)
	fx.AnimState:PushAnimation("idle", false)
	fx.AnimState:PushAnimation("out", false)
	--print("check1")
	--fx:DoTaskInTime(3, function(inst) inst:Remove() end)
	fx:ListenForEvent("animqueueover", fx:Remove())
	--print("check2")
	
	if target.components.combat and target.brostacks then
		target.components.combat.damagemultiplier = 1
	end
	
    inst:ListenForEvent("death", function()
        --inst.components.debuff:Stop()
    end, target)
end

local function OnDetached(inst, target)
	if target.components.combat and target.components.combat.damagemultiplier and target.components.combat.damagemultiplier >= 1 then
		target.brostacks = 0
		target.components.combat.damagemultiplier = 1
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == "rhinobuffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer("rhinobuffover")
    inst.components.timer:StartTimer("rhinobuffover", inst.duration)
    inst.task:Cancel()
	
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
end

local function DoT_OnInit(inst)
   -- local parent = inst.entity:GetParent()
    --if parent ~= nil then
        --parent:PushEvent("startcorrosivedebuff", inst)
    --end
end

local function dot_fn(inst)
	local inst = CreateEntity()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")

    --inst:DoTaskInTime(0, DoT_OnInit)
	
	

    inst.AnimState:SetBank("lavaarena_battlestandard")
    inst.AnimState:SetBuild("lavaarena_battlestandard")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	--print("buffran")
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
    --inst.components.debuff.keepondespawn = true
	
	inst.duration = TheNet:GetServerGameMode() == "lavaarena" and 30 or 60
	
	inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.

    inst:AddComponent("timer")
	inst:DoTaskInTime(0, function() -- in case we want to change duration
		inst.components.timer:StartTimer("rhinobuffover", inst.duration)
	end)
    inst:ListenForEvent("timerdone", OnTimerDone)
	
	return inst
	
end


return MakePlayerCharacter("rhinodrillp", prefabs, assets, common_postinit, master_postinit, start_inv),
Prefab("rhinobro_buffp", dot_fn)
