local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/snapper_shiny_build_01.zip"),
	Asset("ANIM", "anim/snapper_shiny_build_03.zip"),
	Asset("ANIM", "anim/snapper_shiny_build_05.zip"),
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{

}

-----------------------
local mob = 
{
	health = 550,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 175,
	runspeed = 7.1,
	walkspeed = 6,
	attackperiod = 3,
	damage = 40*2,
	hit_range = 99,
	range = 16, 
	bank = "snapper",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_snapper_alt1" or "lavaarena_snapper_basic",
	shiny = "snapper",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGsnapperp",
	minimap = "snapperp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('snapperp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
})
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
---------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.CROCOMMANDER)
	
	inst.mobsleep = false
	
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:RemoveTag("LA_mob")
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:AddComponent("corpsereviver")
	inst.components.corpsereviver:SetReviverSpeedMult(TUNING.FORGE.WILSON.BONUS_REVIVE_SPEED_MULTIPLIER)
	inst.components.corpsereviver:SetAdditionalReviveHealthPercent(TUNING.FORGE.WILSON.BONUS_HEALTH_ON_REVIVE_PERCENT)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local spit = SpawnPrefab(TheNet:GetServerGameMode() == "lavaarena" and "forge_gooball_projectile" or "goo_projectilep")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

local master_postinit = function(inst) 
	-------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, nil, 0, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('snapperp')
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	--inst:AddTag("special_atk1") --allows taunting.
	
	inst:AddTag("monster")
	inst:AddTag("crocovile") --can craft battle standards.
    inst:AddTag("LA_mob")
	
	inst.taunt = true
	inst.taunt2 = true
	inst._ismonster = true
	inst.mobsleep = true

	inst.poisonimmune = true
	
	inst.hit_recovery = 0.75
	
	inst.AnimState:AddOverrideBuild("fossilized")
	
	inst.FireProjectile = FireProjectile
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("body", 0, 0, 0)
	MakeMediumBurnableCharacter(inst, body_symbol)
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.25,1.25) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 100, .75)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end




return MakePlayerCharacter("snapperp", prefabs, assets, common_postinit, master_postinit, start_inv)
