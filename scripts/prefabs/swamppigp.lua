local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("ANIM", "anim/quagmire_swampig_build.zip"),
    Asset("ANIM", "anim/quagmire_swampig_extras.zip"),
    Asset("SOUND", "sound/pig.fsb"),
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 200,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 125,
	runspeed = 5,
	walkspeed = 3,
	attackperiod = 0,
	damage = 60,
	range = TUNING.WORM_ATTACK_DIST,
	bank = "pigman",
	shiny = "swamppig",
	build = "quagmire_swampig_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGswamppigp",
	minimap = "swamppigp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('swamppigp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
})

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/swamp_pig/talk")
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 10)
    end
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
end

local function CheckforHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
		inst.AnimState:Show("hat")
	else
		inst.AnimState:Hide("hat")
	end 
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 

local function SetVariation(inst)
	if inst.var == nil and inst.var2 == nil then
		local var = math.random(0,10)
		local var2 = math.random(0,10)
		if var == 0 then
			inst.var = ""
		else
			inst.var = var
		end		
		if var2 == 0 then
			inst.var2 = ""
		else
			inst.var2 = var2
		end		
	end	
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.var = data.var or 0
		inst.var2 = data.var or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.var = inst.var or 0
	data.var2 = inst.var2 or 0
end

local function SetLoadBuild(inst) --sets character when the player loads.
	if not inst:HasTag("playerghost") then
		SetVariation(inst)
	end
	
	if inst.var ~= nil and inst.var2 ~= nil then 
            inst.AnimState:OverrideSymbol("pig_head", "quagmire_swampig_build", "pig_head"..inst.var)
            inst.AnimState:OverrideSymbol("pig_torso", "quagmire_swampig_build", "pig_torso"..inst.var2)
    end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		--print("DEBUG:SetSkinDefault shinizer ran")
		inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)	
			if inst.var ~= nil and inst.var2 ~= nil then 
				print("Overriding Symbols in SetSkinDefault: "..inst.var.. " and "..inst.var2)
				inst.AnimState:OverrideSymbol("pig_head", mob.shiny.."_shiny_build_0"..num, "pig_head"..inst.var)
				inst.AnimState:OverrideSymbol("pig_torso", mob.shiny.."_shiny_build_0"..num, "pig_torso"..inst.var2)
			end
	else
		--print("DEBUG:SetSkinDefault normalizer ran")
		SetLoadBuild(inst)		
	end	
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.SWAMPPIG)
	
	inst.mobsleep = false
	
	inst.components.combat:AddDamageBuff("atk_banner", 1.25, false)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({})
	end)
	
	inst:AddComponent("buffable")
	inst.components.buffable:AddBuff("werepig_debuff", {{name = "cooldown", val = 1.2, type = "mult"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('swamppigp')
	----------------------------------
	--Tags--
	
	inst:AddTag("pig")
    inst:AddTag("QM_mob")
	
	--inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true

	
	
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	CheckforHat(inst)
	inst:DoTaskInTime(0, SetLoadBuild)
	if inst.var ~= nil and inst.var2 ~= nil then 	
		--print("Overriding Symbols in master_postinit: "..inst.var.. " and "..inst.var2)
        inst.AnimState:OverrideSymbol("pig_head", "quagmire_swampig_build", "pig_head"..inst.var)
        inst.AnimState:OverrideSymbol("pig_torso", "quagmire_swampig_build", "pig_torso"..inst.var2)
    end

	PlayablePets.SetSkeleton(inst, "pp_skeleton_pig")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", PlayablePets.CommonOnUnequip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end




return MakePlayerCharacter("swamppigp", prefabs, assets, common_postinit, master_postinit, start_inv)
