local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/tortank_shiny_build_01.zip"),
	Asset("ANIM", "anim/tortank_shiny_build_03.zip"),
}




local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 900,
	hunger = 200,
	hungerrate = 0.07,
	sanity = 150,
	runspeed = 3,
	walkspeed = 3,
	attackperiod = 3,
	damage = 50*2,
	hitrange = 3,
	range = 2,
	bank = "turtillus",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_turtillus_alt1" or "lavaarena_turtillus_basic",
	scale = 0.8,
	shiny = "tortank",
	stategraph = "SGtortankp",
	minimap = "tortankp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('tortankp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00},
	{'meat',			1.00},
})

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function SetShellPhysics(inst)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
end

local function SetNormalPhysics(inst)
    ChangeToCharacterPhysics(inst)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.TORTANK)
	
	inst.mobsleep = false
	
	inst.components.health:SetAbsorptionAmount(0.95)
	
	inst:RemoveTag("LA_mob")
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.75, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('tortankp')
	----------------------------------
	--Tags--
	
	inst.SetShellPhysics = SetShellPhysics
    inst.SetNormalPhysics = SetNormalPhysics
	
	inst:AddTag("monster")
	inst:AddTag("LA_mob")
	
	inst.altattack = true
	inst.taunt = true
	inst.canhide = true
	inst.mobsleep = true

	
	inst.ram_attempt = 0
	
	
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	inst.AnimState:AddOverrideBuild("fossilized")
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 150, .8)
    inst.DynamicShadow:SetSize(2.5, 1.75)
    inst.Transform:SetSixFaced()
	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end



return MakePlayerCharacter("tortankp", prefabs, assets, common_postinit, master_postinit, start_inv)
