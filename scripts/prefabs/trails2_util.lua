require "prefabutil"

--Oops, this inst trails related at all!

local assets =
{
    Asset("ANIM", "anim/hf_teaser_boarrior.zip"),
}

local prefabs =
{
    
}
local function CreateTeaser(name, bank, build, anim, loop)
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()

		inst:AddTag("structure") --to allow dusting
		inst:AddTag("hf_teaser")

		inst.AnimState:SetBank(bank)
		inst.AnimState:SetBuild(build)
		inst.AnimState:PlayAnimation(anim, loop or false)
		
		inst.nameoverride = "unknownp"

		inst.entity:SetPristine()
	
		if not TheWorld.ismastersim then
			return inst
		end
	
	
		inst:AddComponent("inspectable")	
		
		return inst
	end
    return Prefab(name, fn, assets, prefabs)
end

return CreateTeaser("axep", "hf_teaser_boarrior", "hf_teaser_boarrior", "idle")
