local assets=
{
	Asset("ANIM", "anim/fire.zip"),
    Asset("SOUND", "sound/common.fsb"),
}

local prefabs=
{
	
}

local function OnLoad(inst, data)
	inst:Remove()
end

local function OnIgnite(inst)
end

local function OnExtinguish(inst)
	inst:Remove()
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "dragoon", "structure"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "dragoon", "structure"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "dragoon", "structure"}
	end
end

local function DoAreaDamage(inst)
	local pos = inst:GetPosition()
	local targets = {}
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, 2.5, { "locomotor"}, GetExcludeTags(inst)) 
	for _,ent in ipairs(ents) do
		if ent ~= inst and ent.components.health and not ent.components.health:IsDead() and ent ~= inst and ent.components.health.fire_damage_scale > 0 and (inst.owner and ent ~= inst.owner) and not ent.isfiredamaged then
			table.insert(targets, ent)
		end
	end
	if #targets > 0 then
		for i, v in ipairs(targets) do
			if v.components.combat and v.components.health.fire_damage_scale and inst.owner then
				v.components.combat:GetAttacked(inst.owner and inst.owner or inst, 20 * v.components.health.fire_damage_scale)
				v.isfiredamaged = true
				v:DoTaskInTime(1, function(inst) inst.isfiredamaged = nil end)
			end
		end
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddSoundEmitter()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetBank("fire")
    inst.AnimState:SetBuild("fire")
	inst.AnimState:PlayAnimation("level4", true)
    inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetFinalOffset(-1)
	
	inst.SoundEmitter:PlaySound("dontstarve/common/fireBurstLarge")
	inst.SoundEmitter:PlaySound("dontstarve/common/forestfire", "fire")
	inst.SoundEmitter:SetParameter("fire", "intensity", 1)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddTag("fire")
	inst:AddTag("NOCLICK")
	
	
	if MOBFIRE == "Disable" then
		MakeLargePropagator(inst)
	end
	
	local light = inst.entity:AddLight()
	light:SetFalloff(.2)
    light:SetIntensity(0.9)
    light:SetRadius(6)
    light:Enable(true)
    light:SetColour(255/255,190/255,121/255)
	
	inst:DoPeriodicTask(0.5, DoAreaDamage)

	inst:DoTaskInTime(12, function(inst)
		--inst.SoundEmitter:KillSound("fire")
		inst:Remove()
	end)
	
	inst.OnLoad = OnLoad

	return inst
end

return Prefab( "common/trails2fire", fn, assets, prefabs)
