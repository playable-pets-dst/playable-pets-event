local MakePlayerCharacter = require "prefabs/player_common"

---------------------------

--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/dragoon_basic.zip"),
	Asset("ANIM", "anim/dragoon_actions.zip"),
	Asset("ANIM", "anim/dragoon_lavaarena.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

-----------------------
--Stats--
local mob = 
{
	health = 3800,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 9,
	walkspeed = 2.5,
	
	attackperiod = 3,
	damage = 75*2,
	range = 3,
	hit_range = 5,
	
	bank = "dragoon",
	build = "lavaarena_dragoon_build",
	shiny = "pog",
	
	scale = 3.5,
	stategraph = "SGtrails2p",
	minimap = "trails2p.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'trails2p',
{
    {'monstermeat',  1},
	{'monstermeat',  1},
	{'monstermeat',  1},
	{'monstermeat',  1},
	{'monstermeat',  1},
	{'monstermeat',  1},
})

local FORGE_STATS = PPEV_FORGE.TRAILS2
--==============================================
--					Mob Functions
--==============================================
local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] and other:HasTag("tree") then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
	elseif not inst.recentlycharged[other] and other.components.health ~= nil and not other.components.health:IsDead() and not (other:HasTag("tree") or other:HasTag("wall")) and not ((TheNet:GetServerGameMode() == "lavaarena" or not TheNet:GetPVPEnabled()) and (other:HasTag("player") or other:HasTag("companion"))) and inst.sg:HasStateTag("charging") then
		inst.recentlycharged[other] = true
		inst:DoTaskInTime(1, ClearRecentlyCharged, other)
		other.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(other))

		if other:HasTag("flippable") then
			other:PushEvent("flipped")
		end	
		if not (other:HasTag("epic") or other:HasTag("largecreature")) then
			other:PushEvent("knockback", {radius = 10})
		end
    end
end

local function oncollide(inst, other)
    if other ~= nil and other:IsValid() and not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end
--==============================================
--				Custom Common Functions
--==============================================


--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst) 
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 1, 1) --fire, acid, poison
	----------------------------------
	--Tags--
	inst:AddTag("epic")
	inst:AddTag("monster")
	inst:AddTag("dragoon")
	inst:AddTag("LA_mob")
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	inst.canrun = true
	
	local body_symbol = "catcoon_torso"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "trails2p") --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 500, 1.25)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(4, 2)
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.

	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("trails2p", prefabs, assets, common_postinit, master_postinit, start_inv)