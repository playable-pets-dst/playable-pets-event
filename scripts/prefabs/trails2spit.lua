local assets=
{
	Asset("ANIM", "anim/lava_vomit.zip"),
}

local function GetStatus(inst, viewer)
    if inst.cooled then return "COOL" end
    return "HOT"
end

local INTENSITY = .7

local function fade_in(inst)
    inst.components.fader:StopAll()
    inst.Light:Enable(true)
    inst.components.fader:Fade(0, INTENSITY, 5*FRAMES, function(v) inst.Light:SetIntensity(v) end)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/fx/fire_circle_LP", "fire")
	inst.SoundEmitter:SetParameter("fire", "intensity", 1)
end

local function fade_out(inst)
	inst.SoundEmitter:KillSound("loop")
    inst.components.fader:StopAll()
    inst.components.fader:Fade(INTENSITY, 0, 5*FRAMES, function(v) inst.Light:SetIntensity(v) end, function() inst.Light:Enable(false) end)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "dragoon", "structure", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "dragoon"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "dragoon"}
	end
end

local function DoAreaDamage(inst)
	if inst.cooled ~= true then
		local pos = inst:GetPosition()
		local targets = {}
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, 3.5, { "locomotor"}, GetExcludeTags(inst)) 
		for _,ent in ipairs(ents) do
			if ent ~= inst and ent.components.health and not ent.components.health:IsDead() and ent ~= inst and ent.components.health.fire_damage_scale > 0 and (inst.owner and ent ~= inst.owner) and not ent.isfiredamaged and inst.cooled ~= true then
				table.insert(targets, ent)
			end
		end
		if #targets > 0 then
			for i, v in ipairs(targets) do
				if v.components.combat and v.components.health.fire_damage_scale and inst.owner then
					v.components.combat:GetAttacked(inst.owner and inst.owner or inst, 20 * v.components.health.fire_damage_scale)
					v.isfiredamaged = true
					v:DoTaskInTime(1, function(inst) inst.isfiredamaged = nil end)
				end
			end
		end
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("lava_vomit")
    inst.AnimState:SetBuild("lava_vomit")
	inst.AnimState:PlayAnimation("dump")
    inst.AnimState:PushAnimation("idle_loop", true)
    inst.Transform:SetFourFaced()  
	
	
	inst:AddTag("NOCLICK")
	inst:AddTag("notarget")
	
	inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst.Transform:SetScale(3, 3, 3)
	
    inst:AddComponent("fader")
    local light = inst.entity:AddLight()
    light:SetFalloff(.5)
    light:SetIntensity(INTENSITY)
    light:SetRadius(3)
    light:Enable(false)
    light:SetColour(200/255, 100/255, 170/255)
    fade_in(inst)
	
	if MOBFIRE == "Disable" then
		inst:AddComponent("propagator")
		inst.components.propagator.heatoutput = 70
		inst.components.propagator.decayrate = 0
		inst.components.propagator:Flash()
		inst.components.propagator:StartSpreading()
	end

	inst.cooled = false
	inst:DoPeriodicTask(0.5, DoAreaDamage)
	
    inst.cooltask = inst:DoTaskInTime(20, function(inst) 
		inst.cooled = true
    	inst.AnimState:PushAnimation("cool", false)
    	fade_out(inst)
        inst:DoTaskInTime(4*FRAMES, function(inst)
            inst.AnimState:ClearBloomEffectHandle()
        end)
    end)
    inst:ListenForEvent("animqueueover", function(inst)
   		inst.AnimState:SetPercent("cool", 1)
        if inst.components.propagator then 
            inst.components.propagator:StopSpreading()
            inst:RemoveComponent("propagator") 
        end
        inst:AddComponent("colourtweener")
        inst.components.colourtweener:StartTween({0,0,0,0}, 7, inst.Remove)
    end)

    return inst
end

return Prefab( "trails2spit", fn, assets)