local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/trails_shiny_build_01.zip"),
	Asset("ANIM", "anim/boarbotnik_build.zip"),
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and 7700 or 3000,
	hunger = 325,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 9,
	walkspeed = 6,
	damage = 150*2,
	attackperiod = 3,
	hit_range = 5,
	range = 3,
	bank = "trails",
	build = TheNet:GetServerGameMode() == "lavaarena" and "lavaarena_trails_alt1" or "lavaarena_trails_basic",
	scale = 1.2,
	shiny = "trails",
	--build2 = "alternate build here",
	stategraph = "SGtrailsp",
	minimap = "trailsp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('trailsp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00}, 
	{'meat',			1.00}, 
	{'meat',			1.00}, 
	{'meat',			1.00}, 
})

-------------------------------------------------------
--Knockback--

local function OnHitOther(inst, other) --knockback
	if not inst.is_doing_special then
		PlayablePets.KnockbackOther(inst, other, inst.sg:HasStateTag("rolling") and 1 or 5)
	end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.var = data.var or ""
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.var = inst.var or ""
end

local function setVariation(inst)
	if inst.var == nil then
		local var = math.random(0,2)
		if var == 0 then
			inst.var = ""
		else
			inst.var = var
		end		
	end	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function oncollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.NET and
        Vector3(inst.Physics:GetVelocity()):LengthSq() >= 1 and other:HasTag("tree") and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local function SetLoadBuild(inst) --sets character when the player loads.
    inst.AnimState:SetBuild(mob.build)
	if inst.var ~= nil then
        inst.AnimState:OverrideSymbol("armour", "lavaarena_trails_basic", "armour"..inst.var)
        inst.AnimState:OverrideSymbol("mouth", "lavaarena_trails_basic", "mouth"..inst.var)
    end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)	
		if inst.var ~= nil then
			inst.AnimState:OverrideSymbol("armour", data.build, "armour"..inst.var)
			inst.AnimState:OverrideSymbol("mouth", data.build, "mouth"..inst.var)
		end
	else
		inst.AnimState:SetBuild(mob.build)	
		if inst.var ~= nil then
			inst.AnimState:OverrideSymbol("armour", mob.build, "armour"..inst.var)
			inst.AnimState:OverrideSymbol("mouth", mob.build, "mouth"..inst.var)
		end
	end	
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPEV_FORGE.BOARILLA)
	
	inst.mobsleep = false
	inst:RemoveTag("LA_mob")
	inst.components.health:SetAbsorptionAmount(0.9)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end


local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('trailsp')
	----------------------------------
	--Temperature and Weather Components--	
	inst.AnimState:AddOverrideBuild("fossilized")
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	--inst:AddTag("special_atk1") --allows taunting
	
	inst:AddTag("monster")
	inst:AddTag("LA_mob")
    inst:AddTag("monkey")
	inst:AddTag("epic")
	
	inst.altattack = true
	inst.altattack2 = true
	inst.taunt = true
	inst.canhide = true
	--inst.taunt2 = true
	inst.rams = 0
	inst.mobsleep = true

	
	inst.knocker = true
	
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("helmet", 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	if inst.var ~= nil then
        inst.AnimState:OverrideSymbol("armour", "lavaarena_trails_basic", "armour"..inst.var)
        inst.AnimState:OverrideSymbol("mouth", "lavaarena_trails_basic", "mouth"..inst.var)
    end
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 500, 1.25)
    inst.DynamicShadow:SetSize(3.25, 1.75)
    inst.Transform:SetFourFaced()
	
	inst.jump_range = math.min(TUNING.KLAUS_CHOMP_MAX_RANGE, TUNING.KLAUS_CHOMP_RANGE)
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > 0.9 then
			inst.components.combat:SetRange(3,5)
		end	
	end)
	--inst:ListenForEvent("onhitother", OnHitOther)
	inst.components.combat.onhitotherfn = OnHitOther
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("trailsp", prefabs, assets, common_postinit, master_postinit, start_inv)
