
local projectile_assets =
{
    Asset("ANIM", "anim/warg_gingerbread_bomb.zip"),
    Asset("ANIM", "anim/goo_icing.zip"),
}

local projectile_prefabs =
{
    "icing_splat_fx",
    "icing_splash_fx_full",
    "icing_splash_fx_med",
    "icing_splash_fx_low",
    "icing_splash_fx_melted",
}

local splashfxlist =
{
    "icing_splash_fx_full",
    "icing_splash_fx_med",
    "icing_splash_fx_low",
    "icing_splash_fx_melted",
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "shadow", "notarget", "gingerbread"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "gingerbread"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "gingerbread"}
	end
end

local function DoSplatFx(inst)
	inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/splat")
	local x, y, z = inst.Transform:GetWorldPosition()
	SpawnPrefab("icing_splat_fx").Transform:SetPosition(x, 0, z)
end

local function doprojectilehit(inst, other)
    local caster = (inst._caster ~= nil and inst._caster:IsValid()) and inst._caster or nil

	local targets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, TUNING.WARG_GOO_RADIUS + 2, { "_combat", "_health" }, GetExcludeTags(inst))
	if #ents > 0 then
		for i, other in ipairs(ents) do
			if other ~= nil and other ~= caster and other.components.combat ~= nil  then
				if other.components.pinnable ~= nil and (other.components.health == nil or not other.components.health:IsDead()) and not other.mobplayer then
					DoSplatFx(other.components.pinnable.stuck and inst or other)
					other.components.pinnable:Stick("goo_icing", splashfxlist)
				else
					other.components.combat:GetAttacked(caster, TUNING.WARG_GOO_DAMAGE)
					if TheNet:GetServerGameMode() == "lavaarena" then
						if other.components.colouradder then
							other.components.colouradder:PushColour("icing", 0.4, 0.4, 0.5, 1)
							other.components.combat:AddDamageBuff("icing_debuff", 0.5, false)
							if other.components.locomotor then
								other.components.locomotor:SetExternalSpeedMultiplier(other, "icing_speed_debuff", 0.25)
							end
							if other.icingtask then
								other.icingtask:Cancel()
								other.icingtask = nil
							end
							other.icingtask = other:DoTaskInTime(8, function(inst) 
								inst.components.colouradder:PopColour("icing") 
								inst.components.combat:RemoveDamageBuff("icing_debuff")
								if inst.components.locomotor then
									inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "icing_speed_debuff")
								end
							end)
						end
					end
					DoSplatFx(inst)
				end
			else
				DoSplatFx(inst)
			end
		end
	end	
	DoSplatFx(inst)
    inst:Remove()
end

local function OnProjectileHit(inst, attacker, other)
    doprojectilehit(inst, attacker, other)
    inst:Remove()
end

local function TestProjectileLand(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	if y <= inst:GetPhysicsRadius() + 0.05 then
		doprojectilehit(inst)
		inst:Remove()
	end
end

local function oncollide(inst, other)
    -- If there is a physics collision, try to do some damage to that thing.
    -- This is so you can't hide forever behind walls etc.

	--if other:HasTag("_combat") or other:HasTag("_health") and not other:HasTag("gingerbread") then
		--doprojectilehit(inst, other)
	--end
end

local function projectilefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddPhysics()
    inst.entity:AddNetwork()

    inst.Physics:SetMass(10)
	inst.Physics:SetFriction(.1)
	inst.Physics:SetDamping(0)
	inst.Physics:SetRestitution(.5)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:SetSphere(0.25)

    inst.AnimState:SetBank("warg_gingerbread_bomb")
    inst.AnimState:SetBuild("warg_gingerbread_bomb")
    inst.AnimState:PlayAnimation("spin_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    --inst.Physics:SetCollisionCallback(oncollide)

    inst.persists = false

    inst:AddComponent("locomotor")

	inst:DoPeriodicTask(0, TestProjectileLand)

--[[
    inst:AddComponent("complexprojectile")
    inst.components.complexprojectile:SetOnHit(OnProjectileHit)
    inst.components.complexprojectile:SetHorizontalSpeed(30)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(3, 2, 0))
    inst.components.complexprojectile.usehigharc = false
	
	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
]]

    return inst
end

return Prefab("warg_gooicingp", projectilefn, projectile_assets, projectile_prefabs)
