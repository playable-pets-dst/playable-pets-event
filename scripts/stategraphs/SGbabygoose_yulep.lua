require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end

local longaction = "meep2"
local shortaction = "action2"
local workaction = "action2"
local otheraction = "action2"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "spin_pre"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	CommonHandlers.OnSleep(),
	CommonHandlers.OnFreeze(),
	EventHandler("doattack", function(inst)
		if inst.components.health and not inst.components.health:IsDead()
			and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then
			if not inst.mother_dead then
				inst.sg:GoToState("attack")
			else
				inst.sg:GoToState("spin_pre")
			end
		end
	end),
	CommonHandlers.OnAttacked(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
	EventHandler("locomote", function(inst)
		local is_moving = inst.sg:HasStateTag("moving")
		local is_idling = inst.sg:HasStateTag("idle")
		local is_spinning = inst.sg:HasStateTag("spinning")
		local should_move = inst.components.locomotor:WantsToMoveForward()

		if (is_moving and not should_move) or (is_spinning and not should_move) then
			if is_spinning then
				--Stop Moving
				inst.sg.statemem.move = false
			else
				inst.sg:GoToState("walk_stop")
			end
		elseif (is_idling or is_moving or is_spinning) and should_move then
			if is_spinning then
				--Start Moving
				inst.sg.statemem.move = true
			elseif not is_moving then
				inst.sg:GoToState("walk_start")
			end
		end
	end),
}

local function GrabAggro(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 8, {"LA_mob"}, GetExcludeTagsp(inst))
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				v.components.combat:SetTarget(nil)
				v.components.combat:SetTarget(inst)
			end
		end
	end	
end

local function ShouldMooseStep(inst)
	--local ents = TheSim:FindEntities(x, y, z)
end

local function ShouldStopSpin(inst)
	local pos = inst:GetPosition()

	local nearby_player = FindClosestPlayerInRange(pos.x, pos.y, pos.z, 7.5, true)
	local time_out = inst.numSpins >= 2

	return not nearby_player or time_out
end

local function LightningStrike(inst)
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		local rad = math.random(0,3)
		local angle = math.random() * 2 * PI
		local offset = Vector3(rad * math.cos(angle), 0, -rad * math.sin(angle))

		local pos = inst:GetPosition() + offset

		TheWorld:PushEvent("ms_sendlightningstrike", pos)
		TheWorld:PushEvent("ms_forceprecipitation", true)
	end
end

local states=
{

	State{

		name = "idle",
		tags = {"idle", "canrotate"},
		onenter = function(inst, playanim)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle")
		end,

		events=
		{
			EventHandler("animover", function(inst)
				if inst.components.combat.target then
					if math.random() < 0.25 then
						inst.sg:GoToState("taunt")
						return
					end
				end
				inst.sg:GoToState("idle")
			end),
		},
	},
	
	State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()
			
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/death") 
            inst.AnimState:PlayAnimation("death")
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },
	},	

	State{
		name = "action2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("action")
			PlayablePets.DoWork(inst, 3)
			--inst.AnimState:PushAnimation("eat", false)
			--inst.sg:SetTimeout(math.random()*2+1)
		end,

		timeline=
		{
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/eat") end),

			TimeEvent(10*FRAMES, function(inst)
				inst:PerformBufferedAction()
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("eat_pst") end)
		},

		--ontimeout = function(inst)
			--inst.sg:GoToState("eat_pst")
		--end,
	},
	
	State{
		name = "eat_loop",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PushAnimation("eat", false)
		end,

		events =
		{
			EventHandler("animqueueover", function(inst)
				inst:PerformBufferedAction()
				inst.sg:GoToState("eat_pst")
			end)
		},

		timeline =
		{
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/chew") end),
		},
	},

	State{
		name = "eat_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("eat_pst")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/strain")
		end,

		timeline = {},

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "taunt",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt_pre")
			inst.AnimState:PushAnimation("taunt")
			inst.AnimState:PushAnimation("taunt_pst", false)
		end,

		timeline=
		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/taunt") end),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
		},

		events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},


	State{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("meep")
		end,

		timeline=
		{
			TimeEvent(7*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") 		
			end),
			--TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/honk") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(15*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") 
				if TheNet:GetServerGameMode() == "lavaarena" then
					GrabAggro(inst)
				end	
			end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "meep2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("meep")
		end,

		timeline=
		{
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/honk") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "flyaway",
		tags = {"flying", "busy"},
		onenter = function(inst)
			inst.Physics:Stop()
			inst.DynamicShadow:Enable(false)
			inst.AnimState:PlayAnimation("takeoff_pre_vertical")
			inst.sg.statemem.strainSound = 20*FRAMES
			inst.sg.statemem.flapSound = 9*FRAMES
		end,

		onupdate = function(inst, dt)
			inst.sg.statemem.strainSound = inst.sg.statemem.strainSound - dt
			if inst.sg.statemem.strainSound <= 0 then
				inst.sg.statemem.strainSound = 70*FRAMES
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/strain")
			end

			inst.sg.statemem.flapSound = inst.sg.statemem.flapSound - dt
			if inst.sg.statemem.flapSound <= 0 then
				inst.sg.statemem.flapSound = 3*FRAMES
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap")
			end
		end,

		timeline =
		{
			TimeEvent(9*FRAMES, function(inst)
				inst.AnimState:PushAnimation("takeoff_vertical", true)
				inst.Physics:SetMotorVel(-2 + math.random()*4,3+math.random()*2,-2 + math.random()*4)
			end),
			TimeEvent(10, function(inst) inst:Remove() end)
		}
	},

	State{
		name = "hatch",
		tags = {"busy"},

		onenter = function(inst)
			local angle = math.random()*2*PI
			local speed = GetRandomWithVariance(3, 2)
			inst.Physics:SetMotorVel(speed*math.cos(angle), 0, speed*math.sin(angle))
			inst.AnimState:PlayAnimation("hatch")
		end,

		timeline =
		{
			TimeEvent(FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/hatch") end),
			TimeEvent(20*FRAMES, function(inst) inst.Physics:SetMotorVel(0,0,0) end),
			TimeEvent(47*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/pop") end)
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "special_atk2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spin_pre")
			inst.components.burnable:Extinguish()
			inst.numSpins = 0
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("transform_loop") end),
		},

		timeline =
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/attack") end),
		},
	},

	State{
		name = "transform_loop", --transform into angry and non angry builds without having to attack.
		tags = {"busy", "spinning"},

		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			if inst.isangry == false then
				inst.isangry = true
				inst.DynamicShadow:SetSize(2.5,1.25)
				inst.components.sizetweener:StartTween(1.55, 2)			
			elseif inst.isangry == true then
				inst.isangry = false
				inst.DynamicShadow:SetSize(1.5,1.25)
				inst.components.sizetweener:StartTween(1, 1)
			end
			inst.components.ppskin_manager:LoadSkin(inst.mob_table, true)
			
			inst.AnimState:PlayAnimation("spin_loop")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/spin", "spinLoop")
			
			local fx = SpawnPrefab("mossling_spin_fx")
			fx.entity:SetParent(inst.entity)
			inst.components.burnable:Extinguish()
		end,

		onupdate = function(inst)
			if inst.sg.statemem.move then
				inst.components.locomotor:WalkForward()
			else
				inst.components.locomotor:StopMoving()
			end
		end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("spinLoop")
			inst.components.locomotor:StopMoving()
		end,

		timeline=
		{
		
		},

		events=
		{
			EventHandler("animover",
			function(inst)
				--inst.numSpins = inst.numSpins + 1
				--if ShouldStopSpin(inst) then
					inst.sg:GoToState("transform_pst")
				--else
					--inst.sg:GoToState("spin_loop")
				--end
			end),
		},
	},

	State{
		name = "transform_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spin_pst")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "transform_pst_loop",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spin_pst_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/dizzy", "dizzy")
			inst.sg:SetTimeout(math.random() + 4.5)
		end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("dizzy")
		end,

		timeline=
		{
		},

		ontimeout = function(inst)
			inst.sg:GoToState("spin_pst_loop_pst")
		end,
	},

	State{
		name = "spin_pre",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spin_pre")
			inst.components.burnable:Extinguish()
			inst.numSpins = 0
			inst.isangry = true
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("spin_loop") end),
		},

		timeline =
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/attack") end),
		},
	},

	State{
		name = "spin_loop",
		tags = {"busy", "spinning"},

		onenter = function(inst)
		
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end

			inst.components.combat:StartAttack()
			inst.DynamicShadow:SetSize(2.5,1.25)
			inst.components.sizetweener:StartTween(1.55, 2)
			if inst.isshiny ~= 0 then
			inst.AnimState:SetBuild("mossling_yule_angry_shiny_build_0"..inst.isshiny)
			else
			inst.AnimState:SetBuild("mossling_yule_angry_build")
			end
			--inst.sg.statemem.target = target
			
			inst.AnimState:PlayAnimation("spin_loop")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/spin", "spinLoop")

			--local fx = SpawnPrefab("mossling_spin_fx")
			--fx.entity:SetParent(inst.entity)
			inst.components.burnable:Extinguish()
		end,

		onupdate = function(inst)
			--if inst.sg.statemem.move then
				inst.components.locomotor:WalkForward()
			--else
				--inst.components.locomotor:StopMoving()
			--end
		end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("spinLoop")
			inst.components.locomotor:StopMoving()
			inst.components.combat:SetTarget(nil)
		end,

		timeline=
		{
			--TimeEvent(5*FRAMES, function(inst)
				--if math.random() < 0.1 then
					--LightningStrike(inst)
				--end
			--end),
			--TimeEvent(0*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(15*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(30*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(45*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(60*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(75*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(90*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
			TimeEvent(100*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
		},

		events=
		{
			EventHandler("animover",
			function(inst)
				inst.numSpins = inst.numSpins + 1
				if ShouldStopSpin(inst) then
					inst.sg:GoToState("spin_pst")
				else
					inst.sg:GoToState("spin_loop")
				end
			end),
		},
	},

	State{
		name = "spin_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spin_pst")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("spin_pst_loop") end),
		},
	},

	State{
		name = "spin_pst_loop",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spin_pst_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/dizzy", "dizzy")
			inst.sg:SetTimeout(math.random() + 4.5)
		end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("dizzy")
		end,

		timeline=
		{
		},

		ontimeout = function(inst)
			inst.sg:GoToState("spin_pst_loop_pst")
		end,
	},

	State{
		name = "spin_pst_loop_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spin_pst_loop_pst")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		timeline =
		{
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/yawn") end)
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline =
		{
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/sleep") end)
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline =
		{
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/hatch") end)
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(states,
{
	walktimeline =
	{
		TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
		TimeEvent(5*FRAMES, function(inst) PlayFootstep(inst) end),
		TimeEvent(10*FRAMES, function(inst) PlayFootstep(inst) end),
	}
})

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
		TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/eat") end),
		TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/strain") end)
	}, 
	"action", "eat_pst", nil, "meep", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/death")  end),
		},
		
		corpse_taunt =
		{
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			--TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/honk") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mossling/flap") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "meep"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "meep")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)



return StateGraph("babygooseplayer", states, events, "idle", actionhandlers)
