require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "steal"
local shortaction = "action"
local workaction = "steal"
local otheraction = "action2"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local SHAKE_DIST = 40

local function destroystuff(inst)
	local pt = inst:GetPosition()
	local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, 5)
	local heading_angle = -(inst.Transform:GetRotation())
	for k,v in pairs(ents) do
		if v and v.components.workable and v.components.workable.workleft > 0 and v.components.workable.action ~= ACTIONS.NET then
			SpawnPrefab("collapse_small").Transform:SetPosition(v:GetPosition():Get())
			v.components.workable:Destroy(inst)
		end
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall"}
	end
end

local function DoFrontAoEp(inst)
	local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 3
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 6, nil, GetExcludeTagsp(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)*(inst.isguarding and 0.5 or 1))
				inst:PushEvent("onattackother", { target = v })
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local events=
{
	CommonHandlers.OnLocomote(true,true),
	CommonHandlers.OnSleep(),
	CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
	EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("busy") and inst.israged ~= nil and inst.israged == false then inst.sg:GoToState("hit") end end),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
	end
end

local function ShakeIfClose_Footstep(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function DoFootstep(inst)
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
	ShakeIfClose_Footstep(inst)
	--ShakeIfClose(inst)
end

local states=
{

	State{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop", true)
		end,

		timeline=
		{

		},
	},

----------------------COMBAT------------------------

	State{
		name = "targetstolen",
		tags = {"busy", "canrotate"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt")
		end,

		timeline=
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt") end),
			TimeEvent(9*FRAMES, function(inst) DoFootstep(inst) end),
			TimeEvent(33*FRAMES, function(inst) DoFootstep(inst) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "special_atk2",
		tags = {"busy", "canrotate"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_pst")
			if inst.israged == true then
				inst.israged = false
			else	
			inst.Physics:Stop()
			inst.israged = true
			inst.sg:GoToState("targetstolen")
			end
		end,

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "hit",
		tags = {"hit", "busy"},

		onenter = function(inst, cb)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("standing_hit")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "attack",
		tags = {"attack", "busy", "canrotate"},

		onenter = function(inst)
            inst.components.locomotor:StopMoving()
                
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")				
        end,

		timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(35*FRAMES, function(inst) 
				if TheNet:GetServerGameMode() == "lavaarena" then
					DoFrontAoEp(inst)
				else
					PlayablePets.DoWork(inst, 8)
				end	
			end),
		},


		events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "quick_attack",
		tags = {"attack", "busy", "canrotate"},

		onenter = function(inst)
      
                inst.components.locomotor:StopMoving()                
                inst.components.combat:StartAttack()
                inst.AnimState:PlayAnimation("atk")			
				inst.AnimState:SetTime(15* FRAMES)
        end,

		timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(20*FRAMES, function(inst) inst:PerformBufferedAction() end),
		},


		events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "special_atk1",
		tags = {"attack", "busy"},

		onenter = function(inst)
			if inst.taunt == false then
				inst.sg:GoToState("idle")
			else 
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("ground_pound")
				inst.AnimState:PushAnimation("taunt_pre", false)
			end	
		end,
		
		onexit = function(inst)
			if inst.components.revivablecorpse then
				inst.components.combat:SetAreaDamage(PPEV_FORGE.BEARGER_YULE.AOE_RANGE, PPEV_FORGE.BEARGER_YULE.AOE_DMGMULT)
			end		
		end,

		timeline=
		{
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(20*FRAMES, function(inst)
				ShakeIfClose(inst)
				local isname = inst:GetDisplayName()
				local selfpos = inst:GetPosition()
				if not inst.components.revivablecorpse then
					print("LOGGED: "..isname.." used ground pound at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
				end
				if inst.components.revivablecorpse then
					inst.components.combat:SetAreaDamage(PPEV_FORGE.BEARGER_YULE.AOE_RANGE, PPEV_FORGE.BEARGER_YULE.GP_DMGMULT)
					inst.components.combat:DoAreaAttack(inst, 6, nil, nil, "strong", {"player", "companion"})
				end
				inst.components.groundpounder:GroundPound()
				inst.cangroundpound = false
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},

		events=
		{
			EventHandler("animqueueover", function(inst)
				inst.taunt = false
				inst:DoTaskInTime(10, function() inst.taunt = true end)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
		name = "death",
		tags = {"busy", "pausepredict", "nomorph"},

		onenter = function(inst)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("death")
			inst.components.inventory:DropEverything(true)
			--inst.Physics:ClearCollisionMask()
			
		end,

		timeline=
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/death") end),
			--TimeEvent(6*FRAMES, function(inst)inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) end),
			TimeEvent(50*FRAMES, function(inst)
				ShakeIfClose(inst)
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},
		
		 events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

	},

-------------------EATING-------------------------

	State{
		name = "action2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("action")
			inst.AnimState:PushAnimation("eat_loop", false)
		end,

		onexit = function(inst)

		end,

		timeline=
		{
			TimeEvent(5*FRAMES, function(inst) end),
			TimeEvent(15*FRAMES, function(inst)
				inst:PerformBufferedAction()
				--inst.sg:RemoveStateTag("busy")
				--inst.sg:AddStateTag("wantstoeat")
				--inst.last_eat_time = GetTime()
			end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/gulp") end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("eat_pst") end)
		}
	},

	State{
		name = "eat_loop",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PushAnimation("eat_loop", true)
			local timeout = math.random()+.5
			local ba = inst:GetBufferedAction()
			if ba and ba.target and ba.target:HasTag("edible") then
					timeout = timeout*2
			inst.sg:SetTimeout(timeout)
			end
		end,

		timeline=
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
			TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
		},

		ontimeout = function(inst)
			local ba = inst:GetBufferedAction()
			inst:PerformBufferedAction()
			inst.last_eat_time = GetTime()
			inst.sg:GoToState("eat_pst")
		end,

	},

	State{
		name = "eat_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("eat_pst")
		end,

		timeline=
		{
		},

		events=
		{
			EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
		name = "steal",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("atk", false)
		end,

		timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(35*FRAMES, function(inst) inst:PerformBufferedAction() end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

---------------------------WALKING---------------
-- the tags and names have been switched around to trick the game into making you "walk"

	State{
			name = "run_start",
			tags = {"moving", "running", "canrotate"},

			onenter = function(inst)
			if inst.israged == true and inst.components.hunger:GetPercent() > 0.5 then
				inst.components.locomotor:RunForward()
				inst.AnimState:PlayAnimation("charge_pre")
			else
				inst.components.locomotor:WalkForward()
				inst.AnimState:PlayAnimation("walk_pre")
			end
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_pre" or "walk_pre"
				--inst.AnimState:PlayAnimation(anim)
			end,

			events =
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},
		},

	State{
			name = "run",
			tags = {"moving", "running", "canrotate"},

			onenter = function(inst)
				if inst.israged == true then
					inst.components.locomotor:RunForward()
					inst.AnimState:PlayAnimation("charge_loop")
				else
					inst.components.locomotor:WalkForward()
					inst.AnimState:PlayAnimation("walk_loop")
				end
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_loop" or "walk_loop"
				--inst.AnimState:PlayAnimation(anim)
				--inst.components.locomotor:RunForward()
				if inst.israged == true then
					inst:DoTaskInTime(math.random(13)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/grrrr") end)
				end
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

			timeline=
			{
				TimeEvent(2*FRAMES, function(inst)
					if inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
				TimeEvent(18*FRAMES, function(inst)
					if inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
				TimeEvent(4*FRAMES, function(inst)
					if not inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
				TimeEvent(30*FRAMES, function(inst)
					if not inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
			},
		},

	State{
			name = "run_stop",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_pst" or "walk_pst"
				DoFootstep(inst)
				ShakeIfClose(inst)
				if inst.israged == true then
					inst.AnimState:PlayAnimation("charge_pst")
				else
					inst.AnimState:PlayAnimation("walk_pst")
				end
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},

	State{
			name = "walk_start",
			tags = {"moving", "atk_pre", "canrotate"},

			onenter = function(inst)
				inst.components.locomotor:WalkForward()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt", "taunt")
				inst.AnimState:PlayAnimation("charge_pre")
			end,

			events =
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},
		},

	State{

			name = "walk",
			tags = {"moving", "canrotate"},

			onenter = function(inst)
				--inst.components.locomotor:WalkForward()
				if not inst.SoundEmitter:PlayingSound("taunt") then inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt", "taunt") end
				inst.AnimState:PlayAnimation("charge_roar_loop")
			end,

			timeline=
			{
				TimeEvent(3*FRAMES, function(inst)
					DoFootstep(inst)
					destroystuff(inst)
				end),
				TimeEvent(11*FRAMES, function(inst)
					DoFootstep(inst)
					destroystuff(inst)
				end),
			},

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},
		},

	State{

			name = "walk_stop",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--local should_softstop = false
				inst.AnimState:PlayAnimation("charge_pst")
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},
		
		State{

			name = "action",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst:PerformBufferedAction()
				inst.AnimState:PlayAnimation("charge_pst")
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},

------------------SLEEPING-----------------

		State{
			name = "sleep",
			tags = {"busy", "sleeping"},

			onenter = function(inst)
				
				inst.components.locomotor:StopMoving(true)
				inst.AnimState:PlayAnimation("standing_sleep_pre")
				inst.AnimState:PushAnimation("sleep_pre", false)
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
				EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
			},

			timeline=
			{
				TimeEvent(25*FRAMES, function(inst)
					DoFootstep(inst)
				end),
			},
		},

		State{

			name = "sleeping",
			tags = {"sleeping", "busy"},
			onenter = function(inst)
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.Physics:Stop()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
			timeline =
			{
				TimeEvent(0*FRAMES, function(inst) inst.components.locomotor:StopMoving() end)
			},

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),				
			},
		},

		State{

			name = "wake",
			tags = {"busy", "waking"},

			onenter = function(inst)
				--inst.last_eat_time = GetTime() -- Fake this as eating so he doesn't aggro immediately
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_pst")
				inst.AnimState:PushAnimation("taunt_pre", false)
				if inst.components.sleeper and inst.components.sleeper:IsAsleep() then
					inst.components.sleeper:WakeUp()
				end
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
			},

			timeline=
			{
				TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt_short") end),
			},
		},
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/death") end),
			TimeEvent(50*FRAMES, function(inst)
				ShakeIfClose(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt") end),
			TimeEvent(9*FRAMES, function(inst) DoFootstep(inst) end),
			TimeEvent(33*FRAMES, function(inst) DoFootstep(inst) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

return StateGraph("bearger_yulep", states, events, "idle", actionhandlers)

