require("stategraphs/commonstates")
require("stategraphs/ppstates")
--TODO This code is shit, redo it.

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .25, .015, .25, inst, 10)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
    ShakeAllCameras(CAMERASHAKE.FULL, 0.5, .03, .5, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.8, .03, .5, inst, 30)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.isguarding then
			return "attack_guard"
		elseif inst.altattack and inst.altattack == true then
			return "attack2"
		else	
			return "attack"
		end	
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetCD(inst)
	local health = inst.components.health:GetPercent()
	
	if health <= 0.3 then
		return 8
	else
		return 20
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end

local function DoFrontAoEp(inst)
	local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 3
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 4, nil, GetExcludeTagsp(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)*(inst.isguarding and 0.5 or 1))
				inst:PushEvent("onattackother", { target = v })
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local function DoSlamp(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 7, nil, GetExcludeTagsp(inst))
	inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
	inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v))
			end
		end
	end	
end

local function DoGroundPoundp(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 7, nil, GetExcludeTagsp(inst))
	ShakeAllCameras(CAMERASHAKE.FULL, 0.5, .03, .5, inst, 30)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
	inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)/(BOSS_STATS and 1 or 3))
				v:PushEvent("flipped")
			end
		end
	end	
end

local function BreakBlockp(inst)
	if inst.isguarding then
		inst.isguarding = nil
	end
	inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/shatter")
	local fx = SpawnPrefab("swineclops_break_fxp")
		fx.Transform:SetPosition(inst:GetPosition():Get())
	if inst.altattack and inst.altattack == true then
		inst.components.combat:SetRange(10, 0)
	else
		inst.components.combat:SetRange(3, 5)
	end
	inst.components.combat:SetAttackPeriod(inst.components.health:GetPercent() > 0.5 and 2 or 4)
	inst.components.debuffable:RemoveDebuff("swineclops_defensebuffp")
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.components.health:SetAbsorptionAmount(0)
	end
	if not inst.sg:HasStateTag("busy") then
		inst.sg:GoToState("block_pst")
	end
end

local function DoAttackBuffp(inst)
	inst.components.debuffable:RemoveDebuff("swineclops_defensebuffp")
	inst.components.debuffable:AddDebuff("swineclops_attackbuffp", "swineclops_attackbuffp")
end

local function DoDefenseBuffp(inst)
	inst.components.debuffable:RemoveDebuff("swineclops_attackbuffp")
	inst.components.debuffable:AddDebuff("swineclops_defensebuffp", "swineclops_defensebuffp")
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
		print(distsq)
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(10, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local function SetJumpPhysics(inst)
	ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	ToggleOnCharacterCollisions(inst)
end
-----------------------------------------------------

local events=
{
	EventHandler("attacked", function(inst, data) 		
		if data.stimuli and data.stimuli == "strong" and not (inst.isguarding or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + 0.75 < GetTime()) then 
			inst.sg:GoToState(inst.isguarding and "hit_guard" or "hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("victorypose", function(inst)
		if not inst.sg:HasStateTag("busy") then
			inst.sg:GoToState("pose")
		end
	end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			inst.sg:GoToState("death")	
        end
    end),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
			if inst.altattack and inst.altattack == true then
				inst.sg:GoToState("attack2", data.target) 
			else
				inst.sg:GoToState("attack", data.target) 
			end
		end 
	end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		
        if is_moving and not should_move then
			inst.sg:GoToState(inst.isguarding and "run_stop" or "walk_stop")
        elseif not is_moving and should_move then
			inst.sg:GoToState(inst.isguarding and "run_start" or "walk_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
			if inst.isguarding and inst.isguarding == true then
				inst.AnimState:PlayAnimation("block_loop", true)
			else
				inst.AnimState:PlayAnimation("idle_loop", true)
			end
        end,

    },

	State{
        name= "emote",
        tags = {"idle"},
        
        onenter = function(inst, data)
			inst.Physics:Stop()
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack")
			
			if data.anim and data.anim == "emoteXL_angry" then
				inst.AnimState:PlayAnimation("end_pose_pre", false)
				inst.AnimState:PushAnimation("end_pose_loop", true)
			elseif data.anim and data.anim == "emoteXL_happycheer" then
				inst.AnimState:PlayAnimation("end_pose_pre", false)
				inst.AnimState:PushAnimation("end_pose_loop", true)
			elseif data.anim and data.anim == "emoteXL_annoyed" then
				inst.AnimState:PlayAnimation("end_pose_pre", false)
				inst.AnimState:PushAnimation("end_pose_loop", true)
			elseif data.anim and data.anim == "emoteXL_sad" then 
				inst.AnimState:PlayAnimation("end_pose_pre", false)
				inst.AnimState:PushAnimation("end_pose_loop", true)
			elseif data.anim and data.anim == "emoteXL_strikepose" then 
				inst.AnimState:PlayAnimation("end_pose_pre", false)
				inst.AnimState:PushAnimation("end_pose_loop", true)
			else
				inst.sg:GoToState("idle")
			end	
        end,
        
        events=
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name = "work", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/swipe")
        inst.AnimState:PlayAnimation("attack3")
        if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)
		inst.normalattack = nil
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(6*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/attack")
				inst:PerformBufferedAction()
			end),
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "attack_guard", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/swipe")
        inst.AnimState:PlayAnimation("block_counter")
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(6*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
				inst.components.combat:DoAreaAttack((inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid()) and inst.sg.statemem.attacktarget or inst, 5, nil, nil, nil, GetExcludeTagsp(inst)) 
			end),
    },

        events=
        {
			EventHandler("onhitother", function(inst) 
				 inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/attack")
			end),
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
    State{
        name = "attack", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
        
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.currentcombo = inst.currentcombo + 1
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/swipe")
        inst.AnimState:PlayAnimation("attack1", false)
		inst.leftjab = nil
		if inst.currentcombo >= inst.maxpunches then
			inst.AnimState:PushAnimation("attack1_pst", false)
		end
	end,
	
	onexit = function(inst)
		if inst.currentcombo >= inst.maxpunches then
			inst.currentcombo = 0
		end
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(6*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				DoFrontAoEp(inst)
			end),
    },

        events=
        {
			EventHandler("onmissother", function(inst) 
				inst.AnimState:PushAnimation("attack1_pst", false)
				inst.currentcombo = 99 --end combo the lazy way
			end),
			EventHandler("onhitother", function(inst) 
				 --inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/attack")
			end),
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState(inst.currentcombo < inst.maxpunches and "attack_loop" or "idle")  
			end),
        },
	
    },
	
	State{
        name = "attack_loop", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
        
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.currentcombo = inst.currentcombo + 1
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/swipe")
        inst.AnimState:PlayAnimation((inst.maxpunches > 2 and inst.currentcombo == inst.maxpunches) and "attack3" or "attack2", false)
		if inst.maxpunches == 2 or (inst.currentcombo > inst.maxpunches or (inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget.components.health:IsDead())) then
			inst.AnimState:PushAnimation("attack2_pst", false)
		elseif inst.leftjab and (inst.currentcombo > inst.maxpunches or (inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget.components.health:IsDead())) then
			inst.AnimState:PushAnimation("attack1_pst", false)
		end
		
		if not inst.leftjab then
			inst.leftjab = true
		end	
	end,
	
	onexit = function(inst)
		if inst.currentcombo >= inst.maxpunches then
			inst.currentcombo = 0
		end
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(6*FRAMES, function(inst) 
				DoFrontAoEp(inst)
				if inst.maxpunches > 2 and inst.currentcombo == inst.maxpunches then
					inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/attack")
				end
			end),
    },

        events=
        {
		
			EventHandler("onmissother", function(inst) 
				if inst.currentcombo > inst.maxpunches and inst.currentcombo ~= inst.maxpunches then
					inst.AnimState:PushAnimation("attack2_pst", false)
				end
				inst.currentcombo = 99 --end combo the lazy way
			end),
            EventHandler("onhitother", function(inst, data) 
				if data.target and data.target.components.health and data.target.components.health:IsDead() then
					inst.currentcombo = 99
				end				
			end),
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState(inst.currentcombo < inst.maxpunches and "attack_loop2" or "idle")  
			end),
        },
	
    },
	
	State{
        name = "attack_loop2", --left punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
        
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.currentcombo = inst.currentcombo + 1
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/swipe")
        inst.AnimState:PlayAnimation((inst.maxpunches > 2 and inst.currentcombo == inst.maxpunches) and "attack3" or "attack1b", false)
		if inst.maxpunches == 2 or (inst.currentcombo > inst.maxpunches or (inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget.components.health:IsDead())) then
			inst.AnimState:PushAnimation("attack1_pst", false)
		end
	end,
	
	onexit = function(inst)
		if inst.currentcombo >= inst.maxpunches then
			inst.currentcombo = 0
		end
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(6*FRAMES, function(inst) 
				DoFrontAoEp(inst)
			end),
    },

        events=
        {
		
			EventHandler("onmissother", function(inst) 
				if inst.currentcombo > inst.maxpunches and inst.currentcombo ~= inst.maxpunches then
					inst.AnimState:PushAnimation("attack1_pst", false)
				end
				inst.currentcombo = 99 --end combo the lazy way
			end),
            EventHandler("onhitother", function(inst, data) 
				if data.target and data.target.components.health and data.target.components.health:IsDead() then
					inst.currentcombo = 99
				end				
			end),
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState(inst.currentcombo < inst.maxpunches and "attack_loop" or "idle")  
			end),
        },
	
    },
	
	State{
        name = "attack2", --bellyflop
        tags = {"busy", "slamming"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		inst.altattack = nil
		
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:StartAttack()
        
		inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/jump")
        inst.AnimState:PlayAnimation("bellyflop")
	end,
	
	onexit = function(inst)
		inst.sg.statemem.jump = nil
		inst.sg.statemem.attacktarget = nil
		inst.sg.mem.speed = nil
		inst.components.combat:SetRange(3, 5)
		inst:DoTaskInTime(10, function(inst) inst.altattack = true if not inst.isguarding then inst.components.combat:SetRange(7, 0) end end)
    end,
	
	onupdate = function(inst)
        if inst.sg.statemem.jump then
            inst.Physics:SetMotorVel(inst.sg.mem.speed and inst.sg.mem.speed * 1.2 or 5, 0, 0)
        end
    end,
	
    timeline=
    {
		TimeEvent(1*FRAMES, function(inst) 
			inst.sg.statemem.jump = true
			SetJumpPhysics(inst)
			inst.sg:AddStateTag("nointerrupt")
			if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then
				inst.sg.mem.speed = CalcJumpSpeed(inst, inst.sg.statemem.attacktarget)
			end
		end),
		TimeEvent(13*FRAMES, function(inst) 
			--inst:PerformBufferedAction()
			inst.components.locomotor:Stop()
			inst.sg.statemem.jump = false 
			inst.Physics:Stop()
			SetNormalPhysics(inst)
			DoSlamp(inst)
			ShakePound(inst)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit")
		end),
    },

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.statemem.last_attacked = GetTime()
			inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/hit")
			inst.currentcombo = 0
        end,
		
		timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(8 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step")
            end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit_guard",
        tags = {"busy", "hit"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("block_hit")
			--inst.sg.statemem.last_attacked = GetTime()
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit")
			inst.rams = 0
			inst.cheer_loop = 0
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt1",
        tags = {"busy"},

        onenter = function(inst, force)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt")
			DoAttackBuffp(inst)
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/taunt")
				ShakeRoar(inst) 
			end),
			TimeEvent(24*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(28*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(32*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(36*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
        },
		
		onexit = function(inst)
			if inst.canbuff == false then inst:DoTaskInTime(20, function(inst) inst.canbuff = true end) end
		end,
		

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, force)
			inst.Physics:Stop()
			if inst.canbuff and inst.canbuff == true and not inst.isguarding then
				inst.canbuff = false
				inst.sg:GoToState("taunt1")
			else
				inst.taunt = false
				inst.AnimState:PlayAnimation("taunt2")
			end
        end,

		timeline=
        {
			TimeEvent(8*FRAMES, function(inst) 
				DoGroundPoundp(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(11*FRAMES, function(inst) 
				DoGroundPoundp(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(14*FRAMES, function(inst) 
				--DoGroundPoundp(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(24*FRAMES, function(inst) 
				DoGroundPoundp(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
        },
		
		onexit = function(inst)
			if inst.taunt == false then inst:DoTaskInTime(7, function(inst) inst.taunt = true end) end
		end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	--block
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, force)
			inst.Physics:Stop()
			inst.isguarding = true
			inst.taunt2 = false
			inst.components.combat:SetRange(3, 5)
			DoDefenseBuffp(inst)
			inst.AnimState:PlayAnimation("block_pre")
        end,
		
		onexit = function(inst)
			inst:DoTaskInTime(40, function(inst) inst.taunt2 = true end)
			inst:DoTaskInTime(15, BreakBlockp)
		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") inst.sg:RemoveStateTag("busy") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death")
                ShakePound(inst)
            end),
			TimeEvent(13*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/shatter") -- TODO: Leo: need to double check this sound.
            end),
			TimeEvent(43*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				ShakePound(inst)
			end),
			TimeEvent(62*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit")
				ShakePound(inst)
			end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(8*FRAMES, function(inst) 
				--ShakeIfClose(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") 
			end),
			TimeEvent(30*FRAMES, function(inst) 
				--ShakeIfClose(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") 
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) 
				inst.sg:GoToState("sleeping") 
			end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				local hungerpercent = inst.components.hunger:GetPercent()
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/sleep_in")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/sleep_out") end),
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			--end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline=
        {
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			--end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = {

			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst)  end),
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
			--TimeEvent(1*FRAMES, ShakeIfClose),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step")
			ShakeIfClose(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("walk")  
				end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = {

			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/step") ShakeIfClose(inst) end),
			--TimeEvent(1*FRAMES, ShakeIfClose),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "block_pst", 
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("block_pst")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                        inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.currentcombo = 0
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(1)
				--inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
			else
				--inst.components.combat:SetTarget(nil)
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt")
			end
			inst.sg.statemem.flash = 0
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/grunt") end),
        },
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			if TheNet:GetServerGameMode() ~= "lavaarena" then 
				inst.components.health:SetAbsorptionAmount(0)        
			end   
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" then
				inst.sg.statemem.needstotaunt = true
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt1")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death")
                ShakePound(inst)
            end),
			TimeEvent(13*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/shatter") -- TODO: Leo: need to double check this sound.
            end),
			TimeEvent(43*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				ShakePound(inst)
			end),
			TimeEvent(62*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit")
				ShakePound(inst)
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/taunt")
				ShakeRoar(inst) 
			end),
			TimeEvent(24*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(28*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(32*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
			TimeEvent(36*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/beetletaur/chain_hit") 
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)


    
return StateGraph("beetletaurp", states, events, "idle", actionhandlers)

