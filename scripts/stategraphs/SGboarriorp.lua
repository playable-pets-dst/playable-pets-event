require("stategraphs/commonstates")
require("stategraphs/ppstates")

local BANNER_LOOPS = 20

local function GetCD(inst)
	local health = inst.components.health:GetPercent()
	if health > 0.75 then
		return 15
	elseif health <= 0.75 and health > 0.5 then
		return  10
	elseif health <= 0.5 and health > 0.25 then
		return 5
	end	
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"boarrior_pet", "notarget", "INLIMBO", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.7, .03, .5, inst, 30)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack and inst.altattack == true then
			return "attack_special"
		else
			return "attack"
		end	
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function DoFrontAoEp(inst)
	local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 3
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 5, nil, GetExcludeTags(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)*(inst.isguarding and 0.5 or 1))
				inst:PushEvent("onattackother", { target = v })
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    }, 
	
	State{
		name = "stun",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
            inst.Physics:Stop()
			if stimuli ~= nil and stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(1)
			else
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/hit")
			end
			inst.sg.stun_stimuli = stimuli
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.stun_stimuli or nil)
		end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.stun_stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli ~= nil then inst.sg.stun_stimuli = stimuli end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {

        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.banner_calls and inst.banner_calls < BANNER_LOOPS and inst.sg.statemem.wants_to_banner and not inst.spawned_pigs then
					inst.sg:GoToState("banner_pre")
				else
					inst.sg:GoToState("idle") 
				end	
				inst.sg.stun_stimuli = nil
			end),
        },
    },
	
	State{
		name = "banner_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_pre")
			if not inst.spawned_pigs then
				inst:DoTaskInTime(1, function(inst) inst:SpawnBanners() end)
				inst:DoTaskInTime(5.5, function(inst) inst:SummonPigs() end)
			end
			inst.sg.statemem.wants_to_banner = true --might use to force finish the banner calling.
			--run summon functions here, maybe with a delay.
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("banner_loop") end),
        },
    },
	
	State{
		name = "banner_loop",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_loop", true)
			inst.banner_calls = inst.banner_calls + 1
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/banner_call_a") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.banner_calls < BANNER_LOOPS then
					inst.sg:GoToState("banner_loop") 
				else
					inst.banner_calls = 0 --I don't expect this state to rerun in one life, but just to be safe.	
					inst.sg.statemem.wants_to_banner = nil
					inst.sg:GoToState("banner_pst")
				end
			end),
        },
    },
	
	State{
		name = "banner_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_pst")
			inst.sg.statemem.wants_to_banner = nil
			--run summon functions here, maybe with a delay.
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "attack", --swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()		
        if inst.components.combat.target ~= nil then
            if inst.components.combat.target:IsValid() then
                inst:ForceFacePoint(inst.components.combat.target:GetPosition())
            end
        end
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack1")
		if inst.components.health:GetPercent() > 0.5 or inst.altattack3 == false then
			inst.AnimState:PushAnimation("attack1_pst", false)
		elseif not inst.sg.statemem.attacktarget or inst.sg.statemem.attacktarget.components.health:IsDead() then
			inst.AnimState:PushAnimation("attack1_pst", false)
		end
	end,

    timeline=
    {

			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
            TimeEvent(11*FRAMES, function(inst)
				DoFrontAoEp(inst)
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.components.health:GetPercent() <= 0.5 and inst.sg.statemem.attacktarget and not inst.sg.statemem.attacktarget.components.health:IsDead() and inst.altattack3 and inst.altattack3 == true then
					inst.altattack3 = false
					inst:DoTaskInTime(10, function(inst) inst.altattack3 = true end)
					inst.sg:GoToState("attack2", inst.sg.statemem.attacktarget)
				else
					inst.sg:GoToState("idle")  
				end
			end),
        },
	
    },
	
	State{
        name = "attack2", --swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
		if inst.sg.statemem.attacktarget then  
			inst:ForceFacePoint(inst.sg.statemem.attacktarget:GetPosition())
		end
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack2")
		if not inst.sg.statemem.attacktarget or inst.sg.statemem.attacktarget.components.health:IsDead() then
			inst.AnimState:PushAnimation("attack2_pst", false)	
		end
        
		inst.Physics:SetMotorVelOverride(10,0,0)
	end,
	
	onexit = function(inst)

    end,

    timeline=
    {

            TimeEvent(4*FRAMES, function(inst) 
				DoFrontAoEp(inst)
			end),
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
			TimeEvent(6*FRAMES, function(inst) inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop() end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.sg.statemem.attacktarget and not inst.sg.statemem.attacktarget.components.health:IsDead() then
				inst.sg:GoToState("attack3", inst.sg.statemem.attacktarget)
				else
				inst.sg:GoToState("idle")  
				end end),
        },
	
    },
	
	State{
        name = "attack3", --final swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        --inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack3")
        if inst.sg.statemem.attacktarget then  
			inst:ForceFacePoint(inst.sg.statemem.attacktarget:GetPosition())
		end
		
		if inst.brain ~= nil then
		inst.brain:Stop()
		end
		inst.Physics:SetMotorVelOverride(10,0,0)
	end,
	
	onexit = function(inst)

    end,

    timeline=
    {

			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
			TimeEvent(6*FRAMES, function(inst) inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop() end),
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/taunt_2")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt_short")
			end),
			TimeEvent(6*FRAMES, ShakeIfClose),
            TimeEvent(6*FRAMES, function(inst) 
				DoFrontAoEp(inst)
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("idle")
			end),
        },
	
    },
	
	State{
        name = "attack_special", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target, forcedtarget)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
		inst.altattack = false
		inst.is_doing_special = true --To prevent knockback effects
		inst.Transform:SetEightFaced()
        --inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt")
--		inst:AddTag("groundspike")
		inst:AddTag("object")
        inst:AddTag("stone")
		
        inst.AnimState:PlayAnimation("attack5")
		
        if inst.sg.statemem.attacktarget ~= nil then
            if inst.sg.statemem.attacktarget:IsValid() then
                inst:ForceFacePoint(inst.sg.statemem.attacktarget:GetPosition())
				inst.targetx, inst.targety, inst.targetz = inst.sg.statemem.attacktarget.Transform:GetWorldPosition()
            end
        end
	end,
	
	onexit = function(inst)
		inst:RemoveTag("groundspike")
		inst:RemoveTag("object")
        inst:RemoveTag("stone")
		inst.Transform:SetFourFaced()
		inst:DoTaskInTime(0.25, function(inst) inst.is_doing_special = nil end) --Leo: Give a little bit of time after the state ends before setting it to nil.
		inst.targetx = nil
		inst.targety = nil
		inst.targetz = nil
		inst.components.combat:SetRange(3.5, 5)
		if inst.is_enraged == true then
			inst:DoTaskInTime(7, function(inst) 
				inst.altattack = true 
				inst.components.combat:SetRange(10, 5)
			end)
		else
			inst:DoTaskInTime(15, function(inst) 
				inst.altattack = true 
				inst.components.combat:SetRange(10, 5)
			end)
		end
    end,
	
    timeline=
		{

			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5")
			
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")	
			end),
            --TimeEvent(10*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, {"LA_mob", "battlestandard", "shadow", "INLIMBO", "playerghost"})  end),
			TimeEvent(15*FRAMES, ShakeIfClose),
			TimeEvent(16*FRAMES, function(inst) 
				inst.sg:AddStateTag("nocancel")
				if inst.sg.statemem.attacktarget then
				
				inst.targetx, inst.targety, inst.targetz = inst.sg.statemem.attacktarget.Transform:GetWorldPosition()
				end
				if inst.targetx and inst.targetz then	
					inst:FacePoint(inst.targetx, 0, inst.targetz)
					inst:DoTrailp(inst.targetx, inst.targetz)
				end	
			end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5_fire_1") end),
			TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5_fire_2") end),
			TimeEvent(37*FRAMES, ShakeIfClose),
			TimeEvent(37*FRAMES, function(inst) 
				--inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, {"LA_mob", "battlestandard", "shadow", "INLIMBO", "playerghost"}) 
				if inst.targetx and inst.targetz then	 
					inst:DoTrailp(inst.targetx, inst.targetz, true)
				end	
				inst.sg:RemoveStateTag("nocancel")
			end),
		},

        events=
        {
            EventHandler("animqueueover", function(inst) 
				--inst.components.combat:TryRetarget() --who put this here?
				inst.sg:GoToState("idle")
			end),
        },
    },	
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/hit")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt")	
			end),
			TimeEvent(15*FRAMES, ShakeRoar)
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.taunt2 = false
            inst.AnimState:PlayAnimation("attack4")
			inst.components.combat:StartAttack()
			--inst.components.combat.areahitdamagepercent = 0.8
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/spin") end),
			TimeEvent(13*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, GetExcludeTags(inst)) end),
			--TimeEvent(17*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
			--TimeEvent(26*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
			TimeEvent(31*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, GetExcludeTags(inst)) end), 
			--TimeEvent(41*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
		end,
		
        events=
        {
			EventHandler("onhitother", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe") end),
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death")
			inst.sg.statemem.wants_to_banner = nil
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/death")
            inst.AnimState:PlayAnimation("death2")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline=
        {
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(50*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(55*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death_bodyfall") end),
			TimeEvent(70*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2") end),
			TimeEvent(55*FRAMES, ShakeRoar),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				local hungerpercent = inst.components.hunger:GetPercent()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/sleep_out") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(0*FRAMES, ShakeIfClose),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(20*FRAMES, ShakeIfClose),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)          
        end,
		
		timeline=
        {
			TimeEvent(0*FRAMES, ShakeIfClose),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "dash",
        tags = { "nointerrupt", "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("dash")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")		
        end,
		
		onexit = function(inst)
			inst.ischarging = nil
		end,
		
		timeline = 
		{
			TimeEvent(2*FRAMES, function(inst) inst.components.locomotor:RunForward() 
			--inst.components.locomotor.runspeed = CalcDashSpeed(inst, inst.components.combat.target) 
			end),  
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")  
			end),
			TimeEvent(13*FRAMES, ShakeIfClose),
		},
		
        events =
        {
            EventHandler("animover", function(inst) 
				inst.components.locomotor:StopMoving() 
				if inst.components.combat.target then
					inst.sg:GoToState("attack_special", nil, inst.components.combat.target) 
				else
					inst.sg:GoToState("idle")
				end
			end ),
        },
    },
	
	--Fossilized States--
	State
    {
        name = "fossilized",
        tags = { "busy", "fossilized", "caninterrupt" },

        onenter = function(inst, data)
            --ClearStatusAilments(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("fossilized")
			inst.AnimState:PushAnimation("fossilized_loop", true)
            inst.components.fossilizable:OnFossilize(1)
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_2") end),
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(55*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(77*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(88*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(99*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(110*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(121*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
		},

        events =
        {
            EventHandler("fossilize", function(inst, data)
                inst.components.fossilizable:OnExtend(1)
            end),
            EventHandler("unfossilize", function(inst)
                inst.sg.statemem.unfossilizing = true
                inst.sg:GoToState("unfossilizing")
            end),
        },

        onexit = function(inst)
            inst.components.fossilizable:OnUnfossilize()
            if not inst.sg.statemem.unfossilizing then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
        end,
    },
	

	
	State
    {
        name = "unfossilizing",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_shake")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.unfossilized = true
                    inst.sg:GoToState("unfossilized")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.unfossilized then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
            inst.SoundEmitter:KillSound("shakeloop")
        end,
    },
	
	State
    {
        name = "unfossilized",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_pst")
            inst.components.fossilizable:OnSpawnFX()
        end,

        timeline = 
		{ 
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					AttemptNewTarget(inst)
					inst.sg:GoToState("idle")
					
                end
            end),
        },
    },
	
	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")	
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(50*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(55*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death_bodyfall") end),
			TimeEvent(70*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2") end),
			TimeEvent(55*FRAMES, ShakeRoar),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.components.health:SetPercent(0.5) end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt")	
			end),
			TimeEvent(15*FRAMES, ShakeRoar)
		},
	
	},
	--anims = 
	{
		corpse = "death2",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
--CommonStates.AddFrozenStates(states)

return StateGraph("boarriorp", states, events, "idle", actionhandlers)

