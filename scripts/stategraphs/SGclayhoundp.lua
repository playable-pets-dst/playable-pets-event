require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "eat"
local shortaction = "eat"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function PlayClayShakeSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/stone_shake", nil, .6)
end

local function PlayClayFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/footstep_hound")
end

local function StartAura(inst)

end

local function StopAura(inst)

end

local function ShowEyeFX(inst)
    if inst._eyeflames ~= nil then
        inst._eyeflames:set(true)
    end
end

local function HideEyeFX(inst)
    if inst._eyeflames ~= nil then
        inst._eyeflames:set(false)
    end
end

local function MakeStatue(inst)

end

local function MakeReanimated(inst)

end

local function FossilizeOthers(inst)
	local pos = inst:GetPosition()
	inst.components.fossilizer:Fossilize(pos, inst)
	inst.taunt = false
	inst:DoTaskInTime(PPEV_FORGE.CLAYHOUND.SPECIAL_CD, function(inst) inst.taunt = true end)
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				inst.sg:GoToState("taunt")
				
            
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OpenGift(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.SoundEmitter:PlaySound(inst.sounds.pant)
            inst.Physics:Stop()
            --if playanim then
                --inst.AnimState:PlayAnimation(playanim)
                inst.AnimState:PushAnimation("idle", true)
            --else
                --inst.AnimState:PlayAnimation("idle", true)
            --end
            inst.sg:SetTimeout(2*math.random()+.5)
        end,

    },
	
	State{
		name = "bark",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			--inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("howl")
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.howl) end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "special_atk2",
        tags = {"idle"},

        onenter = function(inst)
			if inst.scared_task == nil then
			inst.scared_task = true
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("scared_pre")
            inst.AnimState:PushAnimation("scared_loop", true)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/hit")
            --inst.sg:SetTimeout(.8 + .3 * math.random())
			elseif inst.scared_task ~= nil and inst.scared_task == true then
				inst.scared_task = nil
				--inst.sg:GoToState("idle", "scared_pst")
			else
				inst.sg:GoToState("idle")
			end
        end,

        onexit = function(inst)
            inst.scared_task = nil
        end,
    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("atk_pre")
        inst.AnimState:PushAnimation("atk", false)
		inst.AnimState:PushAnimation("atk_pst", false)
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
    timeline=
    {

			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(16*FRAMES, function(inst) PlayablePets.DoWork(inst, 2.5) end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "eat",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)
			inst.AnimState:PushAnimation("atk_pst", false)
			
        end,

		timeline=
        {
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bite) end),
			TimeEvent(14*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events=
        {
			--EventHandler("animqueueover", function(inst) if inst:PerformBufferedAction() then inst.components.combat:SetTarget(nil)  inst.sg:GoToState("idle", "atk_pst") end end),
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") ShowEyeFX(inst) end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("howl")
			inst.SoundEmitter:PlaySound(inst.sounds.howl)
        end,

		timeline=
        {
			TimeEvent(13*FRAMES, function(inst) 
				if TheNet:GetServerGameMode() == "lavaarena" and inst.components.fossilizer then
					FossilizeOthers(inst)
				end				
			end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "taunt",
        tags = { "busy", "howling" },

        onenter = function(inst, count)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("howl")
        end,

        timeline =
        {
            TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.howl) end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                    inst.sg:GoToState("idle")
            end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.sg.statemem.clay = true
            HideEyeFX(inst)
            inst.SoundEmitter:PlaySound("dontstarve/common/destroy_pot")

            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline =
        {
            TimeEvent(TUNING.GARGOYLE_REANIMATE_DELAY, function(inst)
                if not inst:IsInLimbo() then
                    inst.AnimState:Resume()
                end
            end),
            TimeEvent(11 * FRAMES, function(inst)
                if inst.sg.statemem.clay then
                    PlayClayFootstep(inst)
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            MakeStatue(inst)
            HideEyeFX(inst)
            StopAura(inst)
            inst.AnimState:PlayAnimation("statue_pre")
        end,
		
		timeline =
        {
            TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, PlayClayShakeSound),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(9 * FRAMES, PlayClayFootstep),
            TimeEvent(10 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                HideEyeFX(inst)
            end),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, PlayClayShakeSound),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
        },

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("idle_statue")
				inst.sg:SetTimeout(1.5)
				
			end,
			
		ontimeout = function(inst)
			inst.sg:GoToState("sleeping")
		end,	

        events =
        {
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            MakeStatue(inst)
            ShowEyeFX(inst)
            StartAura(inst)
            inst.AnimState:PlayAnimation("statue_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,
		
		timeline =
        {
            TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, PlayClayShakeSound),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(10 * FRAMES, PlayClayShakeSound),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                PlayClayFootstep(inst)
            end),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
        },

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	 State
    {
        name = "reanimatestatue",
        tags = { "busy", "noattack", "statue" },

        onenter = function(inst, target)
            MakeStatue(inst)
            ShowEyeFX(inst)
            StartAura(inst)
            inst.Transform:SetSixFaced()
            inst.AnimState:PlayAnimation("statue_pst")
            inst.sg.statemem.target = target
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, PlayClayShakeSound),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(10 * FRAMES, PlayClayShakeSound),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                PlayClayFootstep(inst)
            end),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            MakeReanimated(inst)
            if inst.sg.statemem.target ~= nil then
                inst.components.combat:SetTarget(inst.sg.statemem.target)
            end
            inst.Transform:SetFourFaced()
        end,
    },
	
	
}


--[[CommonStates.AddSleepStates(states,
{
	sleeptimeline = {
        TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep") end),
	},
})

]]
CommonStates.AddRunStates(states,
{
	runtimeline = {
		TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.growl) end),
		TimeEvent(0*FRAMES, PlayClayFootstep ),
		TimeEvent(4*FRAMES, PlayClayFootstep ),
	},
})

CommonStates.AddFrozenStates(states)

CommonStates.AddSimpleActionState(states,"pickup", "attack", 14*FRAMES, {"busy"})

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				HideEyeFX(inst)
				inst.SoundEmitter:PlaySound("dontstarve/common/destroy_pot")
			end),
			TimeEvent(TUNING.GARGOYLE_REANIMATE_DELAY, function(inst)
                if not inst:IsInLimbo() then
                    inst.AnimState:Resume()
                end
            end),
            TimeEvent(11 * FRAMES, function(inst)
                if inst.sg.statemem.clay then
                    PlayClayFootstep(inst)
                end
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				MakeStatue(inst)
				ShowEyeFX(inst)
				StartAura(inst)
			end),
			TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, PlayClayShakeSound),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(10 * FRAMES, PlayClayShakeSound),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                PlayClayFootstep(inst)
            end),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "statue_pst"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle")
local simpleanim = "run_pst"
local simpleidle = "idle"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("clayhoundp", states, events, "reanimatestatue", actionhandlers)

