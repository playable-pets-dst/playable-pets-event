require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "attack"
local shortaction = "action"
local workaction = "attack"
local otheraction = "attack"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SpawnHound(inst)
end


local function PlayClayShakeSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/stone_shake")
end

local function PlayClayFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/footstep")
end

local function ShowEyeFX(inst)
    if inst._eyeflames ~= nil then
        inst._eyeflames:set(true)
    end
end

local function HideEyeFX(inst)
    if inst._eyeflames ~= nil then
        inst._eyeflames:set(false)
    end
end

local function PlayClayShakeSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/stone_shake")
end

local function PlayClayFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/clayhound/footstep")
end

local function FossilizeOthers(inst)
	local pos = inst:GetPosition()
	inst.components.fossilizer:Fossilize(pos, inst)
	inst.taunt = false
	inst:DoTaskInTime(PPEV_FORGE.CLAYHOUND.SPECIAL_CD, function(inst) inst.taunt = true end)
end

local function MakeStatue(inst)
    if not inst.sg.mem.statue then
        inst.sg.mem.statue = true
        local x, y, z = inst.Transform:GetWorldPosition()
        inst.Physics:Stop()
        ChangeToObstaclePhysics(inst)
        inst.Physics:Teleport(x, 0, z)
        inst:AddTag("notarget")
        inst.components.health:SetInvincible(true)

        --Snap to nearest 45 degrees + 15 degree offset for better facing update during camera rotation
        inst.Transform:SetRotation(math.floor(inst.Transform:GetRotation() / 45 + .5) * 45 + 15)

        inst:OnBecameStatue()
    end
end

local function MakeReanimated(inst)
    if inst.sg.mem.statue then
        inst.sg.mem.statue = nil
        local x, y, z = inst.Transform:GetWorldPosition()
        inst.Physics:SetMass(1000)
        ChangeToCharacterPhysics(inst)
        inst.Physics:Teleport(x, 0, z)
        inst:RemoveTag("notarget")
        inst.components.health:SetInvincible(false)

        inst:OnReanimated()
    end
end


local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping")) and
            (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() then
				inst.sg:GoToState("hit")
        end
    end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
	EventHandler("heardwhistle", function(inst, data)
        if not inst.sg:HasStateTag("statue") then
            if inst.components.combat:TargetIs(data.musician) then
                inst.components.combat:SetTarget(nil)
				inst.sg:GoToState("force_transformstatue")
            end
        end
    end),

    --Clay warg
    EventHandler("becomestatue", function(inst)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            inst.sg:GoToState("transformstatue")
        end
    end),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{
	
	State
	{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop")
			--inst.SoundEmitter:PlaySound(inst.sounds.idle)
		end,

		events = 
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end)
		},
	},
	
	 State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(12*FRAMES, function(inst) PlayablePets.DoWork(inst, 5) end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        
        name = "action",
		tags = {"busy"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("run_pst")
        end,
        
		timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },
		
		events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
	},	
	
	State{
        name = "hit",
        tags = {"busy", "hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hit) end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
	{
		name = "special_atk1", 
		tags = {"busy"},

		onenter = function(inst)
			--if inst.taunt == false then
			--inst.sg:GoToState("idle")
			--end
			--inst.taunt = false
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("howl")
			inst.SoundEmitter:PlaySound(inst.sounds.howl)
			if TheNet:GetServerGameMode() == "lavaarena" then
				FossilizeOthers(inst)
			end
		end,

		timeline = 
		{
			--TimeEvent(10*FRAMES, SpawnClayHound),
		},

		events = 
		{
			EventHandler("animover", function(inst) 
				--inst:DoTaskInTime(20, function() inst.taunt = true end)
				inst.sg:GoToState("idle") 
			end)
		},
	},
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("idle_loop", true)
            --inst.AnimState:PushAnimation("idle_loop", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("idle_loop", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("idle_loop")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
                inst.sg:GoToState("idle", true)
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },

	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            HideEyeFX(inst)
            inst.AnimState:PlayAnimation("statue_pre")
        end,
		
		timeline =
        {
            TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                PlayClayFootstep(inst)
            end),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(10 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                HideEyeFX(inst)
            end),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, PlayClayShakeSound),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
            TimeEvent(18 * FRAMES, PlayClayShakeSound),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy"},

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				local hungerpercent = inst.components.hunger:GetPercent()
				if hungerpercent ~= nil and hungerpercent ~= 0 then -- We don't want players to heal out starvation.
				inst.components.health:DoDelta(20, false)
				end
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("statue")
				inst.sg:SetTimeout(1.5)
			end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("sleeping")
		end,

        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            ShowEyeFX(inst)
			inst.SoundEmitter:PlaySound("dontstarve/music/clay_resurrection")
            inst.AnimState:PlayAnimation("statue_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline =
        {
            TimeEvent(1 * FRAMES, PlayClayShakeSound),
            TimeEvent(3 * FRAMES, PlayClayShakeSound),
            TimeEvent(5 * FRAMES, PlayClayShakeSound),
            TimeEvent(7 * FRAMES, PlayClayShakeSound),
            TimeEvent(21 * FRAMES, PlayClayShakeSound),
            TimeEvent(23 * FRAMES, PlayClayShakeSound),
            TimeEvent(25 * FRAMES, PlayClayShakeSound),
            TimeEvent(29 * FRAMES, PlayClayShakeSound),
            TimeEvent(32 * FRAMES, PlayClayShakeSound),
            TimeEvent(34 * FRAMES, PlayClayShakeSound),
            TimeEvent(36 * FRAMES, PlayClayShakeSound),
            TimeEvent(38 * FRAMES, PlayClayShakeSound),
            TimeEvent(39 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(0 * FRAMES, function(inst)
				if inst:HasTag("clay") then
					inst.sg.statemem.clay = true
					HideEyeFX(inst)
					inst.SoundEmitter:PlaySound("dontstarve/common/destroy_pot")
					inst.SoundEmitter:PlaySoundWithParams("dontstarve/creatures/together/antlion/sfx/ground_break", { size = .1 })
				end
				inst.SoundEmitter:PlaySound(inst.sounds.death)
			end),
			TimeEvent(4 * FRAMES, function(inst)
				if inst.sg.statemem.clay then
					PlayClayFootstep(inst)
				end
			end),
			TimeEvent(6 * FRAMES, function(inst)
				if inst.sg.statemem.clay then
					PlayClayFootstep(inst)
				end
			end),
		},
		
        events =
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State
    {
        name = "statue",
        tags = { "busy", "nointerrupt", "statue" },

        onenter = function(inst)
            MakeStatue(inst)
            HideEyeFX(inst)
            inst.AnimState:PlayAnimation("statue")
			inst.sg:SetTimeout(10)
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("reanimatestatue")		
		end,

        onexit = function(inst)
            if not inst.sg.statemem.statue then
                ShowEyeFX(inst)
            end
        end,
    },

    State
    {
        name = "reanimatestatue",
        tags = { "busy", "noattack", "statue" },

        onenter = function(inst, target)
            ShowEyeFX(inst)
            inst.AnimState:PlayAnimation("statue_pst")
            inst.SoundEmitter:PlaySound("dontstarve/music/clay_resurrection")
            inst.sg.statemem.target = target
        end,

        timeline =
        {
            TimeEvent(1 * FRAMES, PlayClayShakeSound),
            TimeEvent(3 * FRAMES, PlayClayShakeSound),
            TimeEvent(5 * FRAMES, PlayClayShakeSound),
            TimeEvent(7 * FRAMES, PlayClayShakeSound),
            TimeEvent(21 * FRAMES, PlayClayShakeSound),
            TimeEvent(23 * FRAMES, PlayClayShakeSound),
            TimeEvent(25 * FRAMES, PlayClayShakeSound),
            TimeEvent(29 * FRAMES, PlayClayShakeSound),
            TimeEvent(32 * FRAMES, PlayClayShakeSound),
            TimeEvent(34 * FRAMES, PlayClayShakeSound),
            TimeEvent(36 * FRAMES, PlayClayShakeSound),
            TimeEvent(38 * FRAMES, PlayClayShakeSound),
            TimeEvent(39 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State
    {
        name = "force_transformstatue",
        tags = { "busy", "noattack", "statue" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("statue_pre")
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, PlayClayShakeSound),
            TimeEvent(4 * FRAMES, PlayClayShakeSound),
            TimeEvent(6 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                PlayClayFootstep(inst)
            end),
            TimeEvent(8 * FRAMES, PlayClayShakeSound),
            TimeEvent(10 * FRAMES, function(inst)
                PlayClayShakeSound(inst)
                HideEyeFX(inst)
            end),
            TimeEvent(12 * FRAMES, PlayClayShakeSound),
            TimeEvent(14 * FRAMES, PlayClayShakeSound),
            TimeEvent(16 * FRAMES, PlayClayShakeSound),
            TimeEvent(18 * FRAMES, PlayClayShakeSound),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.statue = true
                    inst.sg:GoToState("statue")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.statue then
                ShowEyeFX(inst)
            end
        end,
    },
	
}
  
CommonStates.AddRunStates(states, 
{	
	starttimeline = {},
    runtimeline = 
    { 
        TimeEvent(5*FRAMES, 
        function(inst) 
        	PlayFootstep(inst)
        	inst.SoundEmitter:PlaySound(inst.sounds.idle)
        end),
    },
	endtimeline = {},
})
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle_loop", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0 * FRAMES, function(inst)
				if inst:HasTag("clay") then
					inst.sg.statemem.clay = true
					HideEyeFX(inst)
					inst.SoundEmitter:PlaySound("dontstarve/common/destroy_pot")
					inst.SoundEmitter:PlaySoundWithParams("dontstarve/creatures/together/antlion/sfx/ground_break", { size = .1 })
				end
				inst.SoundEmitter:PlaySound(inst.sounds.death)
			end),
			TimeEvent(4 * FRAMES, function(inst)
				if inst.sg.statemem.clay then
					PlayClayFootstep(inst)
				end
			end),
			TimeEvent(6 * FRAMES, function(inst)
				if inst.sg.statemem.clay then
					PlayClayFootstep(inst)
				end
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				ShowEyeFX(inst)
				inst.SoundEmitter:PlaySound("dontstarve/music/clay_resurrection")
			end),
			TimeEvent(1 * FRAMES, PlayClayShakeSound),
            TimeEvent(3 * FRAMES, PlayClayShakeSound),
            TimeEvent(5 * FRAMES, PlayClayShakeSound),
            TimeEvent(7 * FRAMES, PlayClayShakeSound),
            TimeEvent(21 * FRAMES, PlayClayShakeSound),
            TimeEvent(23 * FRAMES, PlayClayShakeSound),
            TimeEvent(25 * FRAMES, PlayClayShakeSound),
            TimeEvent(29 * FRAMES, PlayClayShakeSound),
            TimeEvent(32 * FRAMES, PlayClayShakeSound),
            TimeEvent(34 * FRAMES, PlayClayShakeSound),
            TimeEvent(36 * FRAMES, PlayClayShakeSound),
            TimeEvent(38 * FRAMES, PlayClayShakeSound),
            TimeEvent(39 * FRAMES, PlayClayShakeSound),
            TimeEvent(41 * FRAMES, PlayClayFootstep),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "statue_pst"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    

return StateGraph("clayvargp", states, events, "reanimatestatue", actionhandlers)

