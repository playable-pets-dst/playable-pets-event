require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	--no hit animation, so no attacked handler.
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
	EventHandler("ontalk", function(inst, data)
        if not inst.sg:HasStateTag("busy") then
	        inst.sg:GoToState(math.random() < 0.1 and "announce" or "talk")
        end
    end),
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

--TODO make this a common fn
local function ToggleHide(inst, hide)
	if hide then
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" and not TheWorld:HasTag("cave") then
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end
		inst.noactions = true
		inst:AddTag("notarget")
		inst:AddTag("noplayerindicator")
		inst.components.health:SetInvincible(true)
		inst.components.talker:IgnoreAll("hiding")
		inst.MiniMapEntity:SetIcon("")
		inst.components.locomotor:SetExternalSpeedMultiplier(inst, 2, 2)
		inst:Hide()
	else
		inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
		inst.Physics:CollidesWith(COLLISION.LIMITS)
		inst.noactions = false
		inst.components.health:SetInvincible(false)
		inst:RemoveTag("notarget")
		inst:RemoveTag("noplayerindicator")
		inst.MiniMapEntity:SetIcon(inst.mob_table.minimap)
		inst.components.talker:StopIgnoringAll("hiding")
		inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 2)
		inst:Show()
	end
end

local states=
{
    State
     {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.components.locomotor:StopMoving()
			if playanim and type(playanim) == "string" then
				inst.AnimState:PlayAnimation(playanim)
			else
				inst.AnimState:PlayAnimation("idle")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "land",
        tags = {"flight", "busy"},
        onenter= function(inst)
			local pos = inst:GetPosition()
			if inst._isflying and not PlayablePets.IsAboveLand(pos) then
				inst._cannot_land = true
				inst.sg:GoToState("idle")
			else
				local x, y, z = inst.Transform:GetWorldPosition()
				inst.Transform:SetPosition(x, 15, z)
				ToggleHide(inst, false)
				inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
				inst._isflying = false
				inst.taunt2 = false
				inst.AnimState:PlayAnimation("fly_loop", true)
				inst.DynamicShadow:Enable(false)
				inst.Physics:SetMotorVelOverride(0,-10,0)
			end
        end,

        onupdate= function(inst)
            inst.Physics:SetMotorVelOverride(0,-10,0)
            local x, y, z = inst.Transform:GetWorldPosition()
            if y <= .1 or inst:IsAsleep() then
                inst.Physics:ClearMotorVelOverride()
                inst.Physics:Stop()
                inst.Physics:Teleport(x, 0, z)
                inst.DynamicShadow:Enable(true)
                inst.sg:GoToState("idle", "land")
            end
        end,

        onexit = function(inst)
			if not inst._cannot_land then
				local x, y, z = inst.Transform:GetWorldPosition()
				if y > 0 then
					inst.Transform:SetPosition(x, 0, z)
				end
				inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
				inst._isflying = false
				inst.taunt2 = false
				if inst.taunt2_task then
					inst.taunt2_task:Cancel()
					inst.taunt2_task = nil
				end
				inst.taunt2_task = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			else
				inst._cannot_land = nil
			end            
        end,
    },

   State{
        name = "talk",
        tags = {"canrotate", "talking"},

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				inst.components.locomotor:Stop()
				inst.AnimState:PlayAnimation("dialog"..math.random(3))
			end
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/characters/corvus/speak", "talk") end),
            TimeEvent(50*FRAMES, function(inst) inst.SoundEmitter:KillSound("talk") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

		onexit = function(inst)
	        inst.SoundEmitter:KillSound("talk")
		end,
    },

    State{
        name = "announce",
        tags = {"canrotate", "talking"},

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				inst.components.locomotor:Stop()
				inst.AnimState:PlayAnimation("welcome")
			end
        end,

        timeline =
        {
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/characters/corvus/speak", "talk") end),
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:KillSound("talk") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

		onexit = function(inst)
	        inst.SoundEmitter:KillSound("talk")
		end,
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("land")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
        name = "special_atk2",
        tags = {"flight", "busy", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.sg:SetTimeout(0.5)

            inst.DynamicShadow:Enable(false)

            inst.AnimState:PlayAnimation("takeoff_pre")
            inst.AnimState:PushAnimation("fly_loop")
        end,

        ontimeout = function(inst)
            inst.Physics:SetMotorVel(math.random() * 4 - 2, math.random() * 5 + 10, math.random() * 4 - 2)
        end,
		
		onexit = function(inst)
			inst._isflying = true
			ToggleHide(inst, true)
			local x, y, z = inst.Transform:GetWorldPosition()
			inst.Physics:ClearMotorVelOverride()
            inst.Physics:Stop()
            inst.Physics:Teleport(x, 0, z)
		end,

        timeline =
        {
            TimeEvent(2, function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				inst.AnimState:PlayAnimation("give", false)
				inst.AnimState:PushAnimation("give_pst", false)
				inst.components.combat:StartAttack()
				
				inst.Physics:Stop()
			end
        end,
		
		timeline = {
			TimeEvent(11*FRAMES, function(inst) inst:PerformBufferedAction() end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
                end
            end),
        },

    },
	
}

CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
        TimeEvent(0, function(inst) inst.components.locomotor:WalkForward() end),
    },

    walktimeline =
    {
        TimeEvent(0, function(inst) 
			if not inst._isflying then 
				PlayFootstep(inst)
			end
		end),
        TimeEvent(0 * FRAMES, function(inst) inst.components.locomotor:RunForward() end),
        TimeEvent(7 * FRAMES, function(inst) inst.components.locomotor:WalkForward()  end),
        TimeEvent(12 * FRAMES, function(inst) inst.components.locomotor:RunForward() end),
        TimeEvent(18 * FRAMES, function(inst) inst.components.locomotor:WalkForward() end),
        TimeEvent(12, function(inst) 
			if not inst._isflying then 
				PlayFootstep(inst)
			end
		end),
    },
})

local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "land") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "land",
		corpse_taunt = "welcome"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("molebatp", states, events, "idle", actionhandlers)

