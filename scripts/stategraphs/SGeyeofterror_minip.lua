require("stategraphs/commonstates")
require("stategraphs/ppstates")
require("stategraphs/SGcritter_common")

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function return_to_idle(inst)
    inst.sg:GoToState("idle")
end

local function lower_flying_creature(inst)
    inst:RemoveTag("flying")
    inst:PushEvent("on_landed")
end

local function raise_flying_creature(inst)
    inst:AddTag("flying")
    inst:PushEvent("on_no_longer_landed")
end

local sound_path = "terraria1/mini_eyeofterror/"

local emote_data = {
    emoteXL_angry = "combat_pre",
    emoteXL_sad = "hungry",
    emoteXL_happycheer = "emote_pet",
    emoteXL_strikepose = "nuzzle"
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	EventHandler("emote", function(inst, data) --TODO: Make a common Eventhandler for this.
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState((emote_data and data.anim) and emote_data[data.anim] or "special_atk2", data)
        end
    end),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{

   State {
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
        end,

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },

    State {
        name = "taunt",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            TimeEvent(21*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/taunt")
            end),
        },

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },

    State {
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            TimeEvent(21*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/taunt")
            end),
        },

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },

    State {
        name = "special_atk2",
        tags = { "busy" },

        onenter = function(inst)
            inst.sg:GoToState("emote_"..math.random(2))
        end,
    },

    State {
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },

    State {
        name = "attack",
        tags = {"attack", "charge", "busy"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)

            inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/atk_pre")

            inst.components.combat:StartAttack()

            inst.sg.statemem.target = target
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.Physics:SetMotorVelOverride(4, 0, 0)
                inst.sg.statemem._motorvel_set = true
            end),
            TimeEvent(14*FRAMES, function(inst)
                local target = (
                        inst.sg.statemem.target ~= nil
                        and inst.sg.statemem.target:IsValid()
                        and inst.sg.statemem.target
                    )
                    or nil -- NOTE: nil falls through to combat.target
                PlayablePets.DoWork(inst, 2)
            end),
            TimeEvent(22*FRAMES, function(inst)
                inst.Physics:ClearMotorVelOverride()
                inst.sg.statemem._motorvel_set = false

                inst.components.locomotor:Stop()
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                return_to_idle(inst)
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem._motorvel_set then
                inst.Physics:ClearMotorVelOverride()
            end
        end,
    },

    State {
        name = "work",
        tags = {"attack", "charge", "busy"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)

            inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/atk_pre")

            inst.components.combat:StartAttack()

            inst.sg.statemem.target = target
        end,

        timeline =
        {
            TimeEvent(14*FRAMES, function(inst)
                local target = (
                        inst.sg.statemem.target ~= nil
                        and inst.sg.statemem.target:IsValid()
                        and inst.sg.statemem.target
                    )
                    or nil -- NOTE: nil falls through to combat.target
                PlayablePets.DoWork(inst, 2)
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                return_to_idle(inst)
            end),
        },
    },

    State {
        name = "eat",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("eat_pre")
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)

            inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/eat_pre")
        end,

        timeline =
        {
            TimeEvent(22*FRAMES, function(inst)
                inst:PerformBufferedAction()
                lower_flying_creature(inst)
            end),
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/eat_lp", "eat_loop")
            end),
            TimeEvent(58*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("eat_loop")
                inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/eat_pst")
            end),
            TimeEvent(84*FRAMES, raise_flying_creature),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                return_to_idle(inst)
            end),
        },

        onexit = function(inst)
            -- In case we got interrupted somehow
            inst:ClearBufferedAction()

            raise_flying_creature(inst)
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline = {
			TimeEvent(31*FRAMES, lower_flying_creature),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/sleep_pre")
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        timeline = {
            TimeEvent(36*FRAMES, lower_flying_creature),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
            raise_flying_creature(inst)
		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline = {
            TimeEvent(22*FRAMES, raise_flying_creature),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

local appear_tags = { "busy" }
local appear_timeline =
{
    TimeEvent(19*FRAMES, raise_flying_creature),
    TimeEvent(43*FRAMES, function(inst)
        inst.SoundEmitter:PlaySound("terraria1/mini_eyeofterror/appear")
    end),
    TimeEvent(93*FRAMES, function(inst)
        inst.sg:RemoveStateTag("busy")
    end),
}
CommonStates.AddSimpleState(states, "appear", "appear", appear_tags, "idle", appear_timeline)
SGCritterStates.AddEmote(states, "cute",
{
    TimeEvent(0, function(inst)
        inst.SoundEmitter:PlaySound(sound_path .. "emote_cute")
    end),
})

local emotes =
{
    {
        anim = "emote1",
        timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound(sound_path .. "emote1")
            end),
        },
    },
    {
        anim = "emote2",
        timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound(sound_path .. "emote2")
            end),
            TimeEvent(11*FRAMES, lower_flying_creature),
            TimeEvent(71*FRAMES, raise_flying_creature),
        },
    },
}
SGCritterStates.AddRandomEmotes(states, emotes)

SGCritterStates.AddPetEmote(states,
{
    TimeEvent(0, function(inst)
        inst.SoundEmitter:PlaySound(sound_path .. "emote_pet")
    end),
    TimeEvent(5*FRAMES, lower_flying_creature),
    TimeEvent(33*FRAMES, raise_flying_creature),
},
raise_flying_creature)

SGCritterStates.AddCombatEmote(states,
{
    pre =
    {
        TimeEvent(0, function(inst)
            inst.SoundEmitter:PlaySound(sound_path .. "emote_combat")
        end),
    },
})
SGCritterStates.AddPlayWithOtherCritter(states, events,
{
    active =
    {
        TimeEvent(0, function(inst)
            inst.SoundEmitter:PlaySound(sound_path .. "interact_active")
        end),
    },
    passive =
    {
        TimeEvent(0, function(inst)
            inst.SoundEmitter:PlaySound(sound_path .. "interact_active")
        end),
    },
})
CommonStates.AddWalkStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
return StateGraph("eyeofterror_minip", states, events, "idle", actionhandlers)

