require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "chomp_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if target:HasTag("gingerbread_pig") or target.prefab == "gingerbreadpig" then
			return "eat_pre"
		else
			return "attack"
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"LA_mob", "boarrior_pet", "notarget", "INLIMBO", "shadow", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

 local states=
{

     State
    {
        name = "idle",
        tags = { "idle" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
            if inst.sounds then
                inst.SoundEmitter:PlaySound(inst.sounds.idle)
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

	State{
        name = "hit",
        tags = {"busy", "hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hit) end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State
    {
        name = "howl",
        tags = { "busy", "howling" },

        onenter = function(inst, count)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("howl")
            inst.SoundEmitter:PlaySound(inst.sounds.howl)
            inst.sg.statemem.count = count
        end,

        timeline =
        {
            TimeEvent(10 * FRAMES, function(inst)
                if inst.sg.statemem.count == nil then
                    SpawnHound(inst)
                end
            end),
        },

        events =
        {
            EventHandler("heardwhistle", function(inst)
                inst.sg.statemem.count = 2
            end),
            EventHandler("animover", function(inst)
                if inst.sg.statemem.count ~= nil and inst.sg.statemem.count > 1 then
                    inst.sg:GoToState("howl", inst.sg.statemem.count - 1)
                else
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
		name = "chomp_pre",
        tags = { "chewing", "busy" },
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("eat_pre")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/attack")

			local buffaction = inst:GetBufferedAction()
			local target = buffaction.target
			if target ~= nil and target:IsValid() then
				inst.components.combat:StartAttack()
				inst:ForceFacePoint(target.Transform:GetWorldPosition())
			end
		end,

        timeline =
		{

		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("chomp_loop")
				end
			end),
		},
	},

	State{
		name = "chomp_loop",
		tags = { "chewing", "busy" },

		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_loop")
			inst.SoundEmitter:PlaySound("rifts3/chewing/warg")
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				inst.sg:AddStateTag("caninterrupt")
				inst.sg:AddStateTag("wantstoeat")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("chomp_pst", true)
				end
			end),
		},
	},

	State{
		name = "chomp_pst",
		tags = { "busy", "caninterrupt" },

		onenter = function(inst, chewing)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_pst")
            PlayablePets.DoWork(inst, 2.5)
		end,

		timeline =
		{
			FrameEvent(6, function(inst)

			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.SoundEmitter:KillSound("loop")
		end,
	},
	
	State
    {
        name = "eat_pre",
        tags = { "busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("gingerbread_eat_pre")            
        end,

        timeline =
        {
            TimeEvent(10 * FRAMES, function(inst)
				local killchance = math.random(1,10)
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target and (target:HasTag("gingerbread_pig") or target.prefab == "gingerbreadpig") then
					if killchance < 7 or not target:HasTag("player")then
					inst.components.combat.defaultdamage = 9000
					inst.components.combat.hit_range = 3.5
					end
				end
				inst:PerformBufferedAction()
				if killchance >= 7 and target and target.Cripple and target:HasTag("player") then
					target.Cripple(target, true)
					if not target.components.health:IsDead() then
						target.components.health.currenthealth = 1
					end
				end
            end),
        },
		
		onexit = function(inst)
			inst.components.combat.defaultdamage = 65*2
			inst.components.combat.hit_range = 5
		end,

        events =
        {
            EventHandler("animover", function(inst)
                    inst.sg:GoToState("eat")
            end),
        },
    },
	
	State
    {
        name = "eat",
        tags = { "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("gingerbread_eat_loop")
			inst.AnimState:PushAnimation("gingerbread_eat_pst", false)
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
            TimeEvent(6*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbreadpig/vocal") end end),
            TimeEvent(12*FRAMES, function(inst) if math.random() < 0.7 then inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/idle") end end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
				if inst.AnimState:AnimDone() then
				   inst.sg:GoToState("idle")
			    end
			end),
        },
    },

	--Gingerbread warg
    State
    {
        name = "special_atk2",
        tags = { "attack", "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("attack_icing")
            inst.components.combat:StartAttack()
        end,

        timeline = 
		{
            TimeEvent(14 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
            TimeEvent(17 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
            TimeEvent(26 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
            TimeEvent(33 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
            TimeEvent(42 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(42*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
            TimeEvent(49 * FRAMES, function(inst) inst:LaunchGooIcing() end),
            TimeEvent(49*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh") end),
		},
		
		onexit = function(inst)
			inst.taunt2 = false
			inst:DoTaskInTime(8, function(inst) inst.taunt2 = true end)
		end,

        events =
        {
            EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
			        inst.sg:GoToState("idle")
			    end
			end),
        },
    },
	
	State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(12*FRAMES, function(inst) PlayablePets.DoWork(inst, 5) end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "gingerbread_intro",
        tags = { "intro_state" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("gingerbread_eat_loop")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
            TimeEvent(6*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbreadpig/vocal") end end),
            TimeEvent(12*FRAMES, function(inst) if math.random() < 0.7 then inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/idle") end end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/eat") end),
		},

        events =
        {
            EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.components.combat == nil or inst.components.combat:HasTarget() then
				        inst.sg:GoToState("idle")
					else
				        inst.sg:GoToState("gingerbread_intro")
					end
			    end
			end),
        },
    },
	
	State
    {
        name = "special_atk1",
        tags = { "busy", "howling" },

        onenter = function(inst, count)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("howl")
            inst.SoundEmitter:PlaySound(inst.sounds.howl)
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation("death")	
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
        end,

		timeline =
        {

        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
}
CommonStates.AddRunStates(states,
{
    starttimeline = {},
    runtimeline =
    {
        TimeEvent(5 * FRAMES, function(inst)
            if inst:HasTag("clay") then
                PlayClayFootstep(inst)
            else
                PlayFootstep(inst)
            end
            inst.SoundEmitter:PlaySound(inst.sounds.idle)
        end),
    },
    endtimeline = {},
})
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle_loop", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.howl) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "howl"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})

    
return StateGraph("gingerbread_vargp", states, events, "idle", actionhandlers)

