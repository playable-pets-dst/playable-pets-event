require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction, true)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSurfaced(inst, surfaced)
	if surfaced then
		inst:Show()
		inst:RemoveTag("notarget")
		inst:RemoveTag("noplayerindicator")
		inst.noactions = nil
		inst.shouldwalk = true
		inst.components.health:SetInvincible(false)
		ChangeToCharacterPhysics(inst)
		if inst.surfacetask then
			inst.surfacetask:Cancel()
			inst.surfacetask = nil
		end
		inst.taunt2 = false
		inst:DoTaskInTime(5, function(inst) inst.taunt2 = true end)
	else
		if TheNet:GetServerGameMode() == "lavaarena" then
			inst.surfacetask = inst:DoTaskInTime(6, function(inst) inst.noactions = true inst.sg:GoToState("special_atk2") end)
		end
		inst.components.health:SetInvincible(true)
		inst:AddTag("notarget")
		inst:AddTag("noplayerindicator")
		inst.noactions = true
		inst.shouldwalk = false
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.WORLD)
		inst.Physics:CollidesWith(COLLISION.OBSTACLES)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
	State
    {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle", true)
        end,
		
		timeline =
		{
			TimeEvent(0, function(inst)
				if not inst.noactions then
					inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/idle", "idle_LP")
				end
			end),
		}
    },
	
    State{
		name = "spawn",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst:AddTag("notarget")
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("spawn")
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/enter")
			SetSurfaced(inst, true)
        end,

		timeline = {
			TimeEvent(0*FRAMES, function(inst) --made this a timeline thing because onenter is done right on spawn where Launchitems doesn't work.
                --COMMON_FNS.LaunchItems(inst, 2)
            end)
		},

        events = {
			EventHandler("animover", function(inst)
				inst:RemoveTag("notarget")
				inst.sg:GoToState("idle")
			end),
        },
    },
	
	State{
		name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst, cb)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target ~= nil then
					if target:IsValid() then
						inst:ForceFacePoint(target:GetPosition())
						inst.sg.statemem.attacktarget = target
					end
				end
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("attack")
			end
        end,

		timeline = {
			TimeEvent(5*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/attack")
			inst.FireProjectile(inst, inst.sg.statemem.attacktarget)
		end),
			TimeEvent(16*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/attack")
			inst.FireProjectile(inst, inst.sg.statemem.attacktarget)
		end),
		},

        events = {
			EventHandler("animover", function(inst)
				inst:RemoveTag("notarget")
				inst.sg:GoToState("idle")
			end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			if inst.noactions then
				SetSurfaced(inst, true)
				inst.AnimState:PlayAnimation("spawn")
				inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/enter")
			else
				SetSurfaced(inst, false)
				inst.AnimState:PlayAnimation("death")
			end	
        end,

		onexit = function(inst)
			if inst.noactions then
				inst:Hide()
				inst.DynamicShadow:Enable(false)
			else
				inst:Show()
				inst.DynamicShadow:Enable(true)
			end
		end,
		
		timeline=
        {
			TimeEvent(0, function(inst)
				if inst.noactions then
					inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/death")
					inst.SoundEmitter:KillSound("idle_LP")
				end
			end),
			TimeEvent(20*FRAMES, function(inst)
				if inst.noactions then
					inst.DynamicShadow:Enable(false)
				end
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline =
		{
			TimeEvent(0, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/death")
				inst.SoundEmitter:KillSound("idle_LP")
			end),
			TimeEvent(20*FRAMES, function(inst)
				inst.DynamicShadow:Enable(false)
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("idle")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("idle")
        end,
		
		timeline = 
		{

		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "walkning", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("idle")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "walkning", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("idle")
        end,
		
		timeline = 
		{

		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
}
--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"idle", nil, nil, "idle", "idle") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/death")end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/enter") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn"
	},
	--sounds =
	{

	},
	--fns =
	{
		corpse = function(inst)
			inst:DoTaskInTime(15 * (inst.revive_delay or 1), function(inst)
				inst:AddTag("corpse")
				inst.components.health:SetPercent(0.2)
				inst:PushEvent("respawnfromcorpse", { source = nil, user = nil })
				inst.sg:GoToState("corpse_rebirth")			
			end)
			inst.revive_delay = inst.revive_delay + 0.2
		end,
	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "idle", "idle")
local simpleanim = "idle"
local simpleidle = "idle"
local simplemove = "idle"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle",
}
)


    
return StateGraph("golemp", states, events, "idle", actionhandlers)

