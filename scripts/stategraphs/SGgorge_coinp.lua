require("stategraphs/commonstates")

local events=
{    
	EventHandler("ondropped", function(inst) inst.sg:GoToState("tossed") end),
	EventHandler("onvacuumthrown", function(inst) inst.sg:GoToState("tossed") end),
}

local states=
{   
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle", false)
            --inst.AnimState:PlayAnimation("idle_loop", true)
        end, 
    },
	
	State{
        name = "spawn",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("fall")
			inst.AnimState:PushAnimation("bounce"..math.random(1,2))
            --inst.AnimState:PlayAnimation("idle_loop", true)
        end, 
		
		timeline=
        {
           -- TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
        },
		
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("bounce") end),
		},
    },
	
	State{
        name = "bounce",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
			inst.AnimState:PlayAnimation("bounce"..math.random(1,2))
            --inst.AnimState:PlayAnimation("idle_loop", true)
        end, 
		
		timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
        },
		
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
    },
	
	State{
        name = "tossed",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
			local dir = math.random(1,2)
            inst.AnimState:PlayAnimation("toss"..dir, true)
            --inst.AnimState:PlayAnimation("idle_loop", true)
        end, 
		
		timeline=
        {
            --TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/common/coins/drop") end),
        },
		
		onupdate = function(inst)
			local pos = inst:GetPosition()
			if pos.y < 0.5 then
				inst.sg:GoToState("bounce")
			end		
		end,
		
		events =
		{
			--EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
    },
}

return StateGraph("gorge_coinp", states, events, "idle")