require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "herdling_chick"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "herdling_chick"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "herdling_chick"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.ATTACK, "idle")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	--no hit animation, so no attacked handler.
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
	EventHandler("ontalk", function(inst, data)
        if not inst.sg:HasStateTag("busy") then
	        inst.sg:GoToState("talkto")
        end
    end),
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

--TODO make this a common fn
local function ToggleHide(inst, hide)
	if hide then
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" and not TheWorld:HasTag("cave") then
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end
		inst.noactions = true
		inst:AddTag("notarget")
		inst:AddTag("noplayerindicator")
		inst.components.health:SetInvincible(true)
		inst.components.talker:IgnoreAll("hiding")
		inst.MiniMapEntity:SetIcon("")
		inst.components.locomotor:SetExternalSpeedMultiplier(inst, 2, 2)
		inst:Hide()
	else
		inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
		inst.Physics:CollidesWith(COLLISION.LIMITS)
		inst.noactions = false
		inst.components.health:SetInvincible(false)
		inst:RemoveTag("notarget")
		inst:RemoveTag("noplayerindicator")
		inst.MiniMapEntity:SetIcon(inst.mob_table.minimap)
		inst.components.talker:StopIgnoringAll("hiding")
		inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 2)
		inst:Show()
	end
end

local states=
{
    State{
        name = "idle",
		tags = {"idle"},
        onenter = function(inst)
			if inst._shouldturnoff then
				inst.sg:GoToState("turn_off")
			else
				inst.AnimState:PlayAnimation("idle", true)
			end
        end,
    },

    State{
        name = "launched",
		tags = {"busy", "jumping"},
        onenter = function(inst, data)
            inst.AnimState:PlayAnimation("run_loop", true)
            inst:ClearBufferedAction()
        end,

		onupdate = function(inst)
			local x, y, z = inst.Transform:GetWorldPosition()
			if y < 0.1 then
				inst.Physics:Stop()
				inst.sg:GoToState("idle")
			end
		end,

		onexit = function(inst)
			inst:OnLaunchLanded()
		end,
    },

    State{
        name = "attack",
		tags = {"busy", "death"},
        onenter = function(inst)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("win")

			inst:PushEvent("carnivalgame_herding_gothome")
            inst.SoundEmitter:KillSound("active_loop")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/carnival_games/herding_station/chicks/win") end),
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/carnival_games/herding_station/chicks/talk") end),
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/carnival_games/herding_station/chicks/talk") end),
            TimeEvent(28 * FRAMES, function(inst) 
				SpawnPrefab("carnival_confetti_fx").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
				inst.components.combat:DoAreaAttack(inst, 2, nil, nil, "explosive", GetExcludeTags(inst))
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.components.health:Kill()
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State{
        name = "death",
		tags = {"busy", "death"},
        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			RemovePhysicsColliders(inst)

            inst.SoundEmitter:KillSound("active_loop")
            inst.AnimState:PlayAnimation("lose")
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("summerevent/carnival_games/herding_station/chicks/talk") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
}

CommonStates.AddWalkStates(states,
{
    runtimeline =
    {
    },
},
{
	startwalk = "run_pre",
	walk = "run_loop",
	stopwalk = "run_pst",
})

CommonStates.AddRunStates(states,
{
    runtimeline =
    {
    },
},
{
	startrun = "run_pre2",
})

local moveanim = "run"
local idleanim = "idle"
local actionanim = "run_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "run_loop") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "lose",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("herdling_chickp", states, events, "idle", actionhandlers)

