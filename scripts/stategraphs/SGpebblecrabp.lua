require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) 
		if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

    },
	
	State{
        name = "attack",
        tags = {"busy", "attack"},
        onenter = function(inst, playanim)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle3")
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
        end,
		
		timeline = {
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
			end), 
			TimeEvent(12*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")		
			end),
			TimeEvent(13*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.attacktarget) end),
			TimeEvent(24*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
			end),		
		},
		
		events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("run_pst") 
			inst:Show()
			inst:RemoveTag("notarget")
			inst:RemoveTag("noplayerindicator")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle2")
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/peghook/taunt") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
			if inst.altattack == true then
				inst.altattack = false
			elseif inst.altattack == false then
				inst.altattack = true
			end	
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("burrow")
			inst:AddTag("notarget")
			inst:AddTag("noplayerindicator")
			inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/burrow")
        end,

        timeline=
        {
			
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst:Hide()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("idle")
			end,
			
		onexit = function(inst)
		end,

		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst:Show()
			inst:RemoveTag("notarget")
			inst:RemoveTag("noplayerindicator")
			inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/emerge")
            inst.AnimState:PlayAnimation("emerge")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			if inst.altattack == true then
				inst.sg:GoToState("run_start2")
			else
				inst.sg:GoToState("walk_start")				
			end			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run_start2",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("run_pre")
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_pre")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			if inst.altattack == true then
				inst.AnimState:PlayAnimation("run_pst")
			else
				inst.AnimState:PlayAnimation("walk_pst")
			end            
        end,
		
		timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/walk") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")
		end), 
		TimeEvent(12*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")		
		end),
		TimeEvent(13*FRAMES, function(inst) PlayablePets.DoWork(inst, 2.5) end),
		TimeEvent(24*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/quagmire/creature/pebble_crab/scratch")
		end),
	}, 
	"idle3", nil, nil, "idle3", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.components.health:SetPercent(1)
			end), 
		},
	
	},
	--anims = 
	{
		corpse = "hit",
		corpse_taunt = "idle2"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
    
return StateGraph("pebblecrabp", states, events, "idle", actionhandlers)

