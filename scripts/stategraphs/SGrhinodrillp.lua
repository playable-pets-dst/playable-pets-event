require("stategraphs/commonstates")

require("stategraphs/ppstates")

local longaction = "action_long"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack and inst.altattack == true then
			inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed*1.15, 0, 0)
			inst.components.locomotor:RunForward()
			return "attack2"
		else
			return "attack"
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .2, .01, .1, inst, 8)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.8, .03, .5, inst, 30)
end

local REV_RANGE = 25
local BUFF_RANGE = 6

local function GetBros(inst, range)
	local pos = inst:GetPosition()
	local bros = {}
	local ents = TheSim:FindEntities(pos.x,0,pos.z, range and range or 6, {"rhinobro"}, {"playerghost", "ghost", "INLIMBO", "notarget" })
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not v:HasTag("corpse") and v ~= inst then
			table.insert(bros, v)
		end
	end
	
	return bros
end

local function GetCD(inst)
	local health = inst.components.health:GetPercent()
	
	if health <= 0.3 then
		return 8
	else
		return 20
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		if inst.sg:HasStateTag("charging") then
		return {"rhinobro", "notarget", "wall"}
		else
			return  {"rhinobro", "notarget", "wall"}
		end
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall"}
	end
end

local function DoChargeAoE(inst)
	if inst.sg:HasStateTag("charging") then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, 3, nil, GetExcludeTags(inst))
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and not v.ischargehit then
					if inst.components.combat.target == v then
						inst._hashittarget = true
					end
					v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)*(1 + (0.25*inst.brostacks)))
					v.ischargehit = true
					v:DoTaskInTime(1, function(inst) inst.ischargehit = nil end)
				end
			end
		end	
    --inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, nil, nil, { "INLIMBO", "notarget", "playerghost", "battlestandard", "LA_mob" })
	end
end

local function DoFrontAoEp(inst)
	local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 3
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 3, nil, GetExcludeTags(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v)*(1 + (0.25*inst.brostacks)), nil, (v.sg and (v.sg:HasStateTag("hiding") or v.sg:HasStateTag("shielding") or v.sg:HasStateTag("spinning"))) and "explosive" or nil)
				inst:PushEvent("onattackother", { target = v })
				--v:PushEvent("flipped")
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local function SetDashPhysics(inst)
	inst.Transform:SetEightFaced()
    --inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
end

local function SetNormalPhysics(inst)
	inst.Transform:SetSixFaced()
    ChangeToCharacterPhysics(inst)
end

-----------------------------------------------------
--Cheering

local function DoCheer(inst, bros)
	if bros then
		inst.bros = bros --to connect each other
		inst.sg:GoToState("cheer_pre")
		for i, v in ipairs(bros) do
			if v.sg and not (v.sg:HasStateTag("death") or v:HasTag("corpse"))then
				v.sg:GoToState("cheer_pre")
			end
		end	
	end
end

local function DoBuff(inst)
	if inst.brostacks < 8 then
		inst.brostacks = inst.brostacks + 1
	end
	inst.components.debuffable:AddDebuff("rhinobro_buffp", "rhinobro_buffp")
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end
--
-----------------------------------------------------

local events=
{
	EventHandler("cheercanceled", function(inst, bro)
		if inst.sg:HasStateTag("cheering") then
			inst.sg:GoToState("cheer_pst")
		end		
	end),
	EventHandler("chest_bump", function(inst, bro)
		inst.sg:GoToState("chest_bump", bro)
	end),
	EventHandler("attacked", function(inst, data) 
		if inst.bros and inst.sg:HasStateTag("cheering") then
			for i, v in ipairs(inst.bros) do
				v:PushEvent("cheercanceled")
			end
			inst.bros = nil
		end
		
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + 0.75 < GetTime()) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or TheNet:GetServerGameMode() == "lavaarena" then
            inst.sg:GoToState("corpse", true)
        else
			local bros = GetBros(inst, REV_RANGE)
			if bros and #bros > 0 then
				inst.sg:GoToState("fake_death")
			else
				inst.sg:GoToState("death")
			end	
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(), --reminder if you do add death to this, you'll need to move this above
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
			if inst.altattack and inst.altattack == true then
				inst.sg:GoToState("attack2", data.target) 
			else
				inst.sg:GoToState("attack", data.target) 
			end
		end 
	end),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
	EventHandler("respawnfromcorpse", function(inst, reviver) if inst.sg:HasStateTag("corpse") and reviver then inst.sg:GoToState("corpse_taunt", reviver) end end),		
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
			inst.Transform:SetSixFaced()
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

	State{
        name = "work", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.Transform:SetSixFaced()
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/attack_2")
        inst.AnimState:PlayAnimation("attack")
        if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)
		inst.normalattack = nil
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(13*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				inst:PerformBufferedAction()
			end),
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
    State{
        name = "attack", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
		
		inst.Transform:SetSixFaced()
        
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/attack_2")
        inst.AnimState:PlayAnimation("attack")
	end,
	
	onexit = function(inst)
		inst.normalattack = nil
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(13*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				--inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
				DoFrontAoEp(inst)
				--inst.components.combat:DoAreaAttack(inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget or inst, 5, nil, nil, nil, GetExcludeTags(inst)) 
			end),
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "attack2", --ram
        tags = {"busy", "charging"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		inst._hashittarget = nil
		
		if target ~= nil then
            if target:IsValid() then
                --inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
		
		SetDashPhysics(inst)
		inst.altattack = false
        --inst.components.combat:StartAttack()
        
		inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/attack")
        inst.AnimState:PlayAnimation("attack2_pre")
        
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoChargeAoE)
		end
		--have no idea why I can't move in this state.
		inst:DoTaskInTime(0, function(inst) inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed * 1.15, 0, 0) end)
	end,
	
	onexit = function(inst)
		inst:DoTaskInTime(GetCD(inst), function(inst) inst.altattack = true inst.components.combat:SetRange(10, 0) end)
		inst.components.combat:SetRange(2.5, 5)
		SetNormalPhysics(inst)
    end,
	
    timeline=
    {

    },

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("attack2_loop", inst.sg.statemem.attacktarget)  
			end),
        },
	
    },
	
	State{
        name = "attack2_loop", --ram
        tags = {"attack", "busy", "charging"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		if target and not inst._hashittarget then
            if target:IsValid() then
                --inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
		else
			--we hit our target, time to stop
			if inst.rams < 9 then
				inst.rams = 9
			end
        end
		
		SetDashPhysics(inst)
		inst.Transform:SetEightFaced()
		inst.altattack = false
		inst.rams = inst.rams + 1
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoChargeAoE)
		end
        
        --inst.components.combat:StartAttack()
        --inst.components.locomotor:Stop()
        --inst.Physics:Stop()
		inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed*1.15, 0, 0)
		--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
        inst.AnimState:PlayAnimation("attack2_loop")
        
	end,
	
	onupdate = function(inst)
		if not inst.sg.statemem.charged_target then		
			local target = inst.sg.statemem.attacktarget
			if target == nil or (target.components.health and target.components.health:IsDead()) or inst._hashittarget then
				if not target or (target.components.health and target.components.health:IsDead()) then
					if inst.rams < 9 then
						inst.rams = 9
					end
				end
			else
				
			local angle = inst.Transform:GetRotation() - inst:GetAngleToPoint(target:GetPosition())
			if angle > 180 then 
				angle = 360 - angle 
			elseif angle < -180 then 
				angle = 360 + angle 
			end
				
			--if angle > 45 or angle < -45 then
				--inst.sg.statemem.speed = inst.sg.statemem.speed <= 0 and inst.sg.statemem.speed or inst.sg.statemem.speed - accel
			--else
				--inst.sg.statemem.speed = inst.sg.statemem.speed >= maxspeed and inst.sg.statemem.speed or inst.sg.statemem.speed + accel
			--end
				
			local rot = 0
			local maxrott = 4
				
			if angle >= maxrott then
				rot = maxrott
			elseif angle <= -maxrott then
				rot = -maxrott
			else
				rot = angle
			end
				
			inst.Transform:SetRotation(inst.Transform:GetRotation() - rot)
			end
		end	
	end,
	
	onexit = function(inst)
		--inst.components.combat:SetRange(3, 5)
		SetNormalPhysics(inst)
		--inst.sg.statemem.attacktarget = nil
		if inst._chargetask ~= nil then
			inst._chargetask:Cancel()
			inst._chargetask = nil
		end
    end,
	
    timeline=
    {

    },

        events=
        {
            EventHandler("animover", function(inst) 
				if inst.rams <= 10 then
					inst.sg:GoToState("attack2_loop")
				else
					inst.rams = 0
					if inst._chargetask ~= nil then
						inst._chargetask:Cancel()
						inst._chargetask = nil
					end
					inst.sg:GoToState("attack2_pst")  
				end	
			end),
			EventHandler("onareaattackother", function(inst, data) 
				if data.target == inst.sg.statemem.attacktarget then
					inst.rams = 9
					inst.sg.statemem.charged_target = true
				end
			end),
        },
	
    },
	
	State{
        name = "attack2_pst", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.Transform:SetSixFaced()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("attack2_pst")
		end,
	
		timeline=
		{

		},

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("revive_pre")
            inst.AnimState:PushAnimation("revive_loop", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("revive_loop", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("revive_pst")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle")
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.statemem.last_attacked = GetTime()
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit")
			inst.rams = 0
			inst.cheer_loop = 0
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, force)
			local bros = GetBros(inst, BUFF_RANGE)
			if (#bros > 0 and inst.cancheer and inst.cancheer == true) or force then
				--inst.sg:GoToState("cheer_pre")
				DoCheer(inst, bros)
			else
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("taunt")
			end
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/taunt") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy", "idle"},

        onenter = function(inst, force)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("pose_pre", false)
			inst.AnimState:PushAnimation("pose_loop", true)
        end,

		timeline=
        {
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/cheer") inst.sg:RemoveStateTag("busy") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			--EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst, force)			
            inst.Physics:Stop()
			inst.Transform:SetSixFaced()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/taunt") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "cheer_pre",
        tags = {"busy", "cheering"},

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("cheer_pre")
			inst.cancheer = false
        end,

		timeline=
        {
		
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(20, function(inst) inst.cancheer = true end)
		end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("cheer_loop") end),
        },
    },
	
	State{
		name = "cheer_loop",
        tags = {"busy", "cheering"},

        onenter = function(inst, cb)
			inst.cheer_loop = inst.cheer_loop + 1
			if inst.cheer_loop == 3 then
				DoBuff(inst)
			end
            inst.Physics:Stop()		
			inst.AnimState:PlayAnimation("cheer_loop")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/cheer")
        end,

		timeline=
        {
		
        },

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.cheer_loop >= 4 then
					inst.sg:GoToState("cheer_pst")
				else
					inst.sg:GoToState("cheer_loop")
				end
			end),
			EventHandler("cheercanceled", function(inst) 
				inst.sg:GoToState("cheer_pst")
			end),
        },
    },
	
	State{
		name = "cheer_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("cheer_post")
			inst.cheer_loop = 0
        end,

		timeline=
        {
		
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action_long",
        tags = {"doing", "nodangle"},

        onenter = function(inst, cb)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("revive_pre", false)
			inst.AnimState:PushAnimation("revive_loop", false)
			inst.AnimState:PushAnimation("revive_pst", false)
			
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/revive_LP", "reviveLP")
        end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("reviveLP")
		end,
		
		timeline=
        {
			TimeEvent(25 * FRAMES, function(inst)
                inst:PerformBufferedAction()
				inst.SoundEmitter:KillSound("reviveLP")
            end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	--We aren't really dead, but we need help from a bro! if bro doesn't come to save us then just die.
	State{
        name = "fake_death",
        tags = {"busy", "corpse", "death"},

        onenter = function(inst, time)
			inst.Transform:SetSixFaced()
			
			inst:AddTag("corpse")
			
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

            inst:PushEvent("playerdied", { skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			
			inst.sg:SetTimeout(time and time or 60)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
                 
        end,
		
		timeline=
        {

        },

		ontimeout = function(inst)
			inst:ShowActions(true)
			inst.sg:GoToState("death", true)
		end,
		
		onexit = function(inst)
			inst:RemoveTag("corpse")
		end,
		
        events =
        {
		
        },

    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst, forced)
			inst.Transform:SetSixFaced()
			if forced then
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death_final_final")
				inst.AnimState:PlayAnimation("death_finalfinal", false)
			else
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death")
				inst.AnimState:PlayAnimation("death", false)
				inst.AnimState:PushAnimation("death_finalfinal", false)
			end	
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(31*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death_final_final") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(38*FRAMES, function(inst) 
				ShakeIfClose(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) 
				inst.sg:GoToState("sleeping") 
			end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_in")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_out") end),
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			--end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(9*FRAMES, PlayFootstep),
			--TimeEvent(9*FRAMES, ShakeIfClose),
			TimeEvent(18*FRAMES, PlayFootstep),
			--TimeEvent(18*FRAMES, ShakeIfClose),
			--TimeEvent(1*FRAMES, ShakeIfClose),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = {

			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt") end),
			--TimeEvent(1*FRAMES, ShakeIfClose),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "chest_bump",
        tags = { "busy", "nointerrupt" },

        onenter = function(inst, bro) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("chest_bump")
			local angle = inst.Transform:GetRotation() - inst:GetAngleToPoint(bro:GetPosition())
			inst.Transform:SetRotation(angle)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.cheer_loop = 0
			inst.rams = 0
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(2)
				--inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
			else
				--inst.components.combat:SetTarget(nil)
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
			end
			inst.sg.statemem.flash = 0
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
        },
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst.components.health:SetAbsorptionAmount(0)           
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" then
				inst.sg.statemem.needstotaunt = true
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action_long",
        tags = {"doing", "nodangle"},

        onenter = function(inst, cb)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("revive_pre", false)
			inst.AnimState:PushAnimation("revive_loop", false)
			inst.AnimState:PushAnimation("revive_pst", false)
			
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/revive_LP", "reviveLP")
        end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("reviveLP")
		end,
		
		timeline=
        {
			TimeEvent(25 * FRAMES, function(inst)
                inst:PerformBufferedAction()
				inst.SoundEmitter:KillSound("reviveLP")
            end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle", "idle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("revive_pre", false)
			inst.AnimState:PushAnimation("revive_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/revive_LP", "reviveLP")
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("revive_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
			inst.SoundEmitter:KillSound("reviveLP")
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	--==============================
	-------------------Forge States------------------------	
	State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death")
        end,
		
		timeline=
		{
		
		},

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

			inst:RemoveTag("corpse")
			
			inst.AnimState:PlayAnimation("death_post")
			
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				inst.components.health:SetPercent(0.2)
			end
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

		timeline=
		{

		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
			inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME * 
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) * ((target and target:HasTag("rhinobro")) and 0.25 or
                (target ~= nil and target.components.revivablecorpse ~= nil) and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)



    
return StateGraph("rhinodrillp", states, events, "idle", actionhandlers)

