require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "attack"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction, true)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if (target and inst:IsNear(target, 3.5)) then
			return "attack"
		else
			return "attack2"
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local friendly_banners = {
	"battlestandard_damager_friendly",
	"battlestandard_shield_friendly",
	"battlestandard_speed_friendly",
	--"battlestandard_heal_friendly"
}

local function Spawn_New_Banner(inst)
	local theta = math.random() * 2 * PI
    local pos = inst:GetPosition()
    local radius = math.random(1, 2)
	local offset = FindWalkableOffset(pos, theta, radius, 2, true, true)
	local banner = SpawnPrefab(friendly_banners[math.random(1, #friendly_banners)])
	if inst.mybanner then
		inst.mybanner.components.health:Kill()
	end
	banner.owner = inst
	banner.efficiency = 0.25
	inst.mybanner = banner
	banner.Transform:SetPosition((offset and (pos + offset) or pos):Get())
	banner:ListenForEvent("death", function(inst)
		if inst.owner then
			inst.owner.mybanner = nil
		end
    end)
end

 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --bite
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        --inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack")
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)
        inst.components.combat.hitrange = 99
    end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/attack") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
			end),
            TimeEvent(10*FRAMES, function(inst)
				inst.components.combat.hitrange = 3.25
				PlayablePets.DoWork(inst, 2.5)
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "work", --bite
        tags = {"attack", "busy"},
		
		  onenter = function(inst)
		  inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack")
	end,
	
	onexit = function(inst)
        inst.components.combat:SetTarget(nil)
    end,
	
    timeline=
    {

			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/attack")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
			end),
            TimeEvent(12*FRAMES, function(inst) inst:PerformBufferedAction() end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.components.combat:SetTarget(nil)  inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "attack2", --spit
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)		
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("spit")
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/spit")
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/spit2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/hurt") 
			end),
            TimeEvent(13*FRAMES, function(inst) 
				inst.FireProjectile(inst, inst.sg.statemem.attacktarget)			
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.components.combat:SetTarget(nil)  inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/taunt") 
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "banner_summon",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("banner_summon")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt_2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
        end,

		timeline=
        {
			TimeEvent(20*FRAMES, PlayFootstep),
			TimeEvent(20*FRAMES, Spawn_New_Banner),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.sg:GoToState("banner_summon")
			else	
				inst.AnimState:PlayAnimation("taunt_2")
			end
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt_2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) 
				if TheNet:GetServerGameMode() == "lavaarena" and not inst.mybanner then
					inst.sg:GoToState("")
				else
					inst.sg:GoToState("idle") 
				end	
			end),
        },
    },
	
	State{
		name = "build",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("banner_summon")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt_2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
        end,

		timeline=
        {
			TimeEvent(20*FRAMES, PlayFootstep),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/sleep")				
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep ),
			TimeEvent(10*FRAMES, PlayFootstep ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_loop")
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("stun_pst") end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle_loop", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt_2") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt_2"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)


    
return StateGraph("snapperp", states, events, "idle", actionhandlers)

