require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function ShakeSmall(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .1, .01, .3, inst, 20)
end

local MAX_SPEED = 30 
local ACCELERATION = MAX_SPEED/12
local DISTANCE_BUFFER = 10 
local MAX_SPIN_RAMS = 12
local MAX_RAM_TIME = 3 

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"boarrior_pet", "notarget", "INLIMBO", "shadow", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction, true)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack and inst.components.health:GetPercent() <= 0.35  then
			return "attack2"
		else
			return "attack"
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function CleanHide(inst)
	inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.95 or 0)
	inst.hider_event = true
	if inst.undohidetask then
		inst.undohidetask:Cancel()
		inst.undohidetask = nil
	end
end

local function DoSwarmAttack(inst)
	if inst.sg:HasStateTag("spinning") then
    inst.components.combat:DoAreaAttack(inst, 2.5, nil, nil, nil, GetExcludeTagsp(inst))
	end
end

local function EnableAltAttack(inst)
	inst.altattack = true
	inst.components.combat:SetRange(8) -- attack from farther away
end

local function DisableAltAttack(inst)
	inst.altattack = false
	inst.components.combat:SetRange(2, 3)
end

local function SpinRamSetup(inst, target)
    local target = target or inst.components.combat.target
    if target then
        -- Spin Setup
        inst.sg.statemem.spin_state = "accelerating"
        inst.sg.statemem.current_speed = 0
        inst.components.locomotor:Stop()
        -- Calculate Target Position to Spin towards
        local target_pos = target:GetPosition()
        inst:ForceFacePoint(target_pos)
        inst.Physics:SetMotorVel(inst.sg.statemem.current_speed, 0, 0)
        --local offset = (target_pos - inst:GetPosition()):GetNormalized()*5 -- TODO magic number? -- TODO remove when confirmed new spinning method works
        inst.sg.statemem.target_pos = target_pos-- + offset -- TODO remove offset when new spinning method has been confirmed
        inst.sg.statemem.ram_start_time = GetTime()
    -- Queue Spin to End if No Target
    else
        inst.sg.statemem.end_spin = true
    end
end

local events=
{
    EventHandler("attacked", function(inst) 
		if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("hiding") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and inst.sg:HasStateTag("hiding") and not inst.sg:HasStateTag("nointerrupt") then
			inst.sg:GoToState("hide_hit")
		end 
	end),
    PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.OpenGift(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --spikes
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack1")
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack1a") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
			end), --spikes out
            TimeEvent(12*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 3, nil, nil, nil, GetExcludeTagsp(inst))
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack")
			end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack1b") end), --spikes in
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle", "atk_pst")  end),
        },
	
    },
	
	--NEW, major thanks to Electroely
	State{
        name = "attack2", --shell attack
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.altattack = false
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack2_pre")
        inst.sg.statemem.attacktarget = inst.components.combat.target
	end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
			TimeEvent(20*FRAMES, function(inst) -- TODO spinning starts here, including damage and movement. Maybe just go straight to the attack loop state? Or should we create condition thread here and whatnot?
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact")
                ShakeSmall(inst)
            end),

    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("attack2_loop", inst.sg.statemem.attacktarget)  end),
        },
	
    },
	
	State{
        name = "attack2_loop", 
        tags = {"attack", "busy", "nointerrupt", "spinning", "shell"},
		--use ram attempts maybe.
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		--inst.sg:SetTimeout(37*FRAMES*DURATION_MULT)
		if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
		DisableAltAttack(inst)
		inst.ram_attempt = inst.ram_attempt + 1
		inst:SetShellPhysics()
		inst.components.health:SetInvincible(true)
		--inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 0.01)
		inst.components.combat.multiplier = 0.5
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack2_loop", true)
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack2_LP", "shell_loop")
		inst.sg.statemem.initpos = inst:GetPosition()
		inst.sg.statemem.spinstate = "init"
		inst.sg.statemem.spinspeed = 2
		inst.sg.statemem.attacktarget = inst.components.combat.target
		if inst._spintask == nil then
		inst._spintask = inst:DoPeriodicTask(TUNING.SHADOW_BISHOP.ATTACK_TICK/2, DoSwarmAttack)
		end
		if inst.sg.statemem.attacktarget ~= nil then
			inst:FacePoint(inst.sg.statemem.attacktarget:GetPosition())
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
			local offset = inst.sg.statemem.attacktarget:GetPosition() - inst:GetPosition()
			if offset:Length() < 8 then
				offset = offset:GetNormalized()*8
			end
			inst.sg.statemem.targetpos = inst:GetPosition() + offset
		else 
			if inst._spintask ~= nil then
				inst._spintask:Cancel()
				inst._spintask = nil
			end
			inst.sg:GoToState("attack2_pst")
			inst.ram_attempt = 0
			inst:DoTaskInTime(15, EnableAltAttack)
			-- no target, just cut the attack we're done here there's nothing to it it's the end it's all over it's all gone literally nothing we can do
		end
	end,
	
	onupdate = function(inst)
		if inst.sg.statemem.spinstate == "init" then
			if inst.sg.statemem.spinspeed < 8 then
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed * 1.5
			else 
				inst.sg.statemem.spinstate = "full"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "full" then
			local initoffset = inst.sg.statemem.initpos - inst.sg.statemem.targetpos 
			local currentoffset = inst:GetPosition() - inst.sg.statemem.targetpos 
			-- check if we're past that
			if ((((initoffset.x >= 0) and (currentoffset.x <= 0)) or ((initoffset.x <= 0) and (currentoffset.x >= 0))) and
			(((initoffset.z >= 0) and (currentoffset.z <= 0)) or ((initoffset.z <= 0) and (currentoffset.z >= 0)))) or (inst.sg.statemem.spinspeed > 75) then
				inst.sg.statemem.spinstate = "slowing"
			else
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed * 1.1
				inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
			end
		elseif inst.sg.statemem.spinstate == "slowing" then
			if inst.sg.statemem.spinspeed > 8 then
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed / 1.2
			else
				inst.sg.statemem.spinstate = "stopping"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "stopping" then
			if inst.sg.statemem.spinspeed > 1 then
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed / 2
			else
				inst.sg.statemem.spinspeed = 0
				inst.sg.statemem.spinstate = "done"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "done" then
			if inst.ram_attempt >= 10 then
				inst._spintask:Cancel()
				inst._spintask = nil
				inst.sg:GoToState("attack2_pst")
				--inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 1)
				inst.ram_attempt = 0
				inst:DoTaskInTime(15, EnableAltAttack)
				
			else
				inst.sg:GoToState("attack2_loop")
			end
		end
    end,
	
	onexit = function(inst)
		inst.SoundEmitter:KillSound("shell_loop")
		if inst._spintask ~= nil then
			inst._spintask:Cancel()
			inst._spintask = nil
		end
		inst.sg.statemem.shell = nil
		inst.sg.statemem.spinstate = nil
		inst.sg.statemem.spinspeed = nil
		inst.sg.statemem.targetpos = nil
		inst.sg.statemem.initpos = nil
		if not inst.sg.statemem.attack then
            inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 1)
        end
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		if inst.sg.statemem.attacktarget ~= nil then
			inst.components.combat:SetTarget(inst.sg.statemem.attacktarget)
			inst.sg.statemem.attacktarget = nil
		end
		inst:SetNormalPhysics()
		inst.components.health:SetInvincible(false)
		inst.components.combat.multiplier = 1		
		inst.components.locomotor.walkspeed = 0
    end,
	
    timeline=
    {

			TimeEvent(3*FRAMES, function(inst) inst.sg.statemem.shell = true end),
    },
	
        events=
        {
            --EventHandler("animqueueover", function(inst)  end),
			EventHandler("attacked", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact") end)
        },
	
    },
	
	State{
		name = "attack2_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2_pst") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst")
			inst:PutBackOnGround()
			if inst._spintask ~= nil then
			inst._spintask:Cancel()
			inst._spintask = nil
			end
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/taunt")
				ShakeSmall(inst)
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hide_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pre")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") 
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hiding",
        tags = {"busy", "hiding"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_idle")
			if not inst.defdebuffed and not inst.shrinkdebuffed then
				inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 1 or TUNING.ROCKY_ABSORB)
			end
			if not inst.undohidetask and TheNet:GetServerGameMode() == "lavaarena" then
				inst.hider_event = false
				inst.undohidetask = inst:DoTaskInTime(8, function(inst) inst.sg:GoToState("hide_pst") end)
			end
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hide_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pst")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.95 or 0)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.hider_event = false
				inst:DoTaskInTime(8, function(inst) inst.hider_event = true end)
			end
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	
	State{
		name = "hide_hit",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_dull")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "flip_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
			CleanHide(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_pre")			
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") 
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("flip") end),
        },
    },
	
	State{
		name = "flip",
        tags = {"busy", "flipped"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_loop")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("flip") end),
        },
    },
	
	State{
		name = "flip_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_pst")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "flip_hit",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_hit")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_loop")
			CleanHide(inst)
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("stun_pst") end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			CleanHide(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline =
		{
			TimeEvent(18*FRAMES, function(inst) 
				ShakeSmall(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/death") 
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/step")
			end),
			TimeEvent(12*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/step")
			end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, CleanHide),
			TimeEvent(18*FRAMES, function(inst) 
				ShakeSmall(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/death") 
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/taunt")
				ShakeSmall(inst)
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
    
return StateGraph("tortankp", states, events, "idle", actionhandlers)

