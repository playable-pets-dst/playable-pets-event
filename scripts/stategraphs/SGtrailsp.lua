require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local health = inst.components.health:GetPercent()
		if inst.altattack2 and inst.altattack2 == true and health <= 0.9 then
			return "attack3"
		elseif inst.altattack and inst.altattack == true and health <= 0.5 then
			return "attack2"
		else
			return "attack"
		end	
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end
local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function ShakeIfCloseRoll(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .25, .01, .1, inst, 15)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.8, .03, .5, inst, 30)
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(inst.jump_range, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local function DoRollAoE(inst)
	if inst.sg:HasStateTag("rolling") then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, 3, nil, GetExcludeTagsp(inst))
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and not v.ischargehit and v ~= inst then
					if inst.components.combat.target == v then
						inst._hashittarget = true
					end
					v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v))
					v.ischargehit = true
					v:DoTaskInTime(1, function(inst) inst.ischargehit = nil end)
				end
			end
		end	
    --inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, nil, nil, { "INLIMBO", "notarget", "playerghost", "battlestandard", "LA_mob" })
	end
end

local function DoFrontAoe(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 2
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, 5, { "locomotor"}, GetExcludeTagsp(inst)) 
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.sg.statemem.attacktarget then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent))
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) then
		return target
	else
		return distsq ~= nil and distsq < 225 and not player:HasTag("notarget") and player --Leo: Removed the LA_mob check because we should always want them to attack players in forge.
	end
end

local function SetRollPhysics(inst)
    --inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
end

local function SetNormalPhysics(inst)
    ChangeToCharacterPhysics(inst)
end

local function CleanHide(inst)
	inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.9 or 0)
	inst.canhide = true
	if inst.undohidetask then
		inst.undohidetask:Cancel()
		inst.undohidetask = nil
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("hiding") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + 0.75 < GetTime())  then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and inst.sg:HasStateTag("hiding") then
			inst.sg:GoToState("hide_hit")
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
		inst.normalattack = true
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
        inst.AnimState:PlayAnimation("attack2")
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)
		inst.normalattack = nil
    end,
	
    timeline=
    {

			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/swish") 
			--inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
			end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/attack2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/infection_attack_pre")	
			end),
            TimeEvent(12*FRAMES, function(inst) 
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
				DoFrontAoe(inst)
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.components.combat:SetTarget(nil)  inst.sg:GoToState("idle", "atk_pst")  end),
        },
	
    },
	
	State{
        name = "attack2", --slam
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
		  inst.altattack = false
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
        inst.AnimState:PlayAnimation("attack1")
        if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
    onupdate = function(inst)
        if inst.sg.statemem.jump then
            inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
        end
    end,
	
	onexit = function(inst)
		if inst.altattack2 and inst.altattack2 == true and inst.components.health:GetPercent() < 0.9 then
			inst.components.combat:SetRange(15, 0)
		else
			inst.components.combat:SetRange(3, 5)
		end
		inst.sg.statemem.attacktarget = nil
		inst:DoTaskInTime(10, function(inst) inst.altattack = true inst.components.combat:SetRange(10, 0) end)
    end,
	
    timeline=
    {

			TimeEvent(3*FRAMES, function(inst) 
				if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid()  then
					inst.sg.statemem.speed = CalcJumpSpeed(inst, inst.sg.statemem.attacktarget)		
					inst.sg.statemem.jump = true
				end
			end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/attack1") end),
			TimeEvent(20*FRAMES, ShakePound),
            TimeEvent(23*FRAMES, function(inst)
				inst.components.combat.areahitdamagepercent = 1.5
				inst.components.combat:DoAreaAttack(inst, 6, nil, nil, nil, GetExcludeTagsp(inst))
				inst.components.combat.areahitdamagepercent = 1
			end),
			TimeEvent(18*FRAMES, function(inst) 
				inst.Physics:ClearMotorVelOverride()
				inst.components.locomotor:Stop()
				inst.sg.statemem.jump = false 
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
        name = "attack3", --roll
        tags = {"busy", "rolling"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		inst._hashittarget = nil
		
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
		
		inst.altattack2 = false
		
		inst.components.locomotor:Stop()
		SetRollPhysics(inst)
		--inst.altattack2 = false
        inst.components.combat:StartAttack()
        
        inst.AnimState:PlayAnimation("roll_pre")

		--inst:DoTaskInTime(0, function(inst) inst.Physics:SetMotorVelOverride(inst.components.locomotor.walkspeed * 1.2, 0, 0) end)
	end,
	
	onexit = function(inst)
		inst:DoTaskInTime(15, function(inst) inst.altattack2 = true inst.components.combat:SetRange(15, 0) end)
		if inst.altattack and inst.altattack == true and inst.components.health:GetPercent() <= 0.5 then
			inst.components.combat:SetRange(15, 0)
		else
			inst.components.combat:SetRange(3, 5)
		end
		SetNormalPhysics(inst)
    end,
	
    timeline=
    {

    },

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("attack3_loop", inst.sg.statemem.attacktarget)  
			end),
        },
	
    },
	
	State{
        name = "attack3_loop", --ram
        tags = {"attack", "busy", "rolling"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		if target and not inst._hashittarget then
            if target:IsValid() then
                inst.sg.statemem.attacktarget = target
            end
		else
			--we hit our target, time to stop
			if inst.rams < 29 then
				inst.rams = 29
			end
        end
		
		SetRollPhysics(inst)
		inst.rams = inst.rams + 1
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoRollAoE)
		end

		inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed*1.2, 0, 0)
        inst.AnimState:PlayAnimation("roll_loop")
        
	end,
	
	onupdate = function(inst)
		if not inst.sg.statemem.charged_target then		
			local target = inst.sg.statemem.attacktarget
			if target == nil or target.components.health:IsDead() or inst._hashittarget then
				if not target or target.components.health:IsDead() then
					if inst.rams < 29 then
						inst.rams = 29
					end
				end
			else
				
			local angle = inst.Transform:GetRotation() - inst:GetAngleToPoint(target:GetPosition())
			if angle > 180 then 
				angle = 360 - angle 
			elseif angle < -180 then 
				angle = 360 + angle 
			end
				
			--if angle > 45 or angle < -45 then
				--inst.sg.statemem.speed = inst.sg.statemem.speed <= 0 and inst.sg.statemem.speed or inst.sg.statemem.speed - accel
			--else
				--inst.sg.statemem.speed = inst.sg.statemem.speed >= maxspeed and inst.sg.statemem.speed or inst.sg.statemem.speed + accel
			--end
				
			local rot = 0
			local maxrott = 3
				
			if angle >= maxrott then
				rot = maxrott
			elseif angle <= -maxrott then
				rot = -maxrott
			else
				rot = angle
			end
				
			inst.Transform:SetRotation(inst.Transform:GetRotation() - rot)
			end
		end	
	end,
	
	onexit = function(inst)
		--inst.components.combat:SetRange(3, 5)
		SetNormalPhysics(inst)
		--inst.sg.statemem.attacktarget = nil
		if inst._chargetask ~= nil then
			inst._chargetask:Cancel()
			inst._chargetask = nil
		end
    end,
	
    timeline=
    {
		TimeEvent(0*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(3*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(6*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(9*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(12*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(15*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
    },

        events=
        {
            EventHandler("animover", function(inst) 
				if inst.rams <= 30 then
					inst.sg:GoToState("attack3_loop")
				else
					inst.rams = 0
					if inst._chargetask ~= nil then
						inst._chargetask:Cancel()
						inst._chargetask = nil
					end
					inst.sg:GoToState("attack3_pst")  
				end	
			end),
			EventHandler("onareaattackother", function(inst, data) 
				if data.target == inst.sg.statemem.attacktarget then
					inst.rams = 29
					inst.sg.statemem.charged_target = true
				end
			end),
        },
	
    },
	
	State{
        name = "attack3_pst", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.Physics:Stop()
			inst.Physics:SetMotorVelOverride(inst.components.locomotor.walkspeed*1.2, 0, 0)
			inst.AnimState:PlayAnimation("roll_pst")
		end,
	
		timeline=
		{
			TimeEvent(0*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(3*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(6*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(9*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(11*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
			inst.components.locomotor:Stop()
		end),
		},

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
		name = "hide_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pre")
			
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_pre") 
				--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp") 
				end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hiding",
        tags = {"busy", "hiding"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_loop")
			if not inst.defdebuffed and not inst.shrinkdebuffed then
				inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 1 or 0.9)
			end
			if not inst.undohidetask and TheNet:GetServerGameMode() == "lavaarena" then
				inst.canhide = false
				inst.undohidetask = inst:DoTaskInTime(8, function(inst)
					if not inst.components.health:IsDead() then
						inst.sg:GoToState("hide_pst") 
					else
						inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.9 or 0)

						inst.canhide = false
						inst:DoTaskInTime(8, function(inst) inst.canhide = true end)
					end
				end)
			end
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hide_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pst")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_swell") end),
        },
		
		onexit = function(inst)
            inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.9 or 0)
			if TheNet:GetServerGameMode() == "lavaarena" then
				if inst.undohidetask then
					inst.undohidetask:Cancel()
					inst.undohidetask = nil
				end
				inst.canhide = false
				inst:DoTaskInTime(8, function(inst) inst.canhide = true end)
			end
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hide_hit",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_hit")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_dull") 
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(20*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
			end),
			TimeEvent(20*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_roar")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(11*FRAMES, ShakeIfClose),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
			--TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_in")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_out") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
				end),
			TimeEvent(0*FRAMES, ShakeIfClose),
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(2*FRAMES, ShakeIfClose),
			TimeEvent(10*FRAMES, function(inst) inst.components.locomotor:WalkForward() end),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, stimuli)
            inst.Physics:Stop()
			if stimuli and stimuli == "electric" then
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.sg:SetTimeout(1)
			else
			inst.AnimState:PlayAnimation("stun_loop")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
			end
			inst.sg.statemem.flash = 0
			
			inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 0.9 or 0)
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
        },
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst.components.health:SetAbsorptionAmount(0)        
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
		

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" then
				inst.sg.statemem.needstotaunt = true
			end
			inst.components.health:SetAbsorptionAmount(0)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()	
        end),
	}, 
	"run_pst", nil, nil, "idle_loop", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death") end),
			TimeEvent(11*FRAMES, ShakeIfClose),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_pre") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt") end),
			TimeEvent(20*FRAMES, ShakeRoar),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)


    
return StateGraph("trailsp", states, events, "idle", actionhandlers)

